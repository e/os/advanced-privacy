/*
 * Copyright (C) 2024 E FOUNDATION
 * Copyright (C) 2023 MURENA SAS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package foundation.e.advancedprivacy.trackers.service

import foundation.e.advancedprivacy.domain.entities.ApplicationDescription
import foundation.e.advancedprivacy.domain.entities.FeatureState
import foundation.e.advancedprivacy.domain.entities.MainFeatures
import foundation.e.advancedprivacy.domain.entities.MainFeatures.IpScrambling
import foundation.e.advancedprivacy.domain.entities.MainFeatures.TrackersControl
import foundation.e.advancedprivacy.domain.entities.ProfileType
import foundation.e.advancedprivacy.domain.repositories.LocalStateRepository
import foundation.e.advancedprivacy.domain.usecases.VpnSupervisorUseCase
import foundation.e.advancedprivacy.externalinterfaces.permissions.IPermissionsPrivacyModule
import foundation.e.advancedprivacy.ipscrambler.OrbotSupervisor
import foundation.e.advancedprivacy.trackers.domain.externalinterfaces.TrackersSupervisor
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.drop
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.launch
import timber.log.Timber

class VpnSupervisorUseCaseEos(
    private val localStateRepository: LocalStateRepository,
    private val orbotSupervisor: OrbotSupervisor,
    private val trackersSupervisor: TrackersSupervisor,
    private val appDesc: ApplicationDescription,
    private val permissionsPrivacyModule: IPermissionsPrivacyModule,
    private val scope: CoroutineScope
) : VpnSupervisorUseCase {

    override fun listenSettings() {
        trackersSupervisor.start()

        scope.launch(Dispatchers.IO) {
            localStateRepository.ipScramblingEnabled.collect {
                applySettings(it)
            }
        }

        scope.launch(Dispatchers.IO) {
            localStateRepository.blockTrackers.drop(1).filter { it }.collect {
                localStateRepository.emitStartVpnDisclaimer(TrackersControl())
            }
        }
    }

    private suspend fun applySettings(isIpScramblingEnabled: Boolean) {
        val currentMode = localStateRepository.internetPrivacyMode.value
        when {
            (
                isIpScramblingEnabled &&
                    currentMode in setOf(FeatureState.OFF, FeatureState.STOPPING)
                ) -> {
                applyStartIpScrambling()
            }
            (
                !isIpScramblingEnabled &&
                    currentMode in setOf(FeatureState.ON, FeatureState.STARTING)
                ) -> {
                orbotSupervisor.stop()
            }
            else -> {}
        }
    }

    private suspend fun applyStartIpScrambling() {
        if (orbotSupervisor.prepareAndroidVpn() != null) {
            permissionsPrivacyModule.setVpnPackageAuthorization(appDesc.packageName)
            val alwaysOnVpnPackage = permissionsPrivacyModule.getAlwaysOnVpnPackage()
            if (alwaysOnVpnPackage != null) {
                val vpnApp = if (alwaysOnVpnPackage == permissionsPrivacyModule.getLegacyVpnDummyPackage()) {
                    ApplicationDescription(
                        packageName = alwaysOnVpnPackage,
                        uid = -1,
                        profileId = -1,
                        profileType = ProfileType.MAIN,
                        label = "PTP, L2TP/IPSec or IPSec",
                        icon = null
                    )
                } else {
                    try {
                        permissionsPrivacyModule.getApplicationDescription(
                            packageName = alwaysOnVpnPackage,
                            withIcon = false
                        )
                    } catch (e: Exception) {
                        Timber.e(e, "Can't getApplicationDescription for package: $alwaysOnVpnPackage")
                        ApplicationDescription(
                            packageName = alwaysOnVpnPackage,
                            uid = -1,
                            profileId = -1,
                            profileType = ProfileType.MAIN,
                            label = null,
                            icon = null
                        )
                    }
                }

                localStateRepository.emitOtherVpnRunning(vpnApp)
                localStateRepository.toggleIpScrambling(enabled = false)
                return
            }
        }

        localStateRepository.emitStartVpnDisclaimer(IpScrambling())
        startVpnService(IpScrambling())
    }

    override fun startVpnService(feature: MainFeatures) {
        localStateRepository.internetPrivacyMode.value = FeatureState.STARTING
        orbotSupervisor.setDNSFilter(null)
        orbotSupervisor.start()
    }

    override suspend fun cancelStartVpnService(feature: MainFeatures) {
        localStateRepository.toggleIpScrambling(enabled = false)
    }
}
