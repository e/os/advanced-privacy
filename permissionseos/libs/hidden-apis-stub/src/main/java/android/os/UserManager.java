/*
 * Copyright (C) 2024 E FOUNDATION
 * Copyright (C) 2022 MURENA SAS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package android.os;

import android.annotation.TargetApi;
import android.content.pm.UserInfo;

import androidx.annotation.DeprecatedSinceApi;
import androidx.annotation.RequiresPermission;
import java.util.List;

// Stub based on:
// https://gitlab.e.foundation/e/os/android_frameworks_base/-/blob/[SDK_VERSION]/core/java/android/os/UserManager.java
public class UserManager {

    // Since API 33, android.permission.QUERY_USERS can be used as an alternative
    // to "android.permission.MANAGE_USERS"
    @TargetApi(29)
    @DeprecatedSinceApi(
            api = 36,
            message = "Check availability in SDK36"
    )
    @RequiresPermission("android.permission.MANAGE_USERS")
    public List<UserInfo> getProfiles(int userHandle) {
        return null;
    }

    // Since API 33, android.permission.QUERY_USERS can be used as an alternative
    // to "android.permission.MANAGE_USERS"
    @TargetApi(29)
    @DeprecatedSinceApi(
            api = 36,
            message = "Check availability in SDK36"
    )
    @RequiresPermission("android.permission.MANAGE_USERS")
    public boolean isManagedProfile(int userId) {
        return false;
    }
}
