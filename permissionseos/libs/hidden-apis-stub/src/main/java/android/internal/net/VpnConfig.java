/*
 * Copyright (C) 2025 MURENA SAS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package android.internal.net;

// Stub based on:
// https://gitlab.e.foundation/e/os/android_frameworks_base/-/blob/[VERSION]/core/java/com/android/internal/net/VpnConfig.java
public class VpnConfig {

    // Availability checked from SDK29 to SDK 35
    public static final String LEGACY_VPN = "[Legacy VPN]";
}
