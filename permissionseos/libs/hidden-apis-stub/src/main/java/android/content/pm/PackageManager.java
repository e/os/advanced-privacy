/*
 * Copyright (C) 2024 E FOUNDATION
 * Copyright (C) 2022 MURENA SAS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package android.content.pm;

import android.annotation.TargetApi;
import android.graphics.drawable.Drawable;
import android.os.UserHandle;

import androidx.annotation.DeprecatedSinceApi;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresPermission;

import java.util.List;
import android.util.AndroidException;

// Stub based on:
// https://gitlab.e.foundation/e/os/android_frameworks_base/-/blob/[SDK_VERSION]/core/java/android/content/pm/PackageManager.java
public abstract class PackageManager {

    @TargetApi(29)
    @DeprecatedSinceApi(
            api = 36,
            message = "Check availability in SDK36"
    )
    public static class NameNotFoundException extends AndroidException {
        public NameNotFoundException() {
        }

        public NameNotFoundException(String name) {
            super(name);
        }
    }


    @TargetApi(29)
    @DeprecatedSinceApi(
            api = 36,
            message = "Check availability in SDK36"
    )
    @RequiresPermission("android.permission.GRANT_RUNTIME_PERMISSIONS")
    public abstract void grantRuntimePermission(
        @NonNull String packageName,
        @NonNull String permissionName,
        @NonNull UserHandle user
    );

    @TargetApi(29)
    @DeprecatedSinceApi(
            api = 36,
            message = "Check availability in SDK36"
    )
    @RequiresPermission("android.permission.REVOKE_RUNTIME_PERMISSIONS")
    public abstract void revokeRuntimePermission(
        @NonNull String packageName,
        @NonNull String permissionName,
        @NonNull UserHandle user
    );

    @TargetApi(29)
    @DeprecatedSinceApi(
        api = 36,
        message = "Check availability in SDK36, since SDK33, " +
                "Use {@link #getApplicationInfoAsUser(String, ApplicationInfoFlags, int)} " +
                "when long flags are needed."
    )
    public abstract ApplicationInfo getApplicationInfoAsUser(
        @NonNull String packageName,
        int flags,
        int userId
    ) throws NameNotFoundException;


    @TargetApi(29)
    @DeprecatedSinceApi(
            api = 36,
            message = "Check availability in SDK36, since SDK33, " +
                    "Use {@link #getInstalledPackagesAsUser(PackageInfoFlags, int)} " +
                    "when long flags are needed."
    )
    @RequiresPermission("android.permission.INTERACT_ACROSS_USERS_FULL")
    public abstract List<PackageInfo> getInstalledPackagesAsUser(int flags, int userId);

    // Public
    public abstract List<PackageInfo> getInstalledPackages(int flags);

    // Public
    @NonNull
    public abstract Drawable getUserBadgedIcon(
        @NonNull Drawable drawable,
        @NonNull UserHandle user
    );

    // Public
    public static final int GET_PERMISSIONS = 0x00001000;
}
