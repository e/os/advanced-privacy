/*
 * Copyright (C) 2024 E FOUNDATION
 * Copyright (C) 2022 MURENA SAS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package android.content.pm;

import android.annotation.TargetApi;
import android.os.UserHandle;

import androidx.annotation.DeprecatedSinceApi;

public class UserInfo {
    public int id;

    @TargetApi(29)
    @DeprecatedSinceApi(
            api = 36,
            message = "Check availability in SDK36"
    )
    public UserHandle getUserHandle() {
        return null;
    }
}
