/*
 * Copyright (C) 2022 - 2025 E FOUNDATION
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package android.app;

import android.annotation.TargetApi;

import androidx.annotation.DeprecatedSinceApi;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresPermission;

// Stub based on:
// https://gitlab.e.foundation/e/os/android_frameworks_base/-/blob/[SDK_VERSION]/core/java/android/app/AppOpsManager.java
public class AppOpsManager {

    public static final int OP_NONE = -1;

    @TargetApi(29)
    @DeprecatedSinceApi(
            api = 36,
            message = "Check availability in SDK36"
    )
    public static int strOpToOp(@NonNull String op) {
        return 0;
    }

    @TargetApi(29)
    @DeprecatedSinceApi(
            api = 36,
            message = "Check availability in SDK36"
    )
    @RequiresPermission("android.permission.MANAGE_APP_OPS_MODES")
    public void setMode(int code, int uid, String packageName, int mode) {}
}
