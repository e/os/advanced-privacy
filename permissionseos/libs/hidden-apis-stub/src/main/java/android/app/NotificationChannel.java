/*
 * Copyright (C) 2024 E FOUNDATION
 * Copyright (C) 2022 MURENA SAS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package android.app;

import android.annotation.TargetApi;

import androidx.annotation.DeprecatedSinceApi;

// Stub based on:
// https://gitlab.e.foundation/e/os/android_frameworks_base/-/blob/[SDK_VERSION]/core/java/android/app/NotificationChannel.java
public class NotificationChannel {

    @TargetApi(29)
    @DeprecatedSinceApi(api = 30, message = "Use setBlockable() instead.")
    public void setBlockableSystem(boolean blockableSystem) {}

    // Public in API 33.
    @TargetApi(30)
    public void setBlockable(boolean blockable) {}

}
