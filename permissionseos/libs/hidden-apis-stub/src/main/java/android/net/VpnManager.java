/*
 * Copyright (C) 2022 - 2024 E FOUNDATION
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package android.net;

import android.annotation.TargetApi;

import androidx.annotation.DeprecatedSinceApi;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresPermission;

// Stub based on:
// https://gitlab.e.foundation/e/os/android_frameworks_base/-/blob/[SDK_VERSION]/core/java/android/net/VpnManager.java
public class VpnManager {
    public static final int TYPE_VPN_SERVICE = 1;

    @TargetApi(31)
    @DeprecatedSinceApi(
            api = 36,
            message = "Check availability in SDK36"
    )
    public boolean prepareVpn(
        @Nullable String oldPackage,
        @Nullable String newPackage,
        int userId
    ) {
        return true;
    }

    @TargetApi(31)
    @DeprecatedSinceApi(
            api = 36,
            message = "Check availability in SDK36"
    )
    public void setVpnPackageAuthorization(
            String packageName,
            int userId,
            int vpnType
    ) {}

    @TargetApi(31)
    @DeprecatedSinceApi(
            api = 36,
            message = "Check availability in SDK36"
    )
    @RequiresPermission("android.permission.CONTROL_ALWAYS_ON_VPN")
    public String getAlwaysOnVpnPackageForUser(int userId) {
        return null;
    }
}
