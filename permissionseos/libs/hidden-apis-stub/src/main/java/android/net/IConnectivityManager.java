/*
 * Copyright (C) 2022 E FOUNDATION
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package android.net;

import android.annotation.TargetApi;

import android.os.Binder;
import android.os.IBinder;
import android.os.RemoteException;

import androidx.annotation.DeprecatedSinceApi;

// Stub based on:
// https://gitlab.e.foundation/e/os/android_frameworks_base/-/blob/[SDK_VERSION]/core/java/android/net/IConnectivityManager.java
public interface IConnectivityManager {

    @TargetApi(29)
    @DeprecatedSinceApi(
            api = 31,
            message = "Moved to android.net.VpnManager"
    )
    boolean prepareVpn(String oldPackage, String newPackage, int userId) throws RemoteException;

    @TargetApi(29)
    @DeprecatedSinceApi(
        api = 30,
        message = "Use instead setVpnPackageAuthorization(String packageName, int userId, int vpnType)"
    )
    void setVpnPackageAuthorization(String packageName, int userId, boolean authorized) throws RemoteException;

    @TargetApi(30)
    @DeprecatedSinceApi(
            api = 31,
            message = "Moved to android.net.VpnManager"
    )
    void setVpnPackageAuthorization(String packageName, int userId, int vpnType) throws RemoteException;

    @TargetApi(29)
    @DeprecatedSinceApi(
            api = 31,
            message = "Moved to android.net.VpnManager"
    )
    public String getAlwaysOnVpnPackage(int userId) throws RemoteException;

    public abstract static class Stub extends Binder implements IConnectivityManager {
        public static IConnectivityManager asInterface(IBinder obj) {
            return null;
        }
    }
}
