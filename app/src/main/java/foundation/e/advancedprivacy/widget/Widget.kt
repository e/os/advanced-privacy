/*
 * Copyright (C) 2022 - 2024 E FOUNDATION
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.advancedprivacy

import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.Context
import android.os.Bundle
import foundation.e.advancedprivacy.domain.usecases.GetQuickPrivacyStateUseCase
import foundation.e.advancedprivacy.domain.usecases.TrackersStatisticsUseCase
import foundation.e.advancedprivacy.widget.State
import foundation.e.advancedprivacy.widget.render
import foundation.e.advancedprivacy.widget.renderAll
import java.time.temporal.ChronoUnit
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.merge
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.sample
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch

/**
 * Implementation of App Widget functionality.
 */
class Widget : AppWidgetProvider() {

    override fun onUpdate(context: Context, appWidgetManager: AppWidgetManager, appWidgetIds: IntArray) {
        appWidgetIds.forEach { id ->
            render(context, state.value, appWidgetManager, id)
        }
    }

    override fun onEnabled(context: Context) {
        // Enter relevant functionality for when the first widget is created
    }

    override fun onDisabled(context: Context) {
        // Enter relevant functionality for when the last widget is disabled
    }

    companion object {
        private var updateWidgetJob: Job? = null

        private var state: StateFlow<State> = MutableStateFlow(State())

        private const val DARK_TEXT_KEY = "foundation.e.blisslauncher.WIDGET_OPTION_DARK_TEXT"
        var isDarkText = false

        @OptIn(FlowPreview::class)
        private fun initState(
            getPrivacyStateUseCase: GetQuickPrivacyStateUseCase,
            trackersStatisticsUseCase: TrackersStatisticsUseCase,
            coroutineScope: CoroutineScope
        ): StateFlow<State> {
            return combine(
                getPrivacyStateUseCase.trackerMode,
                getPrivacyStateUseCase.locationMode,
                getPrivacyStateUseCase.ipScramblingMode
            ) { trackerMode, locationMode, ipScramblingMode ->
                State(
                    trackerMode = trackerMode,
                    locationMode = locationMode,
                    ipScramblingMode = ipScramblingMode
                )
            }.sample(50)
                .combine(
                    merge(
                        trackersStatisticsUseCase.listenUpdates()
                            .onStart { emit(Unit) }
                            .debounce(5000),
                        flow {
                            while (true) {
                                emit(Unit)
                                delay(ChronoUnit.HOURS.duration.toMillis())
                            }
                        }

                    )
                ) { state, _ ->
                    state.copy(
                        blockedCallsCount = trackersStatisticsUseCase.getLastMonthBlockedLeaksCount(),
                        appsWithCallsCount = trackersStatisticsUseCase.getLastMonthAppsWithBlockedLeaksCount()
                    )
                }.stateIn(
                    scope = coroutineScope,
                    started = SharingStarted.Eagerly,
                    initialValue = State()
                )
        }

        @OptIn(DelicateCoroutinesApi::class)
        fun startListening(
            appContext: Context,
            getPrivacyStateUseCase: GetQuickPrivacyStateUseCase,
            trackersStatisticsUseCase: TrackersStatisticsUseCase
        ) {
            state = initState(
                getPrivacyStateUseCase,
                trackersStatisticsUseCase,
                GlobalScope
            )

            updateWidgetJob?.cancel()
            updateWidgetJob = GlobalScope.launch(Dispatchers.Main) {
                state.collect {
                    renderAll(appContext, it)
                }
            }
        }
    }

    override fun onAppWidgetOptionsChanged(context: Context, appWidgetManager: AppWidgetManager, appWidgetId: Int, newOptions: Bundle?) {
        super.onAppWidgetOptionsChanged(context, appWidgetManager, appWidgetId, newOptions)
        if (newOptions != null) {
            isDarkText = newOptions.getBoolean(DARK_TEXT_KEY)
        }

        render(context, state.value, appWidgetManager, appWidgetId)
    }
}
