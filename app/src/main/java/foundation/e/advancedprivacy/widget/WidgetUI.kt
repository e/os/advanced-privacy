/*
 * Copyright (C) 2023-2024 MURENA SAS
 * Copyright (C) 2022 E FOUNDATION
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.advancedprivacy.widget

import android.app.PendingIntent
import android.app.PendingIntent.FLAG_IMMUTABLE
import android.app.PendingIntent.FLAG_UPDATE_CURRENT
import android.appwidget.AppWidgetManager
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.graphics.drawable.Icon
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.RemoteViews
import androidx.annotation.StringRes
import foundation.e.advancedprivacy.R
import foundation.e.advancedprivacy.Widget
import foundation.e.advancedprivacy.Widget.Companion.isDarkText
import foundation.e.advancedprivacy.common.BigNumberFormatter
import foundation.e.advancedprivacy.domain.entities.FeatureMode
import foundation.e.advancedprivacy.domain.entities.FeatureState
import foundation.e.advancedprivacy.main.MainActivity
import foundation.e.advancedprivacy.widget.WidgetCommandReceiver.Companion.ACTION_TOGGLE_IPSCRAMBLING
import foundation.e.advancedprivacy.widget.WidgetCommandReceiver.Companion.ACTION_TOGGLE_LOCATION
import foundation.e.advancedprivacy.widget.WidgetCommandReceiver.Companion.ACTION_TOGGLE_TRACKERS
import foundation.e.advancedprivacy.widget.WidgetCommandReceiver.Companion.PARAM_FEATURE_ENABLED

data class State(
    val trackerMode: FeatureMode = FeatureMode.VULNERABLE,
    val locationMode: FeatureMode = FeatureMode.VULNERABLE,
    val ipScramblingMode: FeatureState = FeatureState.STOPPING,
    val blockedCallsCount: Int = 0,
    val appsWithCallsCount: Int = 0
)

fun renderAll(context: Context, state: State) {
    val appWidgetManager = AppWidgetManager.getInstance(context)
    appWidgetManager.getAppWidgetIds(
        ComponentName(context, Widget::class.java)
    ).forEach { id ->
        render(context, state, appWidgetManager, id)
    }
}

fun render(context: Context, state: State, appWidgetManager: AppWidgetManager, widgetId: Int) {
    val numberFormatter = BigNumberFormatter(context)

    val views = buildLayout(context, appWidgetManager, widgetId)

    applyDarkText(context, isDarkText, views)

    views.apply {
        val openPIntent = PendingIntent.getActivity(
            context,
            REQUEST_CODE_DASHBOARD,
            Intent(context, MainActivity::class.java),
            FLAG_IMMUTABLE or FLAG_UPDATE_CURRENT
        )
        setOnClickPendingIntent(R.id.settings_btn, openPIntent)
        setOnClickPendingIntent(R.id.widget_container, openPIntent)

        val leaksLabel = context.getString(R.string.widget_data_blocked_trackers_secondary)
        val countStr = numberFormatter.format(state.blockedCallsCount).toString()
        if (shouldSplitLeaksCountInTwoLines(countStr, leaksLabel)) {
            setTextViewText(R.id.data_blocked_trackers_number, countStr)
            setTextViewText(R.id.data_blocked_trackers_secondary, leaksLabel)
            setViewVisibility(R.id.data_blocked_trackers_number, VISIBLE)
        } else {
            setViewVisibility(R.id.data_blocked_trackers_number, GONE)
            setTextViewText(
                R.id.data_blocked_trackers_secondary,
                buildDataSecondarySpan(context, isDarkText, countStr, R.string.widget_data_blocked_trackers_secondary)
            )
        }

        setTextViewText(
            R.id.data_apps_secondary,
            buildDataSecondarySpan(
                context,
                isDarkText,
                state.appsWithCallsCount.toString(),
                R.string.widget_data_apps_secondary
            )
        )

        val trackersEnabled = state.trackerMode != FeatureMode.VULNERABLE

        setSwitchState(views, R.id.toggle_trackers, trackersEnabled)

        setOnClickPendingIntent(
            R.id.trackers_control,
            PendingIntent.getBroadcast(
                context,
                REQUEST_CODE_TOGGLE_TRACKERS,
                Intent(context, WidgetCommandReceiver::class.java).apply {
                    action = ACTION_TOGGLE_TRACKERS
                    putExtra(PARAM_FEATURE_ENABLED, !trackersEnabled)
                },
                FLAG_IMMUTABLE or FLAG_UPDATE_CURRENT
            )
        )

        val locationEnabled = state.locationMode != FeatureMode.VULNERABLE
        setSwitchState(views, R.id.toggle_location, locationEnabled)

        setOnClickPendingIntent(
            R.id.fake_location,
            PendingIntent.getBroadcast(
                context,
                REQUEST_CODE_TOGGLE_LOCATION,
                Intent(context, WidgetCommandReceiver::class.java).apply {
                    action = ACTION_TOGGLE_LOCATION
                    putExtra(PARAM_FEATURE_ENABLED, !locationEnabled)
                },
                FLAG_IMMUTABLE or FLAG_UPDATE_CURRENT
            )
        )

        setSwitchState(views, R.id.toggle_ipscrambling, state.ipScramblingMode.isChecked)

        setOnClickPendingIntent(
            R.id.ipscrambling,
            PendingIntent.getBroadcast(
                context,
                REQUEST_CODE_TOGGLE_IPSCRAMBLING,
                Intent(context, WidgetCommandReceiver::class.java).apply {
                    action = ACTION_TOGGLE_IPSCRAMBLING
                    putExtra(PARAM_FEATURE_ENABLED, !state.ipScramblingMode.isChecked)
                },
                FLAG_IMMUTABLE or FLAG_UPDATE_CURRENT
            )
        )
    }

    appWidgetManager.updateAppWidget(widgetId, views)
}

private const val REQUEST_CODE_DASHBOARD = 1
private const val REQUEST_CODE_TOGGLE_TRACKERS = 4
private const val REQUEST_CODE_TOGGLE_LOCATION = 5
private const val REQUEST_CODE_TOGGLE_IPSCRAMBLING = 6

private const val NARROW_MAXWIDTH_DP_BREAKPOINT = 240

private const val DATA_BLOCKED_TWO_LINES_THRESHOLD_DIGITS = 3
private const val DATA_BLOCKED_TWO_LINES_THRESHOLD_LABEL = 14

private fun buildLayout(context: Context, appWidgetManager: AppWidgetManager, widgetId: Int): RemoteViews {
    val width = appWidgetManager.getAppWidgetOptions(widgetId)
        .getInt(AppWidgetManager.OPTION_APPWIDGET_MIN_WIDTH)

    return RemoteViews(
        context.packageName,
        when (width) {
            in 1..NARROW_MAXWIDTH_DP_BREAKPOINT ->
                R.layout.widget_narrow
            else -> R.layout.widget_large
        }
    )
}

private fun applyDarkText(context: Context, isDarkText: Boolean, views: RemoteViews) {
    views.apply {
        // FFFFFF %87
        val primaryColor = context.getColor(
            if (isDarkText) {
                R.color.on_surface_medium_emphasis_light
            } else {
                R.color.on_surface_high_emphasis
            }
        )

        listOf(
            R.id.widget_title,
            R.id.data_blocked_trackers_primary,
            R.id.data_blocked_trackers_number,
            R.id.data_apps_primary,
            R.id.trackers_control_label,
            R.id.fake_location_label,
            R.id.ipscrambling_label
        ).forEach {
            setTextColor(it, primaryColor)
        }

        listOf(
            R.id.settings_btn to R.drawable.ic_settings,
            R.id.data_blocked_trackers_icon to R.drawable.ic_block_24,
            R.id.data_apps_icon to R.drawable.ic_apps_24
        ).forEach { (viewId, drawableId) ->
            setImageViewIcon(
                viewId,
                Icon.createWithResource(context, drawableId).apply { setTint(primaryColor) }
            )
        }

        // FFFFFF %60
        val secondaryColor = context.getColor(if (isDarkText) R.color.on_surface_disabled_light else R.color.on_primary_medium_emphasis)

        listOf(
            R.id.period_label,
            R.id.data_blocked_trackers_secondary
        ).forEach { id ->
            setTextColor(id, secondaryColor)
        }
    }
}

private fun shouldSplitLeaksCountInTwoLines(countStr: String, leaksLabel: String): Boolean {
    return countStr.length > DATA_BLOCKED_TWO_LINES_THRESHOLD_DIGITS &&
        (countStr.length + leaksLabel.length) > DATA_BLOCKED_TWO_LINES_THRESHOLD_LABEL
}

private fun buildDataSecondarySpan(context: Context, isDarkText: Boolean, countStr: String, @StringRes secondaryRes: Int): CharSequence {
    val primaryColor = context.getColor(
        if (isDarkText) {
            R.color.on_surface_medium_emphasis_light
        } else {
            R.color.on_surface_high_emphasis
        }
    )

    val secondaryColor = context.getColor(if (isDarkText) R.color.on_surface_disabled_light else R.color.on_primary_medium_emphasis)

    val secondary = context.getString(secondaryRes)

    val spannable = SpannableString("$countStr $secondary")

    spannable.setSpan(
        ForegroundColorSpan(primaryColor),
        0,
        countStr.length,
        Spannable.SPAN_INCLUSIVE_EXCLUSIVE
    )

    spannable.setSpan(
        ForegroundColorSpan(secondaryColor),
        countStr.length,
        spannable.length,
        Spannable.SPAN_INCLUSIVE_EXCLUSIVE
    )
    return spannable
}

private fun setSwitchState(views: RemoteViews, switchId: Int, checked: Boolean) {
    views.setImageViewResource(
        switchId,
        if (checked) {
            R.drawable.ic_switch_enabled_raster
        } else {
            R.drawable.ic_switch_disabled_raster
        }
    )
}
