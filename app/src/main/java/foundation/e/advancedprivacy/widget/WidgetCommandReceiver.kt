/*
 * Copyright (C) 2022 - 2024 E FOUNDATION
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.advancedprivacy.widget

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import foundation.e.advancedprivacy.core.utils.goAsync
import foundation.e.advancedprivacy.domain.usecases.GetQuickPrivacyStateUseCase
import kotlinx.coroutines.CoroutineScope
import org.koin.java.KoinJavaComponent.get

class WidgetCommandReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        val backgroundScope = get<CoroutineScope>(CoroutineScope::class.java)
        val getQuickPrivacyStateUseCase = get<GetQuickPrivacyStateUseCase>(GetQuickPrivacyStateUseCase::class.java)

        goAsync(backgroundScope) {
            val featureEnabled = intent?.extras?.let { bundle ->
                if (bundle.containsKey(PARAM_FEATURE_ENABLED)) {
                    bundle.getBoolean(PARAM_FEATURE_ENABLED)
                } else {
                    null
                }
            }

            when (intent?.action) {
                ACTION_TOGGLE_TRACKERS -> getQuickPrivacyStateUseCase.toggleTrackers(featureEnabled)
                ACTION_TOGGLE_LOCATION -> getQuickPrivacyStateUseCase.toggleLocation(featureEnabled)
                ACTION_TOGGLE_IPSCRAMBLING -> getQuickPrivacyStateUseCase.toggleIpScrambling(featureEnabled)
                else -> {}
            }
        }
    }

    companion object {
        const val ACTION_TOGGLE_TRACKERS = "toggle_trackers"
        const val ACTION_TOGGLE_LOCATION = "toggle_location"
        const val ACTION_TOGGLE_IPSCRAMBLING = "toggle_ipscrambling"
        const val PARAM_FEATURE_ENABLED = "param_feature_enabled"
    }
}
