/*
 * Copyright (C) 2023 MURENA SAS
 * Copyright (C) 2021 - 2024 E FOUNDATION
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.advancedprivacy.domain.usecases

import foundation.e.advancedprivacy.domain.entities.ApplicationDescription
import foundation.e.advancedprivacy.domain.entities.FeatureMode
import foundation.e.advancedprivacy.domain.entities.FeatureState
import foundation.e.advancedprivacy.domain.repositories.LocalStateRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.combine

class GetQuickPrivacyStateUseCase(
    private val localStateRepository: LocalStateRepository
) {
    val trackerMode: Flow<FeatureMode> = combine(
        localStateRepository.blockTrackers,
        localStateRepository.areAllTrackersBlocked
    ) { isBlockTrackers, isAllTrackersBlocked ->
        when {
            isBlockTrackers && isAllTrackersBlocked -> FeatureMode.DENIED
            isBlockTrackers && !isAllTrackersBlocked -> FeatureMode.CUSTOM
            else -> FeatureMode.VULNERABLE
        }
    }

    val locationMode: Flow<FeatureMode> = combine(
        localStateRepository.fakeLocationEnabled,
        localStateRepository.fakeLocationWhitelistedApps
    ) { enabled, whitelist ->
        when {
            !enabled -> FeatureMode.VULNERABLE
            whitelist.isEmpty() -> FeatureMode.DENIED
            else -> FeatureMode.CUSTOM
        }
    }

    val ipScramblingMode: Flow<FeatureState> =
        localStateRepository.internetPrivacyMode

    suspend fun toggleTrackers(enabled: Boolean?) {
        localStateRepository.toggleBlockTrackers(enabled)
    }

    suspend fun toggleLocation(enabled: Boolean?) {
        localStateRepository.toggleFakeLocation(enabled)
    }

    suspend fun toggleIpScrambling(enabled: Boolean?) {
        localStateRepository.toggleIpScrambling(enabled)
    }

    val otherVpnRunning: SharedFlow<ApplicationDescription> = localStateRepository.otherVpnRunning
}
