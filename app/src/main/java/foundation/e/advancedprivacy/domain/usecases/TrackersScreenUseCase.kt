/*
 * Copyright (C) 2023 - 2024 MURENA SAS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.advancedprivacy.domain.usecases

import foundation.e.advancedprivacy.domain.repositories.LocalStateRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class TrackersScreenUseCase(
    private val localStateRepository: LocalStateRepository,
    private val backgroundDispatcher: CoroutineDispatcher = Dispatchers.IO
) {

    suspend fun getLastPosition(): Int = withContext(backgroundDispatcher) {
        localStateRepository.getTrackersScreenLastPosition()
    }

    suspend fun savePosition(currentPosition: Int) = withContext(backgroundDispatcher) {
        localStateRepository.setTrackersScreenLastPosition(currentPosition)
    }

    fun getTrackerTabStartPosition(): Int {
        return localStateRepository.trackersScreenTabStartPosition
    }

    fun resetTrackerTabStartPosition() {
        localStateRepository.trackersScreenTabStartPosition = -1
    }

    suspend fun preselectTab(periodPosition: Int, tabPosition: Int) = withContext(backgroundDispatcher) {
        localStateRepository.setTrackersScreenLastPosition(periodPosition)
        localStateRepository.trackersScreenTabStartPosition = tabPosition
    }
}
