/*
 * Copyright (C) 2023 MURENA SAS
 * Copyright (C) 2021 - 2024 E FOUNDATION
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.advancedprivacy.domain.usecases

import foundation.e.advancedprivacy.data.repositories.AppListRepository
import foundation.e.advancedprivacy.domain.entities.DisplayableApp
import foundation.e.advancedprivacy.domain.entities.LocationMode
import foundation.e.advancedprivacy.domain.entities.ToggleableApp
import foundation.e.advancedprivacy.domain.repositories.LocalStateRepository
import foundation.e.advancedprivacy.dummy.CityDataSource
import kotlin.random.Random
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch

class FakeLocationStateUseCase(
    private val localStateRepository: LocalStateRepository,
    private val citiesRepository: CityDataSource,
    private val appListRepository: AppListRepository,
    coroutineScope: CoroutineScope
) {
    private val _configuredLocationMode = MutableStateFlow<Triple<LocationMode, Float?, Float?>>(
        Triple(LocationMode.REAL_LOCATION, null, null)
    )
    val configuredLocationMode: StateFlow<Triple<LocationMode, Float?, Float?>> = _configuredLocationMode

    init {
        coroutineScope.launch {
            localStateRepository.fakeLocationEnabled.collect {
                applySettings(it, localStateRepository.getFakeLocation())
            }
        }
    }

    private fun applySettings(isEnabled: Boolean, fakeLocation: Pair<Float, Float>, isSpecificLocation: Boolean = false) {
        val locationMode = when {
            !isEnabled -> LocationMode.REAL_LOCATION
            isRandomLocation(fakeLocation, isSpecificLocation) -> LocationMode.RANDOM_LOCATION
            else -> LocationMode.SPECIFIC_LOCATION
        }

        _configuredLocationMode.value = Triple(locationMode, fakeLocation.first, fakeLocation.second)
    }

    private fun isRandomLocation(fakeLocation: Pair<Float, Float>, isSpecificLocation: Boolean): Boolean {
        return fakeLocation in citiesRepository.citiesLocationsList && !isSpecificLocation
    }

    suspend fun setSpecificLocation(latitude: Float, longitude: Float) {
        setFakeLocation(latitude to longitude, true)
    }

    suspend fun setRandomLocation() {
        val randomIndex = Random.nextInt(citiesRepository.citiesLocationsList.size)
        val location = citiesRepository.citiesLocationsList[randomIndex]

        setFakeLocation(location)
    }

    private suspend fun setFakeLocation(location: Pair<Float, Float>, isSpecificLocation: Boolean = false) {
        localStateRepository.setFakeLocation(location)
        localStateRepository.toggleFakeLocation(true)
        applySettings(true, location, isSpecificLocation)
    }

    suspend fun stopFakeLocation() {
        localStateRepository.toggleFakeLocation(false)
        applySettings(false, localStateRepository.getFakeLocation())
    }

    suspend fun toggleBlacklist(app: DisplayableApp) {
        localStateRepository.toggleAppFakeLocationWhitelisted(app)
    }

    suspend fun resetBlacklist() {
        localStateRepository.resetFakeLocationWhitelistedApp()
    }

    fun canResetBlacklist(): Flow<Boolean> = localStateRepository.fakeLocationWhitelistedApps.map {
        it.isNotEmpty()
    }

    fun appsWithBlacklist(): Flow<List<ToggleableApp>> {
        return combine(
            appListRepository.displayableApps.map { apps ->
                apps.filter { it.hasLocationPermission }.sortedBy { it.label.toString() }
            },
            localStateRepository.fakeLocationWhitelistedApps
        ) { apps, whitelist ->
            apps.map { app ->
                ToggleableApp(app = app, isOn = !app.apps.any { whitelist.contains(it.apId) })
            }
        }
    }
}
