/*
 * Copyright (C) 2023 MURENA SAS
 * Copyright (C) 2022 - 2024 E FOUNDATION
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.advancedprivacy.domain.usecases

import foundation.e.advancedprivacy.domain.entities.MainFeatures
import foundation.e.advancedprivacy.domain.entities.MainFeatures.FakeLocation
import foundation.e.advancedprivacy.domain.entities.MainFeatures.IpScrambling
import foundation.e.advancedprivacy.domain.entities.MainFeatures.TrackersControl
import foundation.e.advancedprivacy.domain.repositories.LocalStateRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.drop
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.merge

class ShowFeaturesWarningUseCase(
    private val localStateRepository: LocalStateRepository
) {

    fun showWarning(): Flow<MainFeatures> {
        return merge(
            localStateRepository.fakeLocationEnabled.drop(1).filter { it }
                .filter { it && !localStateRepository.isHideWarningLocation() }
                .map { FakeLocation },
            localStateRepository.startVpnDisclaimer.filter {
                (it is IpScrambling && !localStateRepository.isHideWarningIpScrambling()) ||
                    (it is TrackersControl && !localStateRepository.isHideWarningTrackers())
            }
        )
    }

    suspend fun doNotShowAgain(feature: MainFeatures) {
        when (feature) {
            is TrackersControl -> localStateRepository.hideWarningTrackers(true)
            is FakeLocation -> localStateRepository.hideWarningLocation(true)
            is IpScrambling -> localStateRepository.hideWarningIpScrambling(true)
        }
    }
}
