/*
 * Copyright (C) 2024 E FOUNDATION
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package foundation.e.advancedprivacy.domain.usecases

import foundation.e.advancedprivacy.data.repositories.AppListRepository
import foundation.e.advancedprivacy.domain.repositories.LocalStateRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.map

class FakeLocationForAppUseCase(
    private val appListRepository: AppListRepository,
    localStateRepository: LocalStateRepository,
    backgroundScope: CoroutineScope
) {
    // Cache these values to allow true sync exectution on getFakeLocationOrNull,
    // which is called by the ContentProvider
    private var fakeLocation: Pair<Float, Float>? = null
    private var whitelistedApp: Set<String> = emptySet()

    private val nullFakeLocationPkgs = listOf(
        AppListRepository.PNAME_MICROG_SERVICES_CORE,
        AppListRepository.PNAME_FUSED_LOCATION,
        AppListRepository.PNAME_ANDROID_SYSTEM
    )

    init {
        combine(
            localStateRepository.fakeLocationEnabled,
            localStateRepository.fakeLocation
        ) { enabled, latLon ->
            fakeLocation = if (enabled) latLon else null
        }.launchIn(backgroundScope)

        localStateRepository.fakeLocationWhitelistedApps.map { whitelistedApp = it }.launchIn(backgroundScope)
    }

    fun getFakeLocationOrNull(packageName: String?, uid: Int): Pair<Float, Float>? {
        if (packageName == null || fakeLocation == null || packageName in nullFakeLocationPkgs) {
            return null
        }

        val app = appListRepository.getApp(uid)
        return if (app?.apId != null && app.apId in whitelistedApp) {
            null
        } else {
            fakeLocation
        }
    }
}
