/*
 * Copyright (C) 2022 - 2024 MURENA SAS
 * Copyright (C) 2021 E FOUNDATION
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.advancedprivacy.domain.usecases

import foundation.e.advancedprivacy.R
import foundation.e.advancedprivacy.common.throttleFirst
import foundation.e.advancedprivacy.data.repositories.AppListRepository
import foundation.e.advancedprivacy.data.repositories.ResourcesRepository
import foundation.e.advancedprivacy.domain.entities.ApplicationDescription
import foundation.e.advancedprivacy.domain.entities.DisplayableApp
import foundation.e.advancedprivacy.domain.entities.TrackersPeriodicStatistics
import foundation.e.advancedprivacy.features.trackers.Period
import foundation.e.advancedprivacy.trackers.data.StatsDatabase
import foundation.e.advancedprivacy.trackers.data.TrackersRepository
import foundation.e.advancedprivacy.trackers.data.WhitelistRepository
import foundation.e.advancedprivacy.trackers.domain.entities.Tracker
import java.time.ZonedDateTime
import java.time.temporal.ChronoUnit
import kotlin.time.Duration
import kotlin.time.Duration.Companion.seconds
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.onStart

class TrackersStatisticsUseCase(
    private val whitelistRepository: WhitelistRepository,
    private val trackersRepository: TrackersRepository,
    private val appListRepository: AppListRepository,
    private val statsDatabase: StatsDatabase,
    private val resourcesRepository: ResourcesRepository
) {
    fun initAppList() {
        appListRepository.refreshAppDescriptions()
    }

    @OptIn(FlowPreview::class)
    fun listenUpdates(debounce: Duration = 1.seconds) = statsDatabase.newDataAvailable
        .throttleFirst(windowDuration = debounce)
        .onStart { emit(Unit) }

    private fun buildGraduations(period: Period): List<String?> {
        return when (period) {
            Period.DAY -> buildDayGraduations()
            Period.MONTH -> buildMonthGraduations()
            Period.YEAR -> buildYearGraduations()
        }
    }

    private fun buildDayGraduations(): List<String?> {
        val formatter = resourcesRepository.getFormatter(R.string.trackers_graph_hours_period_format)

        val periods = mutableListOf<String?>()
        var end = ZonedDateTime.now()
        for (i in 1..24) {
            val start = end.truncatedTo(ChronoUnit.HOURS)
            periods.add(if (start.hour % 6 == 0) formatter.format(start) else null)
            end = start.minus(1, ChronoUnit.MINUTES)
        }
        return periods.reversed()
    }

    private fun buildMonthGraduations(): List<String?> {
        val formatter = resourcesRepository.getFormatter(
            R.string.trackers_graph_month_graduations_format
        )

        val periods = mutableListOf<String?>()
        var end = ZonedDateTime.now()
        for (i in 1..30) {
            val start = end.truncatedTo(ChronoUnit.DAYS)
            periods.add(if ((start.dayOfMonth) % 6 == 0) formatter.format(start) else null)
            end = start.minus(1, ChronoUnit.HOURS)
        }

        return periods.reversed()
    }

    private fun buildYearGraduations(): List<String?> {
        val formatter = resourcesRepository.getFormatter(R.string.trackers_graph_year_graduations_format)

        val periods = mutableListOf<String?>()
        var end = ZonedDateTime.now()
        for (i in 1..12) {
            val start = end.truncatedTo(ChronoUnit.DAYS).let {
                it.minusDays(it.dayOfMonth.toLong())
            }
            periods.add(if (start.monthValue % 3 == 0) formatter.format(start) else null)
            end = start.minus(1, ChronoUnit.DAYS)
        }

        return periods.reversed()
    }

    private fun buildLabels(period: Period): List<String> {
        return when (period) {
            Period.DAY -> buildDayLabels()
            Period.MONTH -> buildMonthLabels()
            Period.YEAR -> buildYearLabels()
        }
    }

    private fun buildDayLabels(): List<String> {
        val formatter = resourcesRepository.getFormatter(R.string.trackers_graph_hours_period_format)

        val periods = mutableListOf<String>()
        var end = ZonedDateTime.now()
        for (i in 1..24) {
            val start = end.truncatedTo(ChronoUnit.HOURS)
            periods.add("${formatter.format(start)} - ${formatter.format(end)}")
            end = start.minus(1, ChronoUnit.MINUTES)
        }
        return periods.reversed()
    }

    private fun buildMonthLabels(): List<String> {
        val formater = resourcesRepository.getFormatter(R.string.trackers_graph_days_period_format)

        val periods = mutableListOf<String>()
        var day = ZonedDateTime.now().truncatedTo(ChronoUnit.DAYS)
        for (i in 1..30) {
            periods.add(formater.format(day))
            day = day.minus(1, ChronoUnit.DAYS)
        }
        return periods.reversed()
    }

    private fun buildYearLabels(): List<String> {
        val formater = resourcesRepository.getFormatter(R.string.trackers_graph_months_period_format)

        val periods = mutableListOf<String>()
        var month = ZonedDateTime.now().truncatedTo(ChronoUnit.DAYS).withDayOfMonth(1)
        for (i in 1..12) {
            periods.add(formater.format(month))
            month = month.minus(1, ChronoUnit.MONTHS)
        }
        return periods.reversed()
    }

    suspend fun getGraphData(period: Period): TrackersPeriodicStatistics {
        return TrackersPeriodicStatistics(
            callsBlockedNLeaked = statsDatabase.getTrackersCallsOnPeriod(
                period.periodsCount,
                period.periodUnit
            ),
            periods = buildLabels(period),
            trackersCount = statsDatabase.getTrackersCount(period.periodsCount, period.periodUnit),
            trackersAllowedCount = statsDatabase.getLeakedTrackersCount(period.periodsCount, period.periodUnit),
            graduations = buildGraduations(period)
        )
    }

    suspend fun isWhiteListEmpty(app: DisplayableApp): Boolean {
        return app.apps.all { getWhiteList(it).isEmpty() }
    }

    suspend fun getLastMonthBlockedLeaksCount(): Int {
        return statsDatabase.getBlockedLeaksCount(Period.MONTH.periodsCount, Period.MONTH.periodUnit)
    }

    suspend fun getLastMonthAppsWithBlockedLeaksCount(): Int {
        return statsDatabase.getAppsWithBlockedLeaksCount(Period.MONTH.periodsCount, Period.MONTH.periodUnit)
    }

    private fun getWhiteList(app: ApplicationDescription): List<Tracker> {
        return whitelistRepository.getWhiteListForApp(app).mapNotNull {
            trackersRepository.getTracker(it)
        }
    }
}
