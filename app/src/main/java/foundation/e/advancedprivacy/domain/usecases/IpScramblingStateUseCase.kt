/*
 * Copyright (C) 2023 MURENA SAS
 * Copyright (C) 2021 - 2024 E FOUNDATION
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.advancedprivacy.domain.usecases

import foundation.e.advancedprivacy.data.repositories.AppListRepository
import foundation.e.advancedprivacy.domain.entities.DisplayableApp
import foundation.e.advancedprivacy.domain.entities.FeatureState
import foundation.e.advancedprivacy.domain.entities.ProfileType
import foundation.e.advancedprivacy.domain.entities.ToggleableApp
import foundation.e.advancedprivacy.domain.repositories.LocalStateRepository
import foundation.e.advancedprivacy.ipscrambler.OrbotSupervisor
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.update

class IpScramblingStateUseCase(
    private val orbotSupervisor: OrbotSupervisor,
    private val localStateRepository: LocalStateRepository,
    private val appListRepository: AppListRepository,
    private val backgroundScope: CoroutineScope
) {
    val internetPrivacyMode: StateFlow<FeatureState> = orbotSupervisor.state

    private val whitelistedPackages = MutableStateFlow(orbotSupervisor.appList)

    init {
        orbotSupervisor.requestStatus()

        orbotSupervisor.state.map {
            localStateRepository.internetPrivacyMode.value = it
        }.launchIn(backgroundScope)

        whitelistedPackages.map {
            orbotSupervisor.appList = it
        }.launchIn(backgroundScope)
    }

    suspend fun toggle(hideIp: Boolean) {
        localStateRepository.toggleIpScrambling(enabled = hideIp)
    }

    suspend fun getTorToggleableApp(): Flow<List<ToggleableApp>> {
        return combine(
            appListRepository.displayableApps.map { apps ->
                apps.filter { app ->
                    app.hasInternetPermission && app.profileType == ProfileType.MAIN
                }
            },
            whitelistedPackages
        ) { apps, pNames ->
            apps.map { app ->
                ToggleableApp(app = app, isOn = !app.isWhitelisted(pNames))
            }
        }
    }

    fun toggleBypassTor(app: DisplayableApp) {
        whitelistedPackages.update { whitelist ->
            val packageNames = app.apps.map { it.packageName }.toSet()
            if (app.isWhitelisted()) {
                whitelist.minus(packageNames)
            } else {
                whitelist.union(packageNames)
            }
        }
    }

    val availablesLocations: List<String> = orbotSupervisor.getAvailablesLocations().sorted()

    val exitCountry: String get() = orbotSupervisor.getExitCountryCode()

    suspend fun setExitCountry(locationId: String) {
        if (locationId != exitCountry) {
            orbotSupervisor.setExitCountryCode(locationId)
        }
    }

    private fun DisplayableApp.isWhitelisted(whitelistedPackageNames: Set<String> = whitelistedPackages.value): Boolean = apps.any {
        it.packageName in whitelistedPackageNames
    }
}
