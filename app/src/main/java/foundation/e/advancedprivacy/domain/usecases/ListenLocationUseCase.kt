/*
 * Copyright (C) 2024 E FOUNDATION
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package foundation.e.advancedprivacy.domain.usecases

import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import foundation.e.advancedprivacy.domain.entities.ApplicationDescription
import foundation.e.advancedprivacy.externalinterfaces.permissions.IPermissionsPrivacyModule
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.update
import timber.log.Timber

class ListenLocationUseCase(
    private val permissionsModule: IPermissionsPrivacyModule,
    private val appContext: Context,
    private val appDesc: ApplicationDescription
) {
    companion object {
        const val MIN_TIME_INTERVAL = 1000L
        const val MIN_DIST_INTERVAL = 0f
    }

    private val locationManager: LocationManager
        get() = appContext.getSystemService(LocationManager::class.java) as LocationManager

    private fun hasAcquireLocationPermission(): Boolean {
        return isAccessFineLocationGranted() ||
            permissionsModule.toggleDangerousPermission(appDesc, android.Manifest.permission.ACCESS_FINE_LOCATION, true)
    }

    private fun isAccessFineLocationGranted(): Boolean {
        return appContext.checkSelfPermission(android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
    }

    private val _currentLocation = MutableStateFlow<Location?>(null)
    val currentLocation: StateFlow<Location?> = _currentLocation

    private var localListener = object : LocationListener {

        override fun onLocationChanged(location: Location) {
            _currentLocation.update { location }
        }

        override fun onProviderEnabled(provider: String) {
            reset()
        }

        override fun onProviderDisabled(provider: String) {
            reset()
        }

        private fun reset() {
            stopListeningLocation()
            _currentLocation.value = null
            startListeningLocation()
        }
    }

    fun startListeningLocation(): Boolean {
        return if (hasAcquireLocationPermission()) {
            requestLocationUpdates()
            true
        } else {
            false
        }
    }

    fun stopListeningLocation() {
        locationManager.removeUpdates(localListener)
    }

    private fun requestLocationUpdates() {
        val networkProvider = LocationManager.NETWORK_PROVIDER
            .takeIf { it in locationManager.allProviders }
        val gpsProvider = LocationManager.GPS_PROVIDER
            .takeIf { it in locationManager.allProviders }

        try {
            networkProvider?.let {
                locationManager.requestLocationUpdates(
                    it,
                    MIN_TIME_INTERVAL,
                    MIN_DIST_INTERVAL,
                    localListener
                )
            }
            gpsProvider?.let {
                locationManager.requestLocationUpdates(
                    it,
                    MIN_TIME_INTERVAL,
                    MIN_DIST_INTERVAL,
                    localListener
                )
            }

            var lastKnownLocation = networkProvider?.let {
                locationManager.getLastKnownLocation(it)
            }

            if (lastKnownLocation == null) {
                lastKnownLocation = gpsProvider?.let {
                    locationManager.getLastKnownLocation(it)
                }
            }

            lastKnownLocation?.let { localListener.onLocationChanged(it) }
        } catch (se: SecurityException) {
            Timber.e(se, "Missing permission")
        }
    }
}
