/*
 * Copyright (C) 2023-2024 MURENA SAS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package foundation.e.advancedprivacy.domain.usecases

import foundation.e.advancedprivacy.data.repositories.AppListRepository
import foundation.e.advancedprivacy.domain.entities.AppWithCount
import foundation.e.advancedprivacy.domain.entities.DisplayableApp
import foundation.e.advancedprivacy.domain.entities.TrackerWithCount
import foundation.e.advancedprivacy.domain.entities.TrackersAndAppsLists
import foundation.e.advancedprivacy.features.trackers.Period
import foundation.e.advancedprivacy.trackers.data.StatsDatabase
import foundation.e.advancedprivacy.trackers.data.TrackersRepository
import foundation.e.advancedprivacy.trackers.domain.entities.Tracker
import java.time.Instant
import kotlinx.coroutines.flow.first

class TrackersAndAppsListsUseCase(
    private val statsDatabase: StatsDatabase,
    private val trackersRepository: TrackersRepository,
    private val appListRepository: AppListRepository
) {
    suspend fun getAppsAndTrackersCounts(period: Period): TrackersAndAppsLists {
        val countByEntitiesMaps = getCountByEntityMaps(period)

        return TrackersAndAppsLists(
            trackers = buildTrackerList(countByEntitiesMaps.countByTrackers),
            allApps = buildAllAppList(countByEntitiesMaps.countByApps),
            appsWithTrackers = buildAppList(countByEntitiesMaps.countByApps)
        )
    }

    suspend fun buildWallOfShame(): TrackersAndAppsLists {
        val trackers = statsDatabase
            .get5MostCalledTrackers(since = Period.MONTH.getPeriodStart().epochSecond)
            .mapNotNull { (trackerId, calls) ->
                trackersRepository.getTracker(trackerId)?.let {
                    TrackerWithCount(it, calls)
                }
            }

        return TrackersAndAppsLists(
            trackers = trackers,
            appsWithTrackers = get5MostTrackedAppsLastMonth(),
            allApps = emptyList()
        )
    }

    private suspend fun get5MostTrackedAppsLastMonth(): List<AppWithCount> {
        val countByApIds = statsDatabase.getCallsByAppIds(since = Period.MONTH.getPeriodStart().epochSecond)

        val countByApps = mutableMapOf<DisplayableApp, Int>()
        countByApIds.forEach { (apId, count) ->
            appListRepository.getInternetAppByApId(apId)?.let { app ->
                countByApps[app] = count + (countByApps[app] ?: 0)
            }
        }
        return countByApps.toList().sortedByDescending { it.second }.take(5).map { (app, count) ->
            AppWithCount(app, count)
        }
    }

    private suspend fun getCountByEntityMaps(period: Period): CountByEntitiesMaps {
        val periodStart: Instant = period.getPeriodStart()
        val trackersAndAppsIds = statsDatabase.getDistinctTrackerAndApp(periodStart)
        val trackersAndApps = mapIdsToEntities(trackersAndAppsIds)
        return foldToCountByEntityMaps(trackersAndApps)
    }

    private fun buildTrackerList(countByTracker: Map<Tracker, Int>): List<TrackerWithCount> {
        return countByTracker.map { (tracker, count) ->
            TrackerWithCount(tracker = tracker, count = count)
        }.sortedByDescending { it.count }
    }

    private suspend fun buildAllAppList(countByApp: Map<DisplayableApp, Int>): List<AppWithCount> {
        return appListRepository.displayableApps.first()
            .filter { it.hasInternetPermission }
            .map { app: DisplayableApp ->
                AppWithCount(app = app, count = countByApp[app] ?: 0)
            }.sortedByDescending { it.count }
    }

    private fun buildAppList(countByApp: Map<DisplayableApp, Int>): List<AppWithCount> {
        return countByApp.map { (app, count) ->
            AppWithCount(app = app, count = count)
        }.sortedByDescending { it.count }
    }

    private suspend fun mapIdsToEntities(trackersAndAppsIds: List<Pair<String, String>>): List<Pair<Tracker, DisplayableApp>> {
        return trackersAndAppsIds.mapNotNull { (trackerId, apId) ->
            trackersRepository.getTracker(trackerId)?.let { tracker ->
                appListRepository.getInternetAppByApId(apId)?.let { app ->
                    tracker to app
                }
            }
            // appListsRepository.getDisplayableApp() may transform many apId to one
            // ApplicationDescription, so the lists is not distinct anymore.
        }.distinct()
    }

    private fun foldToCountByEntityMaps(trackersAndApps: List<Pair<Tracker, DisplayableApp>>): CountByEntitiesMaps {
        return trackersAndApps.fold(
            mutableMapOf<DisplayableApp, Int>() to mutableMapOf<Tracker, Int>()
        ) { (countByApp, countByTracker), (tracker, app) ->
            countByApp[app] = countByApp.getOrDefault(app, 0) + 1
            countByTracker[tracker] = countByTracker.getOrDefault(tracker, 0) + 1
            countByApp to countByTracker
        }.let { (countByApp, countByTracker) ->
            CountByEntitiesMaps(
                countByApps = countByApp,
                countByTrackers = countByTracker
            )
        }
    }

    private data class CountByEntitiesMaps(
        val countByApps: Map<DisplayableApp, Int>,
        val countByTrackers: Map<Tracker, Int>
    )
}
