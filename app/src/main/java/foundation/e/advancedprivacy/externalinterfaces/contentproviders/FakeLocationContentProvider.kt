/*
 * Copyright (C) 2024 E FOUNDATION
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package foundation.e.advancedprivacy.externalinterfaces.contentproviders

import android.content.ContentProvider
import android.content.ContentValues
import android.database.Cursor
import android.net.Uri
import android.os.Bundle
import foundation.e.advancedprivacy.domain.usecases.FakeLocationForAppUseCase
import org.koin.android.ext.android.inject

class FakeLocationContentProvider : ContentProvider() {
    private val PARAM_UID = "uid"
    private val PARAM_LATITUDE = "latitude"
    private val PARAM_LONGITUDE = "longitude"

    private val fakeLocationForAppUseCase: FakeLocationForAppUseCase by inject()

    override fun call(method: String, arg: String?, extras: Bundle?): Bundle? {
        val appUid = extras?.getInt(PARAM_UID, -1) ?: -1

        return fakeLocationForAppUseCase.getFakeLocationOrNull(arg, appUid)?.let { (lat, lon) ->
            Bundle().apply {
                putDouble(PARAM_LATITUDE, lat.toDouble())
                putDouble(PARAM_LONGITUDE, lon.toDouble())
            }
        }
    }

    override fun onCreate(): Boolean {
        return true
    }

    override fun query(
        uri: Uri,
        projection: Array<out String>?,
        selection: String?,
        selectionArgs: Array<out String>?,
        sortOrder: String?
    ): Cursor? {
        // Use call instead
        return null
    }

    override fun getType(uri: Uri): String? {
        return "text/plain"
    }

    override fun insert(p0: Uri, p1: ContentValues?): Uri? {
        // ReadOnly content provider
        return null
    }

    override fun delete(p0: Uri, p1: String?, p2: Array<out String>?): Int {
        // ReadOnly content provider
        return 0
    }

    override fun update(p0: Uri, p1: ContentValues?, p2: String?, p3: Array<out String>?): Int {
        // ReadOnly content provider
        return 0
    }
}
