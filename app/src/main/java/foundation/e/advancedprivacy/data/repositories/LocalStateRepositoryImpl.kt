/*
 * Copyright (C) 2023-2024 MURENA SAS
 * Copyright (C) 2021 - 2024 E FOUNDATION
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.advancedprivacy.data.repositories

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.SharedPreferencesMigration
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.floatPreferencesKey
import androidx.datastore.preferences.core.intPreferencesKey
import androidx.datastore.preferences.core.stringSetPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import foundation.e.advancedprivacy.core.utils.getValue
import foundation.e.advancedprivacy.core.utils.mapKey
import foundation.e.advancedprivacy.core.utils.setValue
import foundation.e.advancedprivacy.core.utils.toggleValue
import foundation.e.advancedprivacy.domain.entities.ApplicationDescription
import foundation.e.advancedprivacy.domain.entities.DisplayableApp
import foundation.e.advancedprivacy.domain.entities.FeatureState
import foundation.e.advancedprivacy.domain.entities.MainFeatures
import foundation.e.advancedprivacy.domain.repositories.LocalStateRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map

class LocalStateRepositoryImpl(context: Context) : LocalStateRepository {
    companion object {
        private const val SHARED_PREFS_FILE = "localState"
        private const val PREF_DATASTORE = "localstate_datastore"
    }

    private val blockTrackersKey = booleanPreferencesKey("blockTrackers")
    private val ipScramblingKey = booleanPreferencesKey("ipScrambling")
    private val fakeLocationKey = booleanPreferencesKey("fakeLocation")
    private val fakeLatitudeKey = floatPreferencesKey("fakeLatitude")
    private val fakeLongitudeKey = floatPreferencesKey("fakeLongitude")
    private val fakeLocationWhitelistKey = stringSetPreferencesKey("fakeLocationWhitelist")
    private val firstBootKey = booleanPreferencesKey("firstBoot")
    private val hideWarningTrackersKey = booleanPreferencesKey("hide_warning_trackers")
    private val hideWarningLocationKey = booleanPreferencesKey("hide_warning_location")
    private val hideWarningIpScramblingKey = booleanPreferencesKey("hide_warning_ipscrambling")
    private val trackersScreenLastPositionKey = intPreferencesKey("trackers_screen_last_position")

    private val Context.dataStore: DataStore<Preferences> by preferencesDataStore(
        name = PREF_DATASTORE,
        produceMigrations = ::sharedPreferencesMigration
    )

    private val store = context.dataStore

    private fun sharedPreferencesMigration(context: Context) = listOf(SharedPreferencesMigration(context, SHARED_PREFS_FILE))

    override val blockTrackers: Flow<Boolean> = store.mapKey(blockTrackersKey, true)

    override suspend fun toggleBlockTrackers(enabled: Boolean?) {
        store.toggleValue(blockTrackersKey, enabled, true)
    }

    override val areAllTrackersBlocked: MutableStateFlow<Boolean> = MutableStateFlow(false)

    override val fakeLocationEnabled = store.mapKey(fakeLocationKey, false)

    override suspend fun toggleFakeLocation(enabled: Boolean?) {
        store.toggleValue(fakeLocationKey, enabled, false)
    }

    override val fakeLocation: Flow<Pair<Float, Float>> = store.data.map { preferences ->
        // Initial default value is Quezon City
        val lat = preferences[fakeLatitudeKey] ?: 14.6760f
        val lon = preferences[fakeLongitudeKey] ?: 121.0437f
        lat to lon
    }

    override suspend fun getFakeLocation(): Pair<Float, Float> = fakeLocation.first()
    override suspend fun setFakeLocation(latLon: Pair<Float, Float>) {
        store.edit { preferences ->
            preferences[fakeLatitudeKey] = latLon.first
            preferences[fakeLongitudeKey] = latLon.second
        }
    }

    override val fakeLocationWhitelistedApps = store.mapKey(fakeLocationWhitelistKey, emptySet())

    override suspend fun toggleAppFakeLocationWhitelisted(app: DisplayableApp) {
        store.edit { preferences ->
            val whitelist = preferences[fakeLocationWhitelistKey] ?: emptySet()

            val apIds = app.apps.map { it.apId }.toSet()

            val appInWhitelist = apIds.any { whitelist.contains(it) }
            preferences[fakeLocationWhitelistKey] = if (appInWhitelist) {
                whitelist - apIds
            } else {
                whitelist + apIds
            }
        }
    }

    override suspend fun resetFakeLocationWhitelistedApp() {
        store.setValue(fakeLocationWhitelistKey, emptySet())
    }

    override val ipScramblingEnabled = store.mapKey(ipScramblingKey, false)
    override suspend fun toggleIpScrambling(enabled: Boolean?) {
        store.toggleValue(ipScramblingKey, enabled, false)
    }

    override val internetPrivacyMode: MutableStateFlow<FeatureState> = MutableStateFlow(FeatureState.OFF)

    private val _startVpnDisclaimer = MutableSharedFlow<MainFeatures>()

    override suspend fun emitStartVpnDisclaimer(feature: MainFeatures) {
        _startVpnDisclaimer.emit(feature)
    }

    override val startVpnDisclaimer: SharedFlow<MainFeatures> = _startVpnDisclaimer

    private val _otherVpnRunning = MutableSharedFlow<ApplicationDescription>()

    override suspend fun emitOtherVpnRunning(appDesc: ApplicationDescription) {
        _otherVpnRunning.emit(appDesc)
    }

    override val otherVpnRunning: SharedFlow<ApplicationDescription> = _otherVpnRunning

    override suspend fun isFirstBoot(): Boolean {
        return store.getValue(firstBootKey) ?: true
    }

    override suspend fun setFirstBoot(isStillFirstBoot: Boolean) {
        store.setValue(firstBootKey, isStillFirstBoot)
    }

    override suspend fun isHideWarningTrackers(): Boolean {
        return store.getValue(hideWarningTrackersKey) ?: false
    }

    override suspend fun hideWarningTrackers(hide: Boolean) {
        return store.setValue(hideWarningTrackersKey, hide)
    }

    override suspend fun isHideWarningLocation(): Boolean {
        return store.getValue(hideWarningLocationKey) ?: false
    }

    override suspend fun hideWarningLocation(hide: Boolean) {
        return store.setValue(hideWarningLocationKey, hide)
    }

    override suspend fun isHideWarningIpScrambling(): Boolean {
        return store.getValue(hideWarningIpScramblingKey) ?: false
    }

    override suspend fun hideWarningIpScrambling(hide: Boolean) {
        return store.setValue(hideWarningIpScramblingKey, hide)
    }

    override suspend fun getTrackersScreenLastPosition(): Int {
        return store.getValue(trackersScreenLastPositionKey) ?: 0
    }

    override suspend fun setTrackersScreenLastPosition(position: Int) {
        store.setValue(trackersScreenLastPositionKey, position)
    }

    override var trackersScreenTabStartPosition: Int = 0
}
