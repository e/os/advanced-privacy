/*
 * Copyright (C) 2023 MURENA SAS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package foundation.e.advancedprivacy.features.trackers

import android.content.Context
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.text.style.UnderlineSpan
import android.view.View
import android.widget.TextView
import androidx.core.content.ContextCompat
import foundation.e.advancedprivacy.R

const val URL_LEARN_MORE_ABOUT_TRACKERS = "https://doc.e.foundation/support-topics/advanced_privacy#trackers-blocker"

fun setupDisclaimerBlock(view: TextView, onClickLearnMore: () -> Unit) {
    with(view) {
        linksClickable = true
        isClickable = true
        movementMethod = android.text.method.LinkMovementMethod.getInstance()
        text = buildSpan(view.context, onClickLearnMore)
    }
}

private fun buildSpan(context: Context, onClickLearnMore: () -> Unit): SpannableString {
    val start = context.getString(R.string.trackercontroldisclaimer_start)
    val body = context.getString(R.string.trackercontroldisclaimer_body)
    val link = context.getString(R.string.trackercontroldisclaimer_link)

    val spannable = SpannableString("$start $body $link")

    val startEndIndex = start.length + 1
    val linkStartIndex = startEndIndex + body.length + 1
    val linkEndIndex = spannable.length
    spannable.setSpan(
        ForegroundColorSpan(ContextCompat.getColor(context, R.color.primary_text)),
        0,
        startEndIndex,
        Spannable.SPAN_INCLUSIVE_EXCLUSIVE
    )

    spannable.setSpan(
        ForegroundColorSpan(ContextCompat.getColor(context, R.color.disabled)),
        startEndIndex,
        linkStartIndex,
        Spannable.SPAN_INCLUSIVE_EXCLUSIVE
    )

    spannable.setSpan(
        ForegroundColorSpan(ContextCompat.getColor(context, R.color.accent)),
        linkStartIndex,
        linkEndIndex,
        Spannable.SPAN_INCLUSIVE_EXCLUSIVE
    )
    spannable.setSpan(UnderlineSpan(), linkStartIndex, linkEndIndex, Spannable.SPAN_INCLUSIVE_EXCLUSIVE)
    spannable.setSpan(
        object : ClickableSpan() {
            override fun onClick(p0: View) {
                onClickLearnMore.invoke()
            }
        },
        linkStartIndex,
        linkEndIndex,
        Spannable.SPAN_INCLUSIVE_EXCLUSIVE
    )
    return spannable
}
