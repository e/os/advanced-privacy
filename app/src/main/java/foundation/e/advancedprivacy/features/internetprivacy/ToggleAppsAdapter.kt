/*
 * Copyright (C) 2021 - 2024 E FOUNDATION
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.advancedprivacy.features.internetprivacy

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import foundation.e.advancedprivacy.R
import foundation.e.advancedprivacy.domain.entities.DisplayableApp
import foundation.e.advancedprivacy.domain.entities.ToggleableApp

class ToggleAppsAdapter(
    private val itemsLayout: Int,
    private val listener: (DisplayableApp) -> Unit
) :
    RecyclerView.Adapter<ToggleAppsAdapter.ViewHolder>() {

    class ViewHolder(view: View, private val listener: (DisplayableApp) -> Unit) : RecyclerView.ViewHolder(view) {
        val appName: TextView = view.findViewById(R.id.title)

        val togglePermission: CheckBox = view.findViewById(R.id.toggle)

        fun bind(item: ToggleableApp, isEnabled: Boolean) {
            appName.text = item.app.label
            togglePermission.isChecked = item.isOn
            togglePermission.isEnabled = isEnabled

            itemView.findViewById<ImageView>(R.id.icon).setImageDrawable(item.app.icon)
            togglePermission.setOnClickListener { listener(item.app) }
        }
    }

    var dataSet: List<ToggleableApp> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    var isEnabled: Boolean = true

    fun setData(list: List<ToggleableApp>, isEnabled: Boolean = true) {
        this.isEnabled = isEnabled
        dataSet = list
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(itemsLayout, parent, false)
        return ViewHolder(view, listener)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val permission = dataSet[position]
        holder.bind(permission, isEnabled)
    }

    override fun getItemCount(): Int = dataSet.size
}
