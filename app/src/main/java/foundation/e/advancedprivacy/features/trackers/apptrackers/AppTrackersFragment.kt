/*
 * Copyright (C) 2023-2024 MURENA SAS
 * Copyright (C) 2021 E FOUNDATION
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.advancedprivacy.features.trackers.apptrackers

import android.content.ActivityNotFoundException
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.divider.MaterialDividerItemDecoration
import com.google.android.material.snackbar.Snackbar
import foundation.e.advancedprivacy.R
import foundation.e.advancedprivacy.common.BigNumberFormatter
import foundation.e.advancedprivacy.common.NavToolbarFragment
import foundation.e.advancedprivacy.databinding.ApptrackersFragmentBinding
import foundation.e.advancedprivacy.features.trackers.setupDisclaimerBlock
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class AppTrackersFragment : NavToolbarFragment(R.layout.apptrackers_fragment) {

    private val args: AppTrackersFragmentArgs by navArgs()
    private val viewModel: AppTrackersViewModel by viewModel { parametersOf(args.appId) }

    private val numberFormatter: BigNumberFormatter by lazy { BigNumberFormatter(requireContext()) }

    private lateinit var binding: ApptrackersFragmentBinding

    override fun getTitle(): CharSequence {
        return ""
    }

    private fun displayToast(message: String) {
        Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT)
            .show()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = ApptrackersFragmentBinding.bind(view)

        binding.blockAllToggle.setOnClickListener {
            viewModel.onToggleBlockAll(binding.blockAllToggle.isChecked)
        }
        binding.btnReset.setOnClickListener { viewModel.onClickResetAllTrackers() }

        binding.list.apply {
            layoutManager = LinearLayoutManager(requireContext())
            setHasFixedSize(true)
            addItemDecoration(
                MaterialDividerItemDecoration(requireContext(), LinearLayoutManager.VERTICAL).apply {
                    dividerColor = ContextCompat.getColor(requireContext(), R.color.divider)
                }
            )
            adapter = ToggleTrackersAdapter(viewModel)
        }

        listenViewModel()

        setupDisclaimerBlock(binding.disclaimerBlockTrackers.root, viewModel::onClickLearnMore)
    }

    private fun listenViewModel() {
        with(viewLifecycleOwner) {
            lifecycleScope.launch {
                repeatOnLifecycle(Lifecycle.State.STARTED) {
                    viewModel.singleEvents.collect(::handleEvents)
                }
            }

            lifecycleScope.launch {
                repeatOnLifecycle(Lifecycle.State.STARTED) {
                    viewModel.doOnStartedState()
                }
            }

            lifecycleScope.launch {
                repeatOnLifecycle(Lifecycle.State.STARTED) {
                    render(viewModel.state.value)
                    viewModel.state.collect(::render)
                }
            }
        }
    }
    private fun handleEvents(event: AppTrackersViewModel.SingleEvent) {
        when (event) {
            is AppTrackersViewModel.SingleEvent.ErrorEvent ->
                displayToast(getString(event.errorResId))

            is AppTrackersViewModel.SingleEvent.OpenUrl ->
                try {
                    startActivity(Intent(Intent.ACTION_VIEW, event.url))
                } catch (e: ActivityNotFoundException) {
                    Toast.makeText(
                        requireContext(),
                        R.string.error_no_activity_view_url,
                        Toast.LENGTH_SHORT
                    ).show()
                }

            is AppTrackersViewModel.SingleEvent.ToastTrackersControlDisabled ->
                Snackbar.make(
                    binding.root,
                    R.string.apptrackers_tracker_control_disabled_message,
                    Snackbar.LENGTH_LONG
                ).show()
        }
    }

    private fun render(state: AppTrackersState) {
        setTitle(state.app?.label)
        binding.subtitle.text = getString(R.string.apptrackers_subtitle, state.app?.label)
        binding.dataDetectedTrackers.apply {
            primaryMessage.setText(R.string.apptrackers_detected_tracker_primary)
            number.text = state.getTrackersCount().toString()
            secondaryMessage.setText(R.string.apptrackers_detected_tracker_secondary)
        }

        binding.dataBlockedTrackers.apply {
            primaryMessage.setText(R.string.apptrackers_blocked_tracker_primary)
            number.text = state.getBlockedTrackersCount().toString()
            secondaryMessage.setText(R.string.apptrackers_blocked_tracker_secondary)
        }

        binding.dataBlockedLeaks.apply {
            primaryMessage.setText(R.string.apptrackers_blocked_leaks_primary)
            number.text = numberFormatter.format(state.blocked)
            secondaryMessage.text = getString(R.string.apptrackers_blocked_leaks_secondary, numberFormatter.format(state.leaked))
        }

        binding.blockAllToggle.isChecked = state.isBlockingActivated

        val trackersStatus = state.trackersWithBlockedList
        if (!trackersStatus.isEmpty()) {
            binding.listTitle.isVisible = true
            binding.list.isVisible = true
            binding.list.post {
                (binding.list.adapter as ToggleTrackersAdapter?)?.updateDataSet(trackersStatus)
            }
            binding.noTrackersYet.isVisible = false
            binding.btnReset.isVisible = true
        } else {
            binding.listTitle.isVisible = false
            binding.list.isVisible = false
            binding.noTrackersYet.isVisible = true
            binding.noTrackersYet.text = getString(
                when {
                    !state.isBlockingActivated -> R.string.apptrackers_no_trackers_yet_block_off
                    state.isWhitelistEmpty -> R.string.apptrackers_no_trackers_yet_block_on
                    else -> R.string.app_trackers_no_trackers_yet_remaining_whitelist
                }
            )
            binding.btnReset.isVisible = state.isBlockingActivated && !state.isWhitelistEmpty
        }
    }
}
