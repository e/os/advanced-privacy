/*
 * Copyright (C) 2022 - 2024 MURENA SAS
 * Copyright (C) 2021 E FOUNDATION
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.advancedprivacy.features.trackers

import android.net.Uri
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavDirections
import foundation.e.advancedprivacy.R
import foundation.e.advancedprivacy.domain.usecases.TrackersScreenUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.launch

class TrackersViewModel(private val trackersScreenUseCase: TrackersScreenUseCase) : ViewModel() {
    private val _singleEvents = MutableSharedFlow<SingleEvent>()
    val singleEvents = _singleEvents.asSharedFlow()

    private val _navigate = MutableSharedFlow<NavDirections>()
    val navigate = _navigate.asSharedFlow()

    private val _refreshUiHeight = MutableSharedFlow<Unit>()
    val refreshUiHeight = _refreshUiHeight.asSharedFlow()

    val positionsCount = 3
    fun getPeriod(position: Int): Period {
        return when (position) {
            0 -> Period.DAY
            1 -> Period.MONTH
            2 -> Period.YEAR
            else -> Period.DAY
        }
    }

    fun getDisplayDuration(position: Int): Int {
        return when (position) {
            0 -> R.string.trackers_period_day
            1 -> R.string.trackers_period_month
            else -> R.string.trackers_period_year
        }
    }

    suspend fun getLastPosition(): Int {
        val lastPosition = trackersScreenUseCase.getLastPosition()
        return if (lastPosition in 0 until positionsCount) {
            lastPosition
        } else {
            0
        }
    }

    fun onDisplayedItemChanged(position: Int) = viewModelScope.launch(Dispatchers.IO) {
        trackersScreenUseCase.savePosition(position)
        trackersScreenUseCase.resetTrackerTabStartPosition()
        _refreshUiHeight.emit(Unit)
    }

    fun onClickLearnMore() = viewModelScope.launch {
        _singleEvents.emit(SingleEvent.OpenUrl(Uri.parse(URL_LEARN_MORE_ABOUT_TRACKERS)))
    }

    sealed class SingleEvent {
        data class ErrorEvent(val error: String) : SingleEvent()
        data class OpenUrl(val url: Uri) : SingleEvent()
    }
}
