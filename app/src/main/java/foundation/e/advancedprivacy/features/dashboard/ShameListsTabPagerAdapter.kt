/*
 * Copyright (C) 2023 - 2024 MURENA SAS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package foundation.e.advancedprivacy.features.dashboard

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.divider.MaterialDividerItemDecoration
import foundation.e.advancedprivacy.R
import foundation.e.advancedprivacy.common.BigNumberFormatter
import foundation.e.advancedprivacy.common.BindingListAdapter
import foundation.e.advancedprivacy.common.BindingViewHolder
import foundation.e.advancedprivacy.common.extensions.dpToPx
import foundation.e.advancedprivacy.databinding.DashboardShameListBinding
import foundation.e.advancedprivacy.databinding.TrackersItemAppBinding
import foundation.e.advancedprivacy.domain.entities.AppWithCount
import foundation.e.advancedprivacy.domain.entities.DisplayableApp
import foundation.e.advancedprivacy.domain.entities.TrackerWithCount
import foundation.e.advancedprivacy.features.trackers.TrackerTab
import foundation.e.advancedprivacy.trackers.domain.entities.Tracker

class ShameListsTabPagerAdapter(
    private val onClickShameApp: (DisplayableApp) -> Unit,
    private val onClickShameTracker: (Tracker) -> Unit,
    private val onClickViewAllApps: () -> Unit,
    private val onClickViewAllTrackers: () -> Unit
) : RecyclerView.Adapter<ShameListsTabPagerAdapter.ListsTabViewHolder>() {
    private var uiState: DashboardState = DashboardState()

    fun updateDataSet(state: DashboardState) {
        uiState = state
        notifyDataSetChanged()
    }

    override fun getItemViewType(position: Int): Int = position

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListsTabViewHolder {
        val view = DashboardShameListBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return when (viewType) {
            TrackerTab.APPS.position -> {
                ListsTabViewHolder.AppsListViewHolder(view, onClickShameApp, onClickViewAllApps)
            }
            else -> {
                ListsTabViewHolder.TrackersListViewHolder(view, onClickShameTracker, onClickViewAllTrackers)
            }
        }
    }

    override fun getItemCount(): Int {
        return 2
    }

    override fun onBindViewHolder(holder: ListsTabViewHolder, position: Int) {
        when (position) {
            TrackerTab.APPS.position -> {
                (holder as ListsTabViewHolder.AppsListViewHolder).onBind(uiState)
            }
            TrackerTab.TRACKERS.position -> {
                (holder as ListsTabViewHolder.TrackersListViewHolder).onBind(uiState)
            }
        }
    }

    sealed class ListsTabViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        protected val numberFormatter: BigNumberFormatter by lazy { BigNumberFormatter(itemView.context) }

        protected fun setupRecyclerView(recyclerView: RecyclerView) {
            recyclerView.apply {
                layoutManager = LinearLayoutManager(context)
                setHasFixedSize(true)
                isNestedScrollingEnabled = false
                addItemDecoration(
                    MaterialDividerItemDecoration(context, LinearLayoutManager.VERTICAL).apply {
                        dividerColor = ContextCompat.getColor(context, R.color.divider)
                        dividerInsetStart = 16.dpToPx(context)
                        dividerInsetEnd = 16.dpToPx(context)
                    }
                )
            }
        }

        class AppsListViewHolder(
            private val binding: DashboardShameListBinding,
            private val onClickShameApp: (DisplayableApp) -> Unit,
            private val onClickViewAllApps: () -> Unit
        ) : ListsTabViewHolder(binding.root) {

            private val adapter = object : BindingListAdapter<TrackersItemAppBinding, AppWithCount>() {
                override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BindingViewHolder<TrackersItemAppBinding> {
                    return BindingViewHolder(
                        TrackersItemAppBinding.inflate(
                            LayoutInflater.from(parent.context),
                            parent,
                            false
                        )
                    )
                }

                override fun onBindViewHolder(holder: BindingViewHolder<TrackersItemAppBinding>, position: Int) {
                    val item = dataSet[position]
                    holder.binding.icon.setImageDrawable(item.app.icon)
                    holder.binding.title.text = item.app.label
                    holder.binding.counts.text = itemView.context.getString(
                        R.string.dashboard_wall_of_shame_app_calls,
                        numberFormatter.format(item.count)
                    )
                    holder.binding.root.setOnClickListener { onClickShameApp(item.app) }
                }
            }
            init {
                setupRecyclerView(binding.list)
                binding.list.adapter = adapter
                binding.viewAll.apply {
                    text = binding.root.context.getString(R.string.dashboard_view_all_apps)
                    setOnClickListener { onClickViewAllApps() }
                }
            }

            fun onBind(uiState: DashboardState) {
                adapter.dataSet = uiState.shameApps
            }
        }

        class TrackersListViewHolder(
            private val binding: DashboardShameListBinding,
            private val onClickShameTracker: (Tracker) -> Unit,
            private val onClickViewAllTrackers: () -> Unit
        ) : ListsTabViewHolder(binding.root) {

            private val adapter = object : BindingListAdapter<TrackersItemAppBinding, TrackerWithCount>() {
                override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BindingViewHolder<TrackersItemAppBinding> {
                    return BindingViewHolder(
                        TrackersItemAppBinding.inflate(
                            LayoutInflater.from(parent.context),
                            parent,
                            false
                        )
                    )
                }

                override fun onBindViewHolder(holder: BindingViewHolder<TrackersItemAppBinding>, position: Int) {
                    val item = dataSet[position]
                    holder.binding.icon.isVisible = false
                    holder.binding.title.text = item.tracker.label
                    holder.binding.counts.text = itemView.context.getString(
                        R.string.dashboard_wall_of_shame_trackers_calls,
                        numberFormatter.format(item.count)
                    )
                    holder.binding.root.setOnClickListener { onClickShameTracker(item.tracker) }
                }
            }

            init {
                setupRecyclerView(binding.list)
                binding.list.adapter = adapter
                binding.viewAll.apply {
                    text = binding.root.context.getString(R.string.dashboard_view_all_trackers)
                    setOnClickListener { onClickViewAllTrackers() }
                }
            }

            fun onBind(uiState: DashboardState) {
                adapter.dataSet = uiState.shameTrackers
            }
        }
    }
}
