/*
 * Copyright (C) 2024 MURENA SAS
 * Copyright (C) 2022 E FOUNDATION
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.advancedprivacy.features.dashboard

import foundation.e.advancedprivacy.domain.entities.AppWithCount
import foundation.e.advancedprivacy.domain.entities.FeatureMode
import foundation.e.advancedprivacy.domain.entities.FeatureState
import foundation.e.advancedprivacy.domain.entities.TrackerWithCount

data class DashboardState(
    val trackerMode: FeatureMode = FeatureMode.VULNERABLE,
    val locationMode: FeatureMode = FeatureMode.VULNERABLE,
    val ipScramblingMode: FeatureState = FeatureState.STOPPING,
    val blockedCallsCount: Int = 0,
    val appsWithCallsCount: Int = 0,
    val shameApps: List<AppWithCount> = emptyList(),
    val shameTrackers: List<TrackerWithCount> = emptyList()
)
