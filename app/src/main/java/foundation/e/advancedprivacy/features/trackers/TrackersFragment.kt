/*
 * Copyright (C) 2022 - 2024 MURENA SAS
 * Copyright (C) 2021 E FOUNDATION
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.advancedprivacy.features.trackers

import android.content.ActivityNotFoundException
import android.content.Intent
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.text.style.UnderlineSpan
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayoutMediator
import foundation.e.advancedprivacy.R
import foundation.e.advancedprivacy.common.NavToolbarFragment
import foundation.e.advancedprivacy.common.extensions.findViewHolderForAdapterPosition
import foundation.e.advancedprivacy.common.extensions.safeNavigate
import foundation.e.advancedprivacy.common.extensions.updatePagerHeightForChild
import foundation.e.advancedprivacy.databinding.FragmentTrackersBinding
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

class TrackersFragment : NavToolbarFragment(R.layout.fragment_trackers) {
    private val viewModel: TrackersViewModel by viewModel()
    private lateinit var binding: FragmentTrackersBinding

    private lateinit var pagerAdapter: TrackersPeriodAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding = FragmentTrackersBinding.bind(view)

        val trackersTabs = binding.trackersPeriodsTabs
        val trackersPager = binding.trackersPeriodsPager

        pagerAdapter = TrackersPeriodAdapter(this, viewModel)
        trackersPager.adapter = pagerAdapter
        TabLayoutMediator(trackersTabs, trackersPager) { tab, position ->
            tab.text = getString(viewModel.getDisplayDuration(position))
        }.attach()

        setupTrackersInfos()

        binding.trackersPeriodsPager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageScrollStateChanged(state: Int) {
                super.onPageScrollStateChanged(state)
                if (state == ViewPager2.SCROLL_STATE_IDLE) {
                    viewModel.onDisplayedItemChanged(binding.trackersPeriodsPager.currentItem)
                }
            }
        })

        listenViewModel()
    }

    override fun onStart() {
        super.onStart()
        lifecycleScope.launch {
            binding.trackersPeriodsPager.currentItem = viewModel.getLastPosition()
        }
    }

    fun refreshUiHeight(): SharedFlow<Unit> {
        return viewModel.refreshUiHeight
    }

    private fun listenViewModel() {
        with(viewLifecycleOwner) {
            lifecycleScope.launch {
                repeatOnLifecycle(Lifecycle.State.STARTED) {
                    viewModel.singleEvents.collect(::handleEvents)
                }
            }

            lifecycleScope.launch {
                repeatOnLifecycle(Lifecycle.State.STARTED) {
                    viewModel.navigate.collect(findNavController()::safeNavigate)
                }
            }
        }
    }

    private fun handleEvents(event: TrackersViewModel.SingleEvent) {
        when (event) {
            is TrackersViewModel.SingleEvent.ErrorEvent -> {
                displayToast(event.error)
            }
            is TrackersViewModel.SingleEvent.OpenUrl -> {
                try {
                    startActivity(Intent(Intent.ACTION_VIEW, event.url))
                } catch (e: ActivityNotFoundException) {
                    Toast.makeText(
                        requireContext(),
                        R.string.error_no_activity_view_url,
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
    }

    private fun setupTrackersInfos() {
        val infoText = getString(R.string.trackers_info)
        val moreText = getString(R.string.trackers_info_more)

        val spannable = SpannableString("$infoText $moreText")
        val startIndex = infoText.length + 1
        val endIndex = spannable.length
        spannable.setSpan(
            ForegroundColorSpan(ContextCompat.getColor(requireContext(), R.color.accent)),
            startIndex,
            endIndex,
            Spannable.SPAN_INCLUSIVE_EXCLUSIVE
        )
        spannable.setSpan(UnderlineSpan(), startIndex, endIndex, Spannable.SPAN_INCLUSIVE_EXCLUSIVE)
        spannable.setSpan(
            object : ClickableSpan() {
                override fun onClick(p0: View) {
                    viewModel.onClickLearnMore()
                }
            },
            startIndex,
            endIndex,
            Spannable.SPAN_INCLUSIVE_EXCLUSIVE
        )

        with(binding.trackersInfo) {
            linksClickable = true
            isClickable = true
            movementMethod = LinkMovementMethod.getInstance()
            text = spannable
        }
    }

    fun updatePagerHeight() {
        binding.trackersPeriodsPager.findViewHolderForAdapterPosition(binding.trackersPeriodsPager.currentItem)
            .let { currentViewHolder ->
                currentViewHolder?.itemView?.let { binding.trackersPeriodsPager.updatePagerHeightForChild(it) }
            }
    }

    private fun displayToast(message: String) {
        Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
    }
}
