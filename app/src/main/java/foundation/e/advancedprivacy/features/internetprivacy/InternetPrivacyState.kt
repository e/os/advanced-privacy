/*
 * Copyright (C) 2022 - 2024 E FOUNDATION
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.advancedprivacy.features.internetprivacy

import foundation.e.advancedprivacy.domain.entities.FeatureState
import foundation.e.advancedprivacy.domain.entities.ToggleableApp

data class InternetPrivacyState(
    val mode: FeatureState = FeatureState.OFF,
    val torToggleableApp: List<ToggleableApp> = emptyList(),
    val selectedLocation: String = "",
    val availableLocationIds: List<String> = emptyList(),
    val forceRedraw: Boolean = false
) {
    val selectedLocationPosition get() = availableLocationIds.indexOf(selectedLocation)
}
