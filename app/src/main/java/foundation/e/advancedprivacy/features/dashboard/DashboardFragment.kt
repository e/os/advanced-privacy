/*
 * Copyright (C) 2023-2024 MURENA SAS
 * Copyright (C) 2021 E FOUNDATION
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.advancedprivacy.features.dashboard

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.getColor
import androidx.core.view.isVisible
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import com.google.android.material.tabs.TabLayoutMediator
import foundation.e.advancedprivacy.R
import foundation.e.advancedprivacy.common.BigNumberFormatter
import foundation.e.advancedprivacy.common.NavToolbarFragment
import foundation.e.advancedprivacy.common.extensions.safeNavigate
import foundation.e.advancedprivacy.databinding.FragmentDashboardBinding
import foundation.e.advancedprivacy.domain.entities.FeatureMode
import foundation.e.advancedprivacy.domain.entities.FeatureState
import foundation.e.advancedprivacy.features.dashboard.DashboardViewModel.SingleEvent
import foundation.e.advancedprivacy.features.trackers.TrackerTab
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

class DashboardFragment : NavToolbarFragment(R.layout.fragment_dashboard) {
    private val viewModel: DashboardViewModel by viewModel()
    private val numberFormatter: BigNumberFormatter by lazy { BigNumberFormatter(requireContext()) }

    private lateinit var binding: FragmentDashboardBinding

    private lateinit var tabAdapter: ShameListsTabPagerAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentDashboardBinding.bind(view)

        with(binding.dataBlockedTrackers) {
            primaryMessage.apply {
                setText(R.string.dashboard_data_blocked_trackers_primary)
                setCompoundDrawables(
                    ContextCompat.getDrawable(requireContext(), R.drawable.ic_block_24),
                    null,
                    null,
                    null
                )
            }

            secondaryMessage.setText(R.string.dashboard_data_blocked_trackers_secondary)
        }
        with(binding.dataApps) {
            primaryMessage.apply {
                setText(R.string.dashboard_data_apps_primary)
                setCompoundDrawables(
                    ContextCompat.getDrawable(requireContext(), R.drawable.ic_apps_24),
                    null,
                    null,
                    null
                )
            }

            secondaryMessage.setText(R.string.dashboard_data_apps_secondary)
        }

        binding.trackersControl.title.setText(R.string.dashboard_trackers_title)
        binding.fakeLocation.title.setText(R.string.dashboard_location_title)
        binding.ipScrambling.title.setText(R.string.dashboard_ipscrambling_title)

        tabAdapter = ShameListsTabPagerAdapter(
            onClickShameApp = viewModel::onClickShameApp,
            onClickShameTracker = viewModel::onClickShameTracker,
            onClickViewAllApps = viewModel::onClickViewAllApps,
            onClickViewAllTrackers = viewModel::onClickViewAllTrackers
        )

        binding.listsPager.adapter = tabAdapter

        TabLayoutMediator(binding.listsTabs, binding.listsPager) { tab, position ->
            tab.text = getString(
                when (position) {
                    TrackerTab.APPS.position -> R.string.trackers_toggle_list_apps
                    else -> R.string.trackers_toggle_list_trackers
                }
            )
        }.attach()

        setOnClickListeners()

        listenViewModel()
    }

    private fun setOnClickListeners() {
        binding.viewTrackersStatistics.setOnClickListener {
            viewModel.onClickViewTrackersStatistics()
        }

        with(binding.trackersControl) {
            root.setOnClickListener {
                viewModel.onClickTrackersControl()
            }

            switchFeature.setOnCheckedChangeListener { _, isChecked ->
                viewModel.onClickToggleTrackersContol(isChecked)
            }
        }

        with(binding.fakeLocation) {
            root.setOnClickListener {
                viewModel.onClickFakeLocation()
            }

            switchFeature.setOnCheckedChangeListener { _, isChecked ->
                viewModel.onClickToggleFakeLocation(isChecked)
            }
        }

        with(binding.ipScrambling) {
            root.setOnClickListener {
                viewModel.onClickIpScrambling()
            }

            switchFeature.setOnCheckedChangeListener { _, isChecked ->
                viewModel.onClickToggleIpScrambling(isChecked)
            }
        }

        binding.appsPermissions.setOnClickListener {
            viewModel.onClickAppsPermissions()
        }
    }

    private fun listenViewModel() {
        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.RESUMED) {
                render(viewModel.state.value)
                viewModel.state.collect(::render)
            }
        }

        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.singleEvents.collect { event ->
                    when (event) {
                        is SingleEvent.ToastMessageSingleEvent ->
                            Toast.makeText(
                                requireContext(),
                                getString(event.message, *event.args.toTypedArray()),
                                Toast.LENGTH_LONG
                            ).show()
                    }
                }
            }
        }

        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.navigate.collect(findNavController()::safeNavigate)
            }
        }

        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.doOnStartedState()
            }
        }
    }

    override fun onNavigateUp(): Boolean {
        requireActivity().finish()
        return true
    }

    private fun render(state: DashboardState) {
        binding.dataBlockedTrackers.number.text = numberFormatter.format(state.blockedCallsCount)
        binding.dataApps.number.text = state.appsWithCallsCount.toString()

        with(binding.trackersControl) {
            switchFeature.isChecked = state.trackerMode != FeatureMode.VULNERABLE

            stateLabel.setText(
                when (state.trackerMode) {
                    FeatureMode.DENIED -> R.string.dashboard_state_trackers_on
                    FeatureMode.VULNERABLE -> R.string.dashboard_state_trackers_off
                    FeatureMode.CUSTOM -> R.string.dashboard_state_trackers_custom
                }
            )

            stateLabel.setTextColor(getStateColor(state.trackerMode != FeatureMode.VULNERABLE))
        }

        with(binding.fakeLocation) {
            switchFeature.isChecked = state.locationMode != FeatureMode.VULNERABLE
            stateLabel.setText(
                when (state.locationMode) {
                    FeatureMode.DENIED -> R.string.dashboard_state_geolocation_on
                    FeatureMode.VULNERABLE -> R.string.dashboard_state_geolocation_off
                    FeatureMode.CUSTOM -> R.string.dashboard_state_trackers_custom
                }
            )
            stateLabel.setTextColor(getStateColor(state.locationMode != FeatureMode.VULNERABLE))
        }

        with(binding.ipScrambling) {
            switchFeature.isChecked = state.ipScramblingMode.isChecked

            val isLoading = state.ipScramblingMode.isLoading
            switchFeature.isEnabled = (state.ipScramblingMode != FeatureState.STOPPING)

            stateLoader.isVisible = isLoading
            stateLabel.visibility = if (!isLoading) View.VISIBLE else View.INVISIBLE

            stateLabel.setText(
                if (state.ipScramblingMode == FeatureState.ON) {
                    R.string.dashboard_state_ipaddress_on
                } else {
                    R.string.dashboard_state_ipaddress_off
                }
            )
            stateLabel.setTextColor(getStateColor(state.ipScramblingMode == FeatureState.ON))
        }

        tabAdapter.updateDataSet(state)
    }

    private fun getStateColor(isActive: Boolean): Int {
        return getColor(
            requireContext(),
            if (isActive) {
                R.color.green_valid
            } else {
                R.color.red_off
            }
        )
    }
}
