/*
 * Copyright (C) 2022 - 2024 MURENA SAS
 * Copyright (C) 2021 E FOUNDATION
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.advancedprivacy.features.trackers

import android.annotation.SuppressLint
import android.content.ActivityNotFoundException
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayoutMediator
import foundation.e.advancedprivacy.R
import foundation.e.advancedprivacy.common.extensions.findViewHolderForAdapterPosition
import foundation.e.advancedprivacy.common.extensions.safeNavigate
import foundation.e.advancedprivacy.common.extensions.updatePagerHeightForChild
import foundation.e.advancedprivacy.databinding.TrackersPeriodFragmentBinding
import foundation.e.advancedprivacy.features.trackers.TrackersPeriodViewModel.SingleEvent
import foundation.e.advancedprivacy.features.trackers.graph.GraphHolder
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.flow.merge
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class TrackersPeriodFragment : Fragment(R.layout.trackers_period_fragment) {

    companion object {
        private const val ARG_PERIOD = "period"

        fun buildArguments(period: Period): Bundle {
            return Bundle().apply {
                putString(ARG_PERIOD, period.name)
            }
        }
    }

    private val viewModel: TrackersPeriodViewModel by viewModel {
        parametersOf(
            requireArguments().getString(
                ARG_PERIOD
            )
        )
    }

    private val trackersFragment: TrackersFragment?
        get() = parentFragment as? TrackersFragment?

    private lateinit var binding: TrackersPeriodFragmentBinding

    private lateinit var tabAdapter: ListsTabPagerAdapter
    private lateinit var graphHolder: GraphHolder

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding = TrackersPeriodFragmentBinding.bind(view)

        graphHolder = GraphHolder(binding.graphContainer)

        tabAdapter = ListsTabPagerAdapter(
            onClickApp = viewModel::onClickApp,
            onClickTracker = viewModel::onClickTracker,
            onToggleHideNoTrackersApps = viewModel::onToggleHideNoTrackersApps
        )
        binding.listsPager.adapter = tabAdapter

        TabLayoutMediator(binding.listsTabs, binding.listsPager) { tab, position ->
            tab.text = getString(
                when (position) {
                    TrackerTab.APPS.position -> R.string.trackers_toggle_list_apps
                    else -> R.string.trackers_toggle_list_trackers
                }
            )
        }.attach()

        binding.listsPager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageScrollStateChanged(state: Int) {
                super.onPageScrollStateChanged(state)
                if (state == ViewPager2.SCROLL_STATE_IDLE) {
                    viewModel.onDisplayedItemChanged()
                }
            }
        })

        listenViewModel()
    }

    override fun onResume() {
        super.onResume()
        lifecycleScope.launch {
            viewModel.getStartPosition()?.let {
                binding.listsPager.currentItem = it
            }
        }
    }

    @OptIn(FlowPreview::class)
    private fun listenViewModel() {
        with(viewLifecycleOwner) {
            lifecycleScope.launch {
                repeatOnLifecycle(Lifecycle.State.STARTED) {
                    render(viewModel.state.value)
                    viewModel.state.collect(::render)
                }
            }

            lifecycleScope.launch {
                repeatOnLifecycle(Lifecycle.State.STARTED) {
                    viewModel.singleEvents.collect(::handleEvents)
                }
            }

            lifecycleScope.launch {
                repeatOnLifecycle(Lifecycle.State.RESUMED) {
                    trackersFragment?.refreshUiHeight()?.let { parentFlow ->
                        merge(
                            parentFlow,
                            viewModel.refreshUiHeight
                        )
                            .debounce(50)
                            .collect { updatePagerHeight() }
                    }
                }
            }

            lifecycleScope.launch {
                repeatOnLifecycle(Lifecycle.State.STARTED) {
                    viewModel.navigate.collect(findNavController()::safeNavigate)
                }
            }

            lifecycleScope.launch {
                repeatOnLifecycle(Lifecycle.State.STARTED) {
                    viewModel.doOnStartedState()
                }
            }
        }
    }

    private fun handleEvents(event: SingleEvent) {
        when (event) {
            is SingleEvent.ErrorEvent -> {
                displayToast(event.error)
            }
            is SingleEvent.OpenUrl -> {
                try {
                    startActivity(Intent(Intent.ACTION_VIEW, event.url))
                } catch (e: ActivityNotFoundException) {
                    Toast.makeText(
                        requireContext(),
                        R.string.error_no_activity_view_url,
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
    }

    private fun updatePagerHeight() {
        binding.listsPager.findViewHolderForAdapterPosition(binding.listsPager.currentItem)
            .let { currentViewHolder ->
                currentViewHolder?.itemView?.let { binding.listsPager.updatePagerHeightForChild(it) }
            }

        trackersFragment?.updatePagerHeight()
    }

    private fun displayToast(message: String) {
        Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun render(state: TrackersPeriodState) {
        graphHolder.onBind(state)
        tabAdapter.updateDataSet(state)
    }
}
