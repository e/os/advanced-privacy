/*
 * Copyright (C) 2023 MURENA SAS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.advancedprivacy.features.trackers.graph

import android.graphics.Canvas
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis.AxisDependency
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.formatter.ValueFormatter
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.listener.OnChartValueSelectedListener
import com.github.mikephil.charting.renderer.XAxisRenderer
import foundation.e.advancedprivacy.R
import foundation.e.advancedprivacy.common.extensions.dpToPxF
import foundation.e.advancedprivacy.databinding.TrackersItemGraphBinding
import foundation.e.advancedprivacy.features.trackers.TrackersPeriodState
import kotlin.math.floor

class GraphHolder(private val binding: TrackersItemGraphBinding) {

    companion object {
        private const val x_axis_graduations_count_threshold = 24
    }

    private val context = binding.root.context
    private val barChart = binding.graph

    private val periodMarker = PeriodMarkerView(context)

    private var data = emptyList<Pair<Int, Int>>()
    private var labels = emptyList<String>()
    private var graduations: List<String?> = emptyList()
    private val isHalfGraduations: Boolean
        get() = graduations.size > x_axis_graduations_count_threshold

    private var isHighlighted = false

    private val onChartValueSelectedListener = object : OnChartValueSelectedListener {
        override fun onValueSelected(e: Entry?, h: Highlight?) {
            h?.let {
                val index = it.x.toInt()
                if (index >= 0 &&
                    index < labels.size &&
                    index < data.size
                ) {
                    val period = labels[index]
                    val (blocked, leaked) = data[index]
                    periodMarker.setLabel(period, blocked, leaked)
                }
            }
            isHighlighted = true
        }

        override fun onNothingSelected() {
            isHighlighted = false
        }
    }

    private val xAxisRenderer = object : XAxisRenderer(
        barChart.viewPortHandler,
        barChart.xAxis,
        barChart.getTransformer(AxisDependency.LEFT)
    ) {
        override fun renderAxisLine(c: Canvas) {
            mAxisLinePaint.color = mXAxis.axisLineColor
            mAxisLinePaint.strokeWidth = mXAxis.axisLineWidth
            mAxisLinePaint.pathEffect = mXAxis.axisLineDashPathEffect

            // Bottom line
            c.drawLine(
                mViewPortHandler.contentLeft(),
                mViewPortHandler.contentBottom() - 5.dpToPxF(context),

                mViewPortHandler.contentRight(),
                mViewPortHandler.contentBottom() - 5.dpToPxF(context),
                mAxisLinePaint
            )
        }

        override fun renderGridLines(c: Canvas) {
            if (!mXAxis.isDrawGridLinesEnabled || !mXAxis.isEnabled) return
            val clipRestoreCount = c.save()
            c.clipRect(gridClippingRect)
            if (mRenderGridLinesBuffer.size != mAxis.mEntryCount * 2) {
                mRenderGridLinesBuffer = FloatArray(mXAxis.mEntryCount * 2)
            }

            val positions = mRenderGridLinesBuffer
            mXAxis.mEntries.forEachIndexed { index, value ->
                if ((index * 2 + 1) < positions.size) {
                    positions[index * 2] = value
                    positions[index * 2 + 1] = value
                }
            }

            mTrans.pointValuesToPixel(positions)

            val graduationPositions = positions.filterIndexed { index, _ -> index % 2 == 0 }

            setupGridPaint()
            val gridLinePath = mRenderGridLinesPath
            gridLinePath.reset()

            graduationPositions.forEachIndexed { i, x ->

                val graduationIndex = if (isHalfGraduations) 2 * i else i
                val hasLabel = graduations.getOrNull(graduationIndex) != null
                val bottomY = if (hasLabel) 0 else 3

                gridLinePath.moveTo(x, mViewPortHandler.contentBottom() - 5.dpToPxF(context))
                gridLinePath.lineTo(x, mViewPortHandler.contentBottom() - bottomY.dpToPxF(context))

                c.drawPath(gridLinePath, mGridPaint)

                gridLinePath.reset()
            }
            c.restoreToCount(clipRestoreCount)
        }
    }

    init {
        with(barChart) {
            description = null
            setTouchEnabled(true)
            setScaleEnabled(false)

            setDrawGridBackground(false)
            setDrawBorders(false)
            axisLeft.isEnabled = false
            axisRight.isEnabled = false

            legend.isEnabled = false

            extraTopOffset = 40f
            extraBottomOffset = 4f

            extraLeftOffset = 16f
            extraRightOffset = 16f

            offsetTopAndBottom(0)

            minOffset = 0f

            offsetTopAndBottom(0)

            setDrawValueAboveBar(false)

            periodMarker.chartView = barChart

            marker = periodMarker

            setOnChartValueSelectedListener(onChartValueSelectedListener)

            setXAxisRenderer(xAxisRenderer)
        }
    }

    fun onBind(state: TrackersPeriodState) {
        with(binding) {
            title.text = context.getString(state.title)
            val views = listOf(
                helperText,
                legendBlockedIcon,
                legendBlocked,
                legendAllowedIcon,
                legendAllowed,
                trackersDetected,
                trackersAllowed
            )

            if (state.isEmptyCalls()) {
                graph.visibility = View.INVISIBLE
                graphEmpty.isVisible = true
                views.forEach { it.isVisible = false }
            } else {
                graph.isVisible = true
                graphEmpty.isVisible = false
                views.forEach { it.isVisible = true }
                trackersDetected.text = context.getString(
                    R.string.trackers_graph_detected_trackers,
                    state.trackersCount
                )
                trackersAllowed.text = context.getString(
                    R.string.trackers_graph_allowed_trackers,
                    state.trackersAllowedCount
                )

                refreshDataSet(state)
            }
        }
    }

    private fun refreshDataSet(state: TrackersPeriodState) {
        data = state.callsBlockedNLeaked
        labels = state.periods
        graduations = state.graduations ?: emptyList()

        val trackersDataSet = BarDataSet(
            data.mapIndexed { index, value ->
                BarEntry(
                    index.toFloat(),
                    floatArrayOf(value.first.toFloat(), value.second.toFloat())
                )
            },
            ""
        )

        val blockedColor = ContextCompat.getColor(context, R.color.switch_track_on)
        val leakedColor = ContextCompat.getColor(context, R.color.red_off)

        trackersDataSet.colors = listOf(
            blockedColor,
            leakedColor
        )
        trackersDataSet.setDrawValues(false)

        barChart.data = BarData(trackersDataSet)
        prepareYAxis()
        prepareXAxis()

        barChart.invalidate()
    }

    private fun prepareYAxis() {
        val maxValue = data.maxOfOrNull { it.first + it.second } ?: 0

        barChart.axisLeft.apply {
            isEnabled = true

            setDrawGridLines(false)
            setDrawLabels(true)
            setCenterAxisLabels(false)
            setLabelCount(2, true)
            textColor = context.getColor(R.color.primary_text)
            valueFormatter = object : ValueFormatter() {
                override fun getAxisLabel(value: Float, axis: AxisBase?): String {
                    return if (value >= maxValue.toFloat()) maxValue.toString() else ""
                }
            }
        }
    }

    private val xAxisValueFormatter = object : ValueFormatter() {
        override fun getAxisLabel(value: Float, axis: AxisBase?): String {
            val index = floor(value).toInt() + 1
            return graduations.getOrNull(index) ?: ""
        }
    }

    private val halfGraduationsXAxisValueFormatter = object : ValueFormatter() {
        override fun getAxisLabel(value: Float, axis: AxisBase?): String {
            val index = floor(value).toInt() + 1
            return graduations.getOrNull(index) ?: graduations.getOrNull(index + 1) ?: ""
        }
    }

    private fun prepareXAxis() {
        barChart.xAxis.apply {
            isEnabled = true
            position = XAxis.XAxisPosition.BOTTOM

            setDrawGridLines(true)
            setDrawLabels(true)
            setCenterAxisLabels(false)
            textColor = context.getColor(R.color.primary_text)

            // setLabelCount can't have more than 25 labels.
            if (isHalfGraduations) {
                setLabelCount((graduations.size / 2) + 1, true)
                valueFormatter = halfGraduationsXAxisValueFormatter
            } else {
                setLabelCount(graduations.size + 1, true)
                valueFormatter = xAxisValueFormatter
            }
        }
    }
}
