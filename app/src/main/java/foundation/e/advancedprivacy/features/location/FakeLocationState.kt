/*
 * Copyright (C) 2022 - 2024 E FOUNDATION
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.advancedprivacy.features.location

import android.location.Location
import foundation.e.advancedprivacy.domain.entities.LocationMode
import foundation.e.advancedprivacy.domain.entities.ToggleableApp

data class FakeLocationState(
    val mode: LocationMode = LocationMode.REAL_LOCATION,
    val currentLocation: Location? = null,
    val specificLatitude: Float? = null,
    val specificLongitude: Float? = null,
    val forceRefresh: Boolean = false,
    val appsWithBlackList: List<ToggleableApp> = emptyList(),
    val showResetBlacklist: Boolean = false
)
