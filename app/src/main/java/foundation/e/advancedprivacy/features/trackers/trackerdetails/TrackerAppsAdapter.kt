/*
 * Copyright (C) 2024 E FOUNDATION
 * Copyright (C) 2023 MURENA SAS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.advancedprivacy.features.trackers.trackerdetails

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import foundation.e.advancedprivacy.databinding.ApptrackersItemTrackerToggleBinding
import foundation.e.advancedprivacy.domain.entities.ToggleableApp

class TrackerAppsAdapter(
    private val viewModel: TrackerDetailsViewModel
) : RecyclerView.Adapter<TrackerAppsAdapter.ViewHolder>() {

    class ViewHolder(
        private val binding: ApptrackersItemTrackerToggleBinding,
        private val viewModel: TrackerDetailsViewModel
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: ToggleableApp) {
            binding.title.text = item.app.label
            binding.toggle.apply {
                this.isChecked = item.isOn
                setOnClickListener {
                    viewModel.onToggleUnblockApp(item.app, isChecked)
                }
            }
        }
    }

    private var dataSet: List<ToggleableApp> = emptyList()

    fun updateDataSet(new: List<ToggleableApp>) {
        dataSet = new
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ApptrackersItemTrackerToggleBinding.inflate(LayoutInflater.from(parent.context), parent, false),
            viewModel
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(dataSet[position])
    }

    override fun getItemCount(): Int = dataSet.size
}
