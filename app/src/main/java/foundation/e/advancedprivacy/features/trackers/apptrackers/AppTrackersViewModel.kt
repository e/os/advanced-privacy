/*
 * Copyright (C) 2023 MURENA SAS
 * Copyright (C) 2021 - 2024 E FOUNDATION
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.advancedprivacy.features.trackers.apptrackers

import android.net.Uri
import androidx.annotation.StringRes
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import foundation.e.advancedprivacy.domain.entities.DisplayableApp
import foundation.e.advancedprivacy.domain.entities.FeatureMode
import foundation.e.advancedprivacy.domain.usecases.AppTrackersUseCase
import foundation.e.advancedprivacy.domain.usecases.GetQuickPrivacyStateUseCase
import foundation.e.advancedprivacy.domain.usecases.TrackersStateUseCase
import foundation.e.advancedprivacy.domain.usecases.TrackersStatisticsUseCase
import foundation.e.advancedprivacy.features.trackers.URL_LEARN_MORE_ABOUT_TRACKERS
import foundation.e.advancedprivacy.trackers.domain.entities.Tracker
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.merge
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class AppTrackersViewModel(
    private val app: DisplayableApp,
    private val appTrackersUseCase: AppTrackersUseCase,
    private val trackersStateUseCase: TrackersStateUseCase,
    private val trackersStatisticsUseCase: TrackersStatisticsUseCase,
    private val getQuickPrivacyStateUseCase: GetQuickPrivacyStateUseCase
) : ViewModel() {
    private val _state = MutableStateFlow(AppTrackersState())
    val state = _state.asStateFlow()

    private val _singleEvents = MutableSharedFlow<SingleEvent>()
    val singleEvents = _singleEvents.asSharedFlow()

    init {
        _state.update {
            it.copy(
                app = app
            )
        }
    }

    suspend fun doOnStartedState() = withContext(Dispatchers.IO) {
        merge(
            getQuickPrivacyStateUseCase.trackerMode.map {
                _state.update { s -> s.copy(isTrackersBlockingEnabled = it != FeatureMode.VULNERABLE) }
            },
            trackersStatisticsUseCase.listenUpdates().map { fetchStatistics() }
        ).collect()
    }

    fun onClickLearnMore() {
        viewModelScope.launch {
            _singleEvents.emit(SingleEvent.OpenUrl(Uri.parse(URL_LEARN_MORE_ABOUT_TRACKERS)))
        }
    }

    fun onToggleBlockAll(isBlocked: Boolean) {
        viewModelScope.launch(Dispatchers.IO) {
            if (!state.value.isTrackersBlockingEnabled) {
                _singleEvents.emit(SingleEvent.ToastTrackersControlDisabled)
            }
            appTrackersUseCase.toggleAppWhitelist(
                app,
                state.value.trackersWithBlockedList.map { it.first },
                isBlocked
            )
            updateWhitelist()
        }
    }

    fun onToggleTracker(tracker: Tracker, isBlocked: Boolean) {
        viewModelScope.launch(Dispatchers.IO) {
            if (!state.value.isTrackersBlockingEnabled) {
                _singleEvents.emit(SingleEvent.ToastTrackersControlDisabled)
            }

            trackersStateUseCase.blockTracker(app, tracker, isBlocked)
            updateWhitelist()
        }
    }

    fun onClickTracker(tracker: Tracker) {
        viewModelScope.launch(Dispatchers.IO) {
            tracker.link?.let {
                runCatching { Uri.parse(it) }.getOrNull()
            }?.let { _singleEvents.emit(SingleEvent.OpenUrl(it)) }
        }
    }

    fun onClickResetAllTrackers() {
        viewModelScope.launch(Dispatchers.IO) {
            appTrackersUseCase.clearWhitelist(app)
            updateWhitelist()
        }
    }

    private suspend fun fetchStatistics() = withContext(Dispatchers.IO) {
        val (blocked, leaked) = appTrackersUseCase.getCalls(app)
        val trackersWithBlockedList = appTrackersUseCase.getTrackersWithBlockedList(app)

        _state.update { s ->
            s.copy(
                leaked = leaked,
                blocked = blocked,
                isBlockingActivated = !trackersStateUseCase.isWhitelisted(app),
                isWhitelistEmpty = trackersStatisticsUseCase.isWhiteListEmpty(app),
                trackersWithBlockedList = trackersWithBlockedList
            )
        }
    }

    private suspend fun updateWhitelist() = withContext(Dispatchers.IO) {
        _state.update { s ->
            s.copy(
                isBlockingActivated = !trackersStateUseCase.isWhitelisted(app),
                trackersWithBlockedList = appTrackersUseCase.enrichWithBlockedState(
                    app,
                    s.trackersWithBlockedList.map { it.first }
                ),
                isWhitelistEmpty = trackersStatisticsUseCase.isWhiteListEmpty(app)
            )
        }
    }

    sealed class SingleEvent {
        data class ErrorEvent(@StringRes val errorResId: Int) : SingleEvent()
        data class OpenUrl(val url: Uri) : SingleEvent()
        object ToastTrackersControlDisabled : SingleEvent()
    }
}
