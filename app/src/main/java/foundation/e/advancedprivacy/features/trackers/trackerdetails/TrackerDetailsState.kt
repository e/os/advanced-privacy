/*
 * Copyright (C) 2024 E FOUNDATION
 * Copyright (C) 2023 MURENA SAS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.advancedprivacy.features.trackers.trackerdetails

import foundation.e.advancedprivacy.domain.entities.ToggleableApp
import foundation.e.advancedprivacy.trackers.domain.entities.Tracker

data class TrackerDetailsState(
    val tracker: Tracker? = null,
    val isBlockAllActivated: Boolean = false,
    val detectedCount: Int = 0,
    val blockedCount: Int = 0,
    val leakedCount: Int = 0,
    val appList: List<ToggleableApp> = emptyList(),
    val isTrackersBlockingEnabled: Boolean = false
)
