/*
 * Copyright (C) 2023 MURENA SAS
 * Copyright (C) 2022 - 2024 E FOUNDATION
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.advancedprivacy.features.trackers.apptrackers

import foundation.e.advancedprivacy.domain.entities.DisplayableApp
import foundation.e.advancedprivacy.trackers.domain.entities.Tracker

data class AppTrackersState(
    val app: DisplayableApp? = null,
    val isBlockingActivated: Boolean = false,
    val trackersWithBlockedList: List<Pair<Tracker, Boolean>> = emptyList(),
    val leaked: Int = 0,
    val blocked: Int = 0,
    val isTrackersBlockingEnabled: Boolean = false,
    val isWhitelistEmpty: Boolean = true
) {
    fun getTrackersCount() = trackersWithBlockedList.size

    fun getBlockedTrackersCount(): Int = trackersWithBlockedList.count { it.second }
}
