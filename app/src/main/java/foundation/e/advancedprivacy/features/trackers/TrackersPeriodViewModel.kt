/*
 * Copyright (C) 2022 - 2024 MURENA SAS
 * Copyright (C) 2021 E FOUNDATION
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.advancedprivacy.features.trackers

import android.net.Uri
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavDirections
import foundation.e.advancedprivacy.R
import foundation.e.advancedprivacy.domain.entities.DisplayableApp
import foundation.e.advancedprivacy.domain.usecases.TrackersAndAppsListsUseCase
import foundation.e.advancedprivacy.domain.usecases.TrackersScreenUseCase
import foundation.e.advancedprivacy.domain.usecases.TrackersStatisticsUseCase
import foundation.e.advancedprivacy.trackers.domain.entities.Tracker
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class TrackersPeriodViewModel(
    private val period: Period,
    private val trackersStatisticsUseCase: TrackersStatisticsUseCase,
    private val trackersAndAppsListsUseCase: TrackersAndAppsListsUseCase,
    private val trackersScreenUseCase: TrackersScreenUseCase
) : ViewModel() {

    private val _state = MutableStateFlow(
        TrackersPeriodState(
            title = when (period) {
                Period.DAY -> R.string.trackers_graph_title_day
                Period.MONTH -> R.string.trackers_graph_title_month
                Period.YEAR -> R.string.trackers_graph_title_year
            }
        )
    )

    val state = _state.asStateFlow()

    private val _singleEvents = MutableSharedFlow<SingleEvent>()
    val singleEvents = _singleEvents.asSharedFlow()

    private val _refreshUiHeight = MutableSharedFlow<Unit>()
    val refreshUiHeight = _refreshUiHeight.asSharedFlow()

    private val _navigate = MutableSharedFlow<NavDirections>()
    val navigate = _navigate.asSharedFlow()

    suspend fun doOnStartedState() = withContext(Dispatchers.IO) {
        trackersStatisticsUseCase.listenUpdates().collect {
            trackersStatisticsUseCase.getGraphData(period).let { graphData ->
                _state.update {
                    it.copy(
                        callsBlockedNLeaked = graphData.callsBlockedNLeaked,
                        periods = graphData.periods,
                        trackersCount = graphData.trackersCount,
                        trackersAllowedCount = graphData.trackersAllowedCount,
                        graduations = graphData.graduations
                    )
                }
            }

            trackersAndAppsListsUseCase.getAppsAndTrackersCounts(period).let { lists ->
                _state.update {
                    it.copy(
                        trackers = lists.trackers,
                        allApps = lists.allApps,
                        appsWithTrackers = lists.appsWithTrackers
                    )
                }
                _refreshUiHeight.emit(Unit)
            }
        }
    }

    fun onClickTracker(tracker: Tracker) = viewModelScope.launch {
        _navigate.emit(TrackersFragmentDirections.gotoTrackerDetailsFragment(trackerId = tracker.id))
    }

    fun onClickApp(app: DisplayableApp) = viewModelScope.launch {
        _navigate.emit(TrackersFragmentDirections.gotoAppTrackersFragment(appId = app.id))
    }

    fun onToggleHideNoTrackersApps() = viewModelScope.launch {
        _state.update { it.copy(hideNoTrackersApps = !it.hideNoTrackersApps) }
        _refreshUiHeight.emit(Unit)
    }

    fun onDisplayedItemChanged() = viewModelScope.launch {
        _refreshUiHeight.emit(Unit)
    }

    fun getStartPosition(): Int? {
        val startPosition = trackersScreenUseCase.getTrackerTabStartPosition()
        return startPosition.takeIf { it in 0..1 }
    }

    sealed class SingleEvent {
        data class ErrorEvent(val error: String) : SingleEvent()
        data class OpenUrl(val url: Uri) : SingleEvent()
    }
}
