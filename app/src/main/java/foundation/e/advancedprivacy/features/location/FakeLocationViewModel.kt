/*
 * Copyright (C) 2021, 2024 E FOUNDATION
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.advancedprivacy.features.location

import android.location.Location
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import foundation.e.advancedprivacy.domain.entities.DisplayableApp
import foundation.e.advancedprivacy.domain.usecases.FakeLocationStateUseCase
import foundation.e.advancedprivacy.domain.usecases.ListenLocationUseCase
import kotlin.time.Duration.Companion.milliseconds
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.merge
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class FakeLocationViewModel(
    private val fakeLocationStateUseCase: FakeLocationStateUseCase,
    private val listenLocationUseCase: ListenLocationUseCase
) : ViewModel() {
    companion object {
        private val SET_SPECIFIC_LOCATION_DELAY = 200.milliseconds
    }

    private val _state = MutableStateFlow(FakeLocationState())
    val state = _state.asStateFlow()

    val currentLocation: StateFlow<Location?> = listenLocationUseCase.currentLocation

    private val _singleEvents = MutableSharedFlow<SingleEvent>()
    val singleEvents = _singleEvents.asSharedFlow()

    private val specificLocationInputFlow = MutableSharedFlow<Action.SetSpecificLocationAction>()

    @OptIn(FlowPreview::class)
    suspend fun doOnStartedState() = withContext(Dispatchers.Main) {
        launch {
            merge(
                fakeLocationStateUseCase.configuredLocationMode.map { (mode, lat, lon) ->
                    _state.update { s ->
                        s.copy(
                            mode = mode,
                            specificLatitude = lat,
                            specificLongitude = lon
                        )
                    }
                },
                specificLocationInputFlow
                    .debounce(SET_SPECIFIC_LOCATION_DELAY).map { action ->
                        fakeLocationStateUseCase.setSpecificLocation(action.latitude, action.longitude)
                    },
                fakeLocationStateUseCase.appsWithBlacklist().map { apps ->
                    _state.update { s -> s.copy(appsWithBlackList = apps) }
                },
                fakeLocationStateUseCase.canResetBlacklist().map {
                    _state.update { s -> s.copy(showResetBlacklist = it) }
                }
            ).collect {}
        }
    }

    fun submitAction(action: Action) = viewModelScope.launch {
        when (action) {
            is Action.StartListeningLocation -> actionStartListeningLocation()
            is Action.StopListeningLocation -> listenLocationUseCase.stopListeningLocation()
            is Action.SetSpecificLocationAction -> setSpecificLocation(action)
            is Action.UseRandomLocationAction -> fakeLocationStateUseCase.setRandomLocation()
            is Action.UseRealLocationAction ->
                fakeLocationStateUseCase.stopFakeLocation()
        }
    }

    private suspend fun actionStartListeningLocation() {
        val started = listenLocationUseCase.startListeningLocation()
        if (!started) {
            _singleEvents.emit(SingleEvent.RequestLocationPermission)
        }
    }

    private suspend fun setSpecificLocation(action: Action.SetSpecificLocationAction) {
        specificLocationInputFlow.emit(action)
    }

    fun onToggleApp(app: DisplayableApp) = viewModelScope.launch(Dispatchers.IO) {
        fakeLocationStateUseCase.toggleBlacklist(app)
    }

    fun onClickResetAllApplications() = viewModelScope.launch(Dispatchers.IO) {
        fakeLocationStateUseCase.resetBlacklist()
    }

    sealed class SingleEvent {
        object RequestLocationPermission : SingleEvent()
        data class ErrorEvent(val error: String) : SingleEvent()
    }

    sealed class Action {
        object StartListeningLocation : Action()
        object StopListeningLocation : Action()
        object UseRealLocationAction : Action()
        object UseRandomLocationAction : Action()
        data class SetSpecificLocationAction(
            val latitude: Float,
            val longitude: Float
        ) : Action()
    }
}
