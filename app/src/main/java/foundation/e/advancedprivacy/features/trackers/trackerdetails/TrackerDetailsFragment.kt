/*
 * Copyright (C) 2023-2024 MURENA SAS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.advancedprivacy.features.trackers.trackerdetails

import android.content.ActivityNotFoundException
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat.getColor
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.divider.MaterialDividerItemDecoration
import com.google.android.material.snackbar.Snackbar
import foundation.e.advancedprivacy.R
import foundation.e.advancedprivacy.common.BigNumberFormatter
import foundation.e.advancedprivacy.common.NavToolbarFragment
import foundation.e.advancedprivacy.databinding.TrackerdetailsFragmentBinding
import foundation.e.advancedprivacy.features.trackers.setupDisclaimerBlock
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class TrackerDetailsFragment : NavToolbarFragment(R.layout.trackerdetails_fragment) {

    private val args: TrackerDetailsFragmentArgs by navArgs()
    private val viewModel: TrackerDetailsViewModel by viewModel { parametersOf(args.trackerId) }
    private val numberFormatter: BigNumberFormatter by lazy { BigNumberFormatter(requireContext()) }

    private lateinit var binding: TrackerdetailsFragmentBinding

    override fun getTitle(): CharSequence {
        return ""
    }

    private fun displayToast(message: String) {
        Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT)
            .show()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = TrackerdetailsFragmentBinding.bind(view)

        binding.blockAllToggle.setOnClickListener {
            viewModel.onToggleBlockAll(binding.blockAllToggle.isChecked)
        }

        binding.apps.apply {
            layoutManager = LinearLayoutManager(requireContext())
            setHasFixedSize(true)
            addItemDecoration(
                MaterialDividerItemDecoration(requireContext(), LinearLayoutManager.VERTICAL).apply {
                    dividerColor = getColor(requireContext(), R.color.divider)
                }
            )
            adapter = TrackerAppsAdapter(viewModel)
        }

        setupDisclaimerBlock(binding.disclaimerBlockTrackers.root, viewModel::onClickLearnMore)

        listenViewModel()
    }

    private fun listenViewModel() {
        with(viewLifecycleOwner) {
            lifecycleScope.launch {
                repeatOnLifecycle(Lifecycle.State.STARTED) {
                    viewModel.singleEvents.collect(::handleEvents)
                }
            }

            lifecycleScope.launch {
                repeatOnLifecycle(Lifecycle.State.STARTED) {
                    viewModel.doOnStartedState()
                }
            }

            lifecycleScope.launch {
                repeatOnLifecycle(Lifecycle.State.STARTED) {
                    render(viewModel.state.value)
                    viewModel.state.collect(::render)
                }
            }
        }
    }

    private fun handleEvents(event: TrackerDetailsViewModel.SingleEvent) {
        when (event) {
            is TrackerDetailsViewModel.SingleEvent.ErrorEvent ->
                displayToast(getString(event.errorResId))
            is TrackerDetailsViewModel.SingleEvent.ToastTrackersControlDisabled ->
                Snackbar.make(
                    binding.root,
                    R.string.apptrackers_tracker_control_disabled_message,
                    Snackbar.LENGTH_LONG
                ).show()
            is TrackerDetailsViewModel.SingleEvent.OpenUrl -> {
                try {
                    startActivity(Intent(Intent.ACTION_VIEW, event.url))
                } catch (e: ActivityNotFoundException) {
                    Toast.makeText(
                        requireContext(),
                        R.string.error_no_activity_view_url,
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
    }

    private fun render(state: TrackerDetailsState) {
        setTitle(state.tracker?.label)
        binding.subtitle.text = getString(R.string.trackerdetails_subtitle, state.tracker?.label)
        binding.dataAppCount.apply {
            primaryMessage.setText(R.string.trackerdetails_app_count_primary)
            number.text = state.detectedCount.toString()
            secondaryMessage.setText(R.string.trackerdetails_app_count_secondary)
        }

        binding.dataBlockedLeaks.apply {
            primaryMessage.setText(R.string.trackerdetails_blocked_leaks_primary)
            number.text = numberFormatter.format(state.blockedCount)
            secondaryMessage.text = getString(R.string.trackerdetails_blocked_leaks_secondary, numberFormatter.format(state.leakedCount))
        }

        binding.blockAllToggle.isChecked = state.isBlockAllActivated

        binding.apps.post {
            (binding.apps.adapter as TrackerAppsAdapter?)?.updateDataSet(state.appList)
        }
    }
}
