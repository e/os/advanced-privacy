/*
 * Copyright (C) 2023 MURENA SAS
 * Copyright (C) 2021 E FOUNDATION
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.advancedprivacy.features.trackers.apptrackers

import android.text.SpannableString
import android.text.style.UnderlineSpan
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import foundation.e.advancedprivacy.R
import foundation.e.advancedprivacy.databinding.ApptrackersItemTrackerToggleBinding
import foundation.e.advancedprivacy.trackers.domain.entities.Tracker

class ToggleTrackersAdapter(
    private val viewModel: AppTrackersViewModel
) : RecyclerView.Adapter<ToggleTrackersAdapter.ViewHolder>() {
    class ViewHolder(
        private val binding: ApptrackersItemTrackerToggleBinding,
        private val viewModel: AppTrackersViewModel
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: Pair<Tracker, Boolean>) {
            val label = item.first.label
            with(binding.title) {
                if (item.first.link != null) {
                    setTextColor(ContextCompat.getColor(context, R.color.accent))
                    val spannable = SpannableString(label)
                    spannable.setSpan(UnderlineSpan(), 0, spannable.length, 0)
                    text = spannable
                } else {
                    setTextColor(ContextCompat.getColor(context, R.color.primary_text))
                    text = label
                }
                setOnClickListener { viewModel.onClickTracker(item.first) }
            }
            with(binding.toggle) {
                isChecked = item.second

                setOnClickListener {
                    viewModel.onToggleTracker(item.first, isChecked)
                }
            }
        }
    }

    private var dataSet: List<Pair<Tracker, Boolean>> = emptyList()

    fun updateDataSet(new: List<Pair<Tracker, Boolean>>) {
        dataSet = new
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ApptrackersItemTrackerToggleBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ),
            viewModel
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val permission = dataSet[position]
        holder.bind(permission)
    }

    override fun getItemCount(): Int = dataSet.size
}
