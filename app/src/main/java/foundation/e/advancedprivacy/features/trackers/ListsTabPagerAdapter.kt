/*
 * Copyright (C) 2023 - 2024 MURENA SAS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package foundation.e.advancedprivacy.features.trackers

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.divider.MaterialDividerItemDecoration
import foundation.e.advancedprivacy.R
import foundation.e.advancedprivacy.common.BindingListAdapter
import foundation.e.advancedprivacy.common.BindingViewHolder
import foundation.e.advancedprivacy.common.extensions.dpToPx
import foundation.e.advancedprivacy.databinding.TrackersAppsListBinding
import foundation.e.advancedprivacy.databinding.TrackersItemAppBinding
import foundation.e.advancedprivacy.databinding.TrackersListBinding
import foundation.e.advancedprivacy.domain.entities.AppWithCount
import foundation.e.advancedprivacy.domain.entities.DisplayableApp
import foundation.e.advancedprivacy.domain.entities.TrackerWithCount
import foundation.e.advancedprivacy.trackers.domain.entities.Tracker

class ListsTabPagerAdapter(
    private val onClickTracker: (Tracker) -> Unit,
    private val onClickApp: (DisplayableApp) -> Unit,
    private val onToggleHideNoTrackersApps: () -> Unit
) : RecyclerView.Adapter<ListsTabPagerAdapter.ListsTabViewHolder>() {
    private var uiState: TrackersPeriodState = TrackersPeriodState()

    fun updateDataSet(state: TrackersPeriodState) {
        uiState = state
        notifyDataSetChanged()
    }

    override fun getItemViewType(position: Int): Int = position

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListsTabViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            TrackerTab.APPS.position -> {
                ListsTabViewHolder.AppsListViewHolder(
                    TrackersAppsListBinding.inflate(inflater, parent, false),
                    onClickApp,
                    onToggleHideNoTrackersApps
                )
            }
            else -> {
                ListsTabViewHolder.TrackersListViewHolder(
                    TrackersListBinding.inflate(inflater, parent, false),
                    onClickTracker
                )
            }
        }
    }

    override fun getItemCount(): Int {
        return 2
    }

    override fun onBindViewHolder(holder: ListsTabViewHolder, position: Int) {
        when (position) {
            TrackerTab.APPS.position -> {
                (holder as ListsTabViewHolder.AppsListViewHolder).onBind(uiState)
            }
            TrackerTab.TRACKERS.position -> {
                (holder as ListsTabViewHolder.TrackersListViewHolder).onBind(uiState.trackers ?: emptyList())
            }
        }
    }

    sealed class ListsTabViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        protected fun setupRecyclerView(recyclerView: RecyclerView) {
            recyclerView.apply {
                layoutManager = LinearLayoutManager(context)
                setHasFixedSize(true)
                isNestedScrollingEnabled = false
                addItemDecoration(
                    MaterialDividerItemDecoration(context, LinearLayoutManager.VERTICAL).apply {
                        dividerColor = ContextCompat.getColor(context, R.color.divider)
                        dividerInsetStart = 16.dpToPx(context)
                        dividerInsetEnd = 16.dpToPx(context)
                    }
                )
            }
        }

        class AppsListViewHolder(
            private val binding: TrackersAppsListBinding,
            private val onClickApp: (DisplayableApp) -> Unit,
            private val onToggleHideNoTrackersApps: () -> Unit
        ) : ListsTabViewHolder(binding.root) {
            private val adapter = object : BindingListAdapter<TrackersItemAppBinding, AppWithCount>() {
                override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BindingViewHolder<TrackersItemAppBinding> {
                    return BindingViewHolder(
                        TrackersItemAppBinding.inflate(
                            LayoutInflater.from(parent.context),
                            parent,
                            false
                        )
                    )
                }

                override fun onBindViewHolder(holder: BindingViewHolder<TrackersItemAppBinding>, position: Int) {
                    val item = dataSet[position]
                    holder.binding.icon.setImageDrawable(item.app.icon)
                    holder.binding.title.text = item.app.label
                    holder.binding.counts.text = itemView.context.getString(
                        R.string.trackers_list_app_trackers_counts, item.count.toString()
                    )
                    holder.binding.root.setOnClickListener {
                        onClickApp(item.app)
                    }
                }
            }

            init {
                setupRecyclerView(binding.list)
                binding.list.adapter = adapter
                binding.toggleNoTrackerApps.setOnClickListener { onToggleHideNoTrackersApps() }
            }

            fun onBind(uiState: TrackersPeriodState) {
                adapter.dataSet = (
                    if (uiState.hideNoTrackersApps) {
                        uiState.appsWithTrackers
                    } else {
                        uiState.allApps
                    }
                    ) ?: emptyList()

                binding.toggleNoTrackerApps.apply {
                    isCloseIconVisible = uiState.hideNoTrackersApps
                    isChecked = uiState.hideNoTrackersApps
                }
            }
        }

        class TrackersListViewHolder(
            binding: TrackersListBinding,
            private val onClickTracker: (Tracker) -> Unit
        ) : ListsTabViewHolder(binding.root) {

            private val adapter = object : BindingListAdapter<TrackersItemAppBinding, TrackerWithCount>() {
                override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BindingViewHolder<TrackersItemAppBinding> {
                    return BindingViewHolder(
                        TrackersItemAppBinding.inflate(
                            LayoutInflater.from(parent.context),
                            parent,
                            false
                        )
                    )
                }

                override fun onBindViewHolder(holder: BindingViewHolder<TrackersItemAppBinding>, position: Int) {
                    val item = dataSet[position]
                    holder.binding.icon.isVisible = false
                    holder.binding.title.text = item.tracker.label
                    holder.binding.counts.text = itemView.context.getString(
                        R.string.trackers_list_tracker_apps_counts, item.count.toString()
                    )
                    holder.binding.root.setOnClickListener { onClickTracker(item.tracker) }
                }
            }

            init {
                setupRecyclerView(binding.list)
                binding.list.adapter = adapter
            }

            fun onBind(trackers: List<TrackerWithCount>) {
                adapter.dataSet = trackers
            }
        }
    }
}
