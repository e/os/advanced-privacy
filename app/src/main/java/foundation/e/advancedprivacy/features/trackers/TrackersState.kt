/*
 * Copyright (C) 2023 - 2024 MURENA SAS
 * Copyright (C) 2022 E FOUNDATION
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.advancedprivacy.features.trackers

import androidx.annotation.StringRes
import foundation.e.advancedprivacy.R
import foundation.e.advancedprivacy.domain.entities.AppWithCount
import foundation.e.advancedprivacy.domain.entities.TrackerWithCount
import java.time.Instant
import java.time.ZonedDateTime
import java.time.temporal.ChronoUnit
import java.time.temporal.TemporalUnit

data class TrackersPeriodState(
    @StringRes val tabLabel: Int = R.string.empty,
    @StringRes val title: Int = R.string.empty,
    val callsBlockedNLeaked: List<Pair<Int, Int>> = emptyList(),
    val periods: List<String> = emptyList(),
    val trackersCount: Int = 0,
    val trackersAllowedCount: Int = 0,
    val graduations: List<String?>? = null,
    val allApps: List<AppWithCount>? = null,
    val trackers: List<TrackerWithCount>? = null,
    val appsWithTrackers: List<AppWithCount>? = null,
    val hideNoTrackersApps: Boolean = false
) {

    fun isEmptyCalls(): Boolean {
        return callsBlockedNLeaked.isEmpty() ||
            callsBlockedNLeaked.all { it.first == 0 && it.second == 0 }
    }
}

enum class TrackerTab(val position: Int) {
    APPS(0),
    TRACKERS(1)
}

enum class Period(val periodsCount: Int, val periodUnit: TemporalUnit) {
    DAY(24, ChronoUnit.HOURS),
    MONTH(30, ChronoUnit.DAYS),
    YEAR(12, ChronoUnit.MONTHS);

    fun getPeriodStart(): Instant {
        var start = ZonedDateTime.now()
            .minus(periodsCount.toLong(), periodUnit)
            .plus(1, periodUnit)
        var truncatePeriodUnit = periodUnit
        if (periodUnit === ChronoUnit.MONTHS) {
            start = start.withDayOfMonth(1)
            truncatePeriodUnit = ChronoUnit.DAYS
        }

        return start.truncatedTo(truncatePeriodUnit).toInstant()
    }
}
