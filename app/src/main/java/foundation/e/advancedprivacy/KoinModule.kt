/*
 * Copyright (C) 2023 - 2024 MURENA SAS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.advancedprivacy

import android.content.res.Resources
import android.graphics.drawable.Drawable
import android.os.Process
import foundation.e.advancedprivacy.core.coreModule
import foundation.e.advancedprivacy.data.repositories.AppListRepository
import foundation.e.advancedprivacy.data.repositories.LocalStateRepositoryImpl
import foundation.e.advancedprivacy.data.repositories.ResourcesRepository
import foundation.e.advancedprivacy.domain.entities.ApplicationDescription
import foundation.e.advancedprivacy.domain.entities.DisplayableApp
import foundation.e.advancedprivacy.domain.entities.ProfileType
import foundation.e.advancedprivacy.domain.repositories.LocalStateRepository
import foundation.e.advancedprivacy.domain.usecases.AppTrackersUseCase
import foundation.e.advancedprivacy.domain.usecases.FakeLocationForAppUseCase
import foundation.e.advancedprivacy.domain.usecases.FakeLocationStateUseCase
import foundation.e.advancedprivacy.domain.usecases.GetQuickPrivacyStateUseCase
import foundation.e.advancedprivacy.domain.usecases.IpScramblingStateUseCase
import foundation.e.advancedprivacy.domain.usecases.ListenLocationUseCase
import foundation.e.advancedprivacy.domain.usecases.ShowFeaturesWarningUseCase
import foundation.e.advancedprivacy.domain.usecases.TrackerDetailsUseCase
import foundation.e.advancedprivacy.domain.usecases.TrackersAndAppsListsUseCase
import foundation.e.advancedprivacy.domain.usecases.TrackersScreenUseCase
import foundation.e.advancedprivacy.domain.usecases.TrackersStateUseCase
import foundation.e.advancedprivacy.domain.usecases.TrackersStatisticsUseCase
import foundation.e.advancedprivacy.dummy.CityDataSource
import foundation.e.advancedprivacy.externalinterfaces.permissions.IPermissionsPrivacyModule
import foundation.e.advancedprivacy.features.dashboard.DashboardViewModel
import foundation.e.advancedprivacy.features.internetprivacy.InternetPrivacyViewModel
import foundation.e.advancedprivacy.features.location.FakeLocationViewModel
import foundation.e.advancedprivacy.features.trackers.Period
import foundation.e.advancedprivacy.features.trackers.TrackersPeriodViewModel
import foundation.e.advancedprivacy.features.trackers.TrackersViewModel
import foundation.e.advancedprivacy.features.trackers.apptrackers.AppTrackersViewModel
import foundation.e.advancedprivacy.features.trackers.trackerdetails.TrackerDetailsViewModel
import foundation.e.advancedprivacy.ipscrambler.ipScramblerModule
import foundation.e.advancedprivacy.permissions.externalinterfaces.PermissionsPrivacyModuleImpl
import foundation.e.advancedprivacy.trackers.data.TrackersRepository
import foundation.e.advancedprivacy.trackers.domain.entities.Tracker
import foundation.e.advancedprivacy.trackers.service.trackerServiceModule
import foundation.e.advancedprivacy.trackers.trackersModule
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.androidx.viewmodel.dsl.viewModelOf
import org.koin.core.module.dsl.singleOf
import org.koin.core.qualifier.named
import org.koin.dsl.module
import timber.log.Timber

val appModule = module {
    includes(coreModule, trackersModule, ipScramblerModule, trackerServiceModule)

    single<CoroutineScope> {
        CoroutineScope(
            SupervisorJob() +
                Dispatchers.IO +
                CoroutineExceptionHandler { _, throwable ->
                    Timber.e(throwable, "Uncaught error in backgroundScope")
                }
        )
    }

    factory<Resources> { androidContext().resources }
    single<LocalStateRepository> {
        LocalStateRepositoryImpl(context = androidContext())
    }

    single<ApplicationDescription>(named("AdvancedPrivacy")) {
        ApplicationDescription(
            packageName = androidContext().packageName,
            uid = Process.myUid(),
            label = androidContext().resources.getString(R.string.app_name),
            icon = null,
            profileId = -1,
            profileType = ProfileType.MAIN
        )
    }

    single<CharSequence>(named("SystemAppLabel")) {
        androidContext().getString(R.string.dummy_system_app_label)
    }
    single<Drawable>(named("SystemAppIcon")) {
        androidContext().getDrawable(R.drawable.ic_e_app_logo)!!
    }

    single<CharSequence>(named("CompatibilityAppLabel")) {
        androidContext().getString(R.string.dummy_apps_compatibility_app_label)
    }
    single<Drawable>(named("CompatibilityAppIcon")) {
        androidContext().getDrawable(R.drawable.ic_apps_compatibility_components)!!
    }

    single { CityDataSource }
    single { ResourcesRepository(androidContext()) }

    singleOf(::FakeLocationStateUseCase)

    single {
        ListenLocationUseCase(
            permissionsModule = get(),
            appDesc = get(named("AdvancedPrivacy")),
            appContext = androidContext()
        )
    }

    singleOf(::FakeLocationForAppUseCase)

    singleOf(::GetQuickPrivacyStateUseCase)
    single {
        IpScramblingStateUseCase(
            orbotSupervisor = get(),
            localStateRepository = get(),
            appListRepository = get(),
            backgroundScope = get()
        )
    }

    singleOf(::ShowFeaturesWarningUseCase)
    singleOf(::TrackersStateUseCase)
    singleOf(::TrackersStatisticsUseCase)
    singleOf(::TrackersAndAppsListsUseCase)

    singleOf(::AppTrackersUseCase)
    singleOf(::TrackerDetailsUseCase)
    single {
        TrackersScreenUseCase(localStateRepository = get())
    }

    single<IPermissionsPrivacyModule> {
        PermissionsPrivacyModuleImpl(context = androidContext())
    }

    viewModel { parameters ->
        val appListRepository: AppListRepository = get()
        val app = appListRepository.getAppById(parameters.get()) ?: DisplayableApp(
            id = "dummy-app",
            label = androidContext().resources.getString(R.string.app_name),
            icon = get(named("SystemAppIcon")),
            apps = setOf(get(named("AdvancedPrivacy"))),
            hasLocationPermission = true,
            hasInternetPermission = true,
            profileType = ProfileType.MAIN
        )

        AppTrackersViewModel(
            app = app,
            trackersStateUseCase = get(),
            trackersStatisticsUseCase = get(),
            getQuickPrivacyStateUseCase = get(),
            appTrackersUseCase = get()
        )
    }

    viewModel { parameters ->
        val trackersRepository: TrackersRepository = get()
        val tracker = trackersRepository.getTracker(parameters.get()) ?: Tracker("-1", emptySet(), "dummy", null)

        TrackerDetailsViewModel(
            tracker = tracker,
            trackersStateUseCase = get(),
            trackersStatisticsUseCase = get(),
            getQuickPrivacyStateUseCase = get(),
            trackerDetailsUseCase = get()
        )
    }

    viewModel { parameters ->

        val period: Period = runCatching { Period.valueOf(parameters.get()) }.getOrDefault(Period.DAY)

        TrackersPeriodViewModel(
            period = period,
            trackersStatisticsUseCase = get(),
            trackersAndAppsListsUseCase = get(),
            trackersScreenUseCase = get()
        )
    }

    viewModelOf(::TrackersViewModel)
    viewModelOf(::FakeLocationViewModel)
    viewModelOf(::InternetPrivacyViewModel)
    viewModelOf(::DashboardViewModel)
}
