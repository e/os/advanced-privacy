/*
 * Copyright (C) 2023 MURENA SAS
 * Copyright (C) 2022 - 2024 E FOUNDATION
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.advancedprivacy.common

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.CheckBox
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import foundation.e.advancedprivacy.R
import foundation.e.advancedprivacy.domain.entities.MainFeatures
import foundation.e.advancedprivacy.domain.entities.MainFeatures.FakeLocation
import foundation.e.advancedprivacy.domain.entities.MainFeatures.IpScrambling
import foundation.e.advancedprivacy.domain.entities.MainFeatures.TrackersControl
import foundation.e.advancedprivacy.domain.usecases.ShowFeaturesWarningUseCase
import foundation.e.advancedprivacy.domain.usecases.VpnSupervisorUseCase
import foundation.e.advancedprivacy.main.MainActivity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.koin.android.ext.android.inject
import org.koin.java.KoinJavaComponent.get
import timber.log.Timber

class WarningDialog : AppCompatActivity() {
    companion object {
        private const val PARAM_FEATURE = "feature"

        fun startListening(showFeaturesWarningUseCase: ShowFeaturesWarningUseCase, appScope: CoroutineScope, appContext: Context) {
            showFeaturesWarningUseCase.showWarning().map { feature ->
                appContext.startActivity(
                    createIntent(context = appContext, feature = feature)
                )
            }.launchIn(appScope)
        }

        private fun createIntent(context: Context, feature: MainFeatures): Intent {
            val intent = Intent(context, WarningDialog::class.java)
            intent.putExtra(PARAM_FEATURE, feature)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            return intent
        }
    }

    private val showFeaturesWarningUseCase: ShowFeaturesWarningUseCase by inject()
    private val vpnSupervisorUseCase: VpnSupervisorUseCase by inject()

    private var isWaitingForResult = false
    private lateinit var feature: MainFeatures

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setBackgroundDrawable(ColorDrawable(0))

        feature = try {
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.TIRAMISU) {
                intent.getParcelableExtra(PARAM_FEATURE, MainFeatures::class.java)!!
            } else {
                @Suppress("DEPRECATION")
                intent.getParcelableExtra(PARAM_FEATURE)!!
            }
        } catch (e: Exception) {
            Timber.e(e, "Missing mandatory activity parameter")
            finish()
            return
        }
        showWarningDialog(feature)
    }

    private fun showWarningDialog(feature: MainFeatures) {
        val builder = AlertDialog.Builder(this)
        builder.setOnDismissListener { if (!isWaitingForResult) finish() }

        val content: View = layoutInflater.inflate(R.layout.alertdialog_do_not_show_again, null)
        val checkbox = content.findViewById<CheckBox>(R.id.checkbox)
        builder.setView(content)

        builder.setMessage(
            when (feature) {
                is TrackersControl -> R.string.warningdialog_trackers_message
                is FakeLocation -> R.string.warningdialog_location_message
                is IpScrambling -> R.string.warningdialog_ipscrambling_message
            }
        )

        builder.setTitle(
            when (feature) {
                is TrackersControl -> R.string.warningdialog_trackers_title
                is FakeLocation -> R.string.warningdialog_location_title
                is IpScrambling -> R.string.warningdialog_ipscrambling_title
            }
        )

        builder.setPositiveButton(
            when (feature) {
                is IpScrambling -> R.string.warningdialog_ipscrambling_cta
                else -> R.string.ok
            }
        ) { _, _ ->
            lifecycleScope.launch {
                if (checkbox.isChecked) {
                    withContext(Dispatchers.IO) {
                        showFeaturesWarningUseCase.doNotShowAgain(feature)
                    }
                }

                val vpnDisclaimerIntent = (feature as? IpScrambling)?.startVpnDisclaimer

                if (vpnDisclaimerIntent != null) {
                    isWaitingForResult = true
                    launchAndroidVpnDisclaimer.launch(vpnDisclaimerIntent)
                } else {
                    finish()
                }
            }
        }

        if (feature is TrackersControl) {
            builder.setNeutralButton(R.string.warningdialog_trackers_secondary_cta) { _, _ ->
                MainActivity.deepLinkBuilder(this)
                    .setDestination(R.id.trackersFragment)
                    .createPendingIntent().send()

                finish()
            }
        }
        builder.show()
    }

    private val launchAndroidVpnDisclaimer = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        lifecycleScope.launch {
            if (result.resultCode == Activity.RESULT_OK) {
                vpnSupervisorUseCase.startVpnService(feature)
            } else {
                vpnSupervisorUseCase.cancelStartVpnService(feature)
            }
            finish()
        }
    }
}
