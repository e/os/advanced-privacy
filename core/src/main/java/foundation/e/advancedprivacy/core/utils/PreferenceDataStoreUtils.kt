/*
 * Copyright (C) 2024 E FOUNDATION
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package foundation.e.advancedprivacy.core.utils

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.firstOrNull
import kotlinx.coroutines.flow.map

suspend fun <T> DataStore<Preferences>.getValue(key: Preferences.Key<T>): T? {
    return data.map { it[key] }.firstOrNull()
}

suspend fun <T> DataStore<Preferences>.setValue(key: Preferences.Key<T>, value: T) {
    edit { preferences ->
        preferences[key] = value
    }
}

suspend fun <T> DataStore<Preferences>.removeKey(key: Preferences.Key<T>) {
    edit { preferences ->
        preferences.remove(key)
    }
}

suspend fun DataStore<Preferences>.toggleValue(key: Preferences.Key<Boolean>, enabled: Boolean?, defaultValue: Boolean = true) {
    edit { preferences ->
        val toSet = enabled ?: !(preferences[key]?: defaultValue)
        preferences[key] = toSet
    }
}

fun <T> DataStore<Preferences>.mapKey(key: Preferences.Key<T>): Flow<T?> {
    return data.map { preferences -> preferences[key] }.distinctUntilChanged()
}

fun <T> DataStore<Preferences>.mapKey(key: Preferences.Key<T>, default: T): Flow<T> {
    return data.map { preferences -> preferences[key] ?: default }.distinctUntilChanged()
}
