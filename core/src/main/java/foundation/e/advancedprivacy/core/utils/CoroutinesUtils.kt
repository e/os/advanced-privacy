/*
 * Copyright (C) 2023 MURENA SAS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.advancedprivacy.core.utils

import kotlin.contracts.ExperimentalContracts
import kotlinx.coroutines.CancellationException

@OptIn(ExperimentalContracts::class)
inline fun <T> runSuspendCatching(block: () -> T): Result<T> {
    // Deactivated because it causes falls positive warnings: https://stackoverflow.com/a/78915097
    // https://youtrack.jetbrains.com/issue/KT-63414, https://youtrack.jetbrains.com/issue/KT-63416
//    contract {
//        callsInPlace(block, InvocationKind.EXACTLY_ONCE)
//    }

    return runCatching(block).onFailure {
        if (it is CancellationException) {
            throw it
        }
    }
}

inline fun <R, T : R> Result<T>.recoverSuspendCatching(transform: (exception: Throwable) -> R): Result<R> {
    return when (val exception = exceptionOrNull()) {
        null -> this
        else -> runSuspendCatching { transform(exception) }
    }
}
