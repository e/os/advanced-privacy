/*
 * Copyright (C) 2022 - 2023 MURENA SAS
 * Copyright (C) 2022 - 2024 E FOUNDATION
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.advancedprivacy.data.repositories

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.ApplicationInfo
import android.content.pm.PackageInfo
import android.graphics.drawable.Drawable
import foundation.e.advancedprivacy.domain.entities.ApplicationDescription
import foundation.e.advancedprivacy.domain.entities.DisplayableApp
import foundation.e.advancedprivacy.domain.entities.ProfileType
import foundation.e.advancedprivacy.externalinterfaces.permissions.IPermissionsPrivacyModule
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.joinAll
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class AppListRepository(
    private val permissionsModule: IPermissionsPrivacyModule,
    private val context: Context,
    private val compatibilityAppLabel: CharSequence,
    private val compatibilityAppIcon: Drawable,
    private val systemAppLabel: CharSequence,
    private val systemAppIcon: Drawable,
    private val backgroundScope: CoroutineScope
) {
    companion object {
        private const val PNAME_SETTINGS = "com.android.settings"
        private const val PNAME_PWAPLAYER = "foundation.e.pwaplayer"
        private const val PNAME_INTENT_VERIFICATION = "com.android.statementservice"
        const val PNAME_MICROG_SERVICES_CORE = "com.google.android.gms"
        const val PNAME_FUSED_LOCATION = "com.android.location.fused"
        const val PNAME_ANDROID_SYSTEM = "android"

        private val compatibilityInternetPNames = setOf(
            PNAME_PWAPLAYER,
            PNAME_INTENT_VERIFICATION,
            PNAME_MICROG_SERVICES_CORE
        )

        private val compatibilityLocationPNames = setOf(
            PNAME_PWAPLAYER,
            PNAME_INTENT_VERIFICATION
        )

        private val compatibilityPNames = compatibilityInternetPNames + compatibilityLocationPNames
    }

    private val _displayableApps = MutableStateFlow<Set<DisplayableApp>>(emptySet())
    val displayableApps: StateFlow<Set<DisplayableApp>> = _displayableApps

    fun getAppById(id: String): DisplayableApp? = _displayableApps.value.find { it.id == id }

    fun getInternetAppByApId(apId: String): DisplayableApp? {
        return _displayableApps.value.find { app -> app.hasInternetPermission && app.apps.any { it.apId == apId } }
    }

    private var appsByUid = mapOf<Int, ApplicationDescription>()
    private var appsByAPId = mapOf<String, ApplicationDescription>()

    fun getApp(appUid: Int): ApplicationDescription? {
        return appsByUid[appUid] ?: run {
            runBlocking { refreshAppDescriptions(fetchMissingIcons = false, force = true)?.join() }
            appsByUid[appUid]
        }
    }

    fun getApp(apId: String): ApplicationDescription? {
        if (apId.isBlank()) return null

        return appsByAPId[apId] ?: run {
            runBlocking { refreshAppDescriptions(fetchMissingIcons = false, force = true)?.join() }
            appsByAPId[apId]
        }
    }

    private val appResources = mutableMapOf<String, Pair<CharSequence, Drawable>>()

    private suspend fun fetchAppDescriptions(fetchMissingIcons: Boolean = false) {
        val compatibilyInternetApps = mutableSetOf<ApplicationDescription>()
        val compatibilyLocationApps = mutableSetOf<ApplicationDescription>()
        val systemInternetApps = mutableSetOf<ApplicationDescription>()
        val systemLocationApps = mutableSetOf<ApplicationDescription>()
        val appDescs = mutableSetOf<ApplicationDescription>()
        val tempDisplayableApp = mutableSetOf<DisplayableApp>()

        val fetchResourcesJobs: MutableList<Job> = mutableListOf()

        val launcherPackageNames = context.packageManager.queryIntentActivities(
            Intent(Intent.ACTION_MAIN, null).apply { addCategory(Intent.CATEGORY_LAUNCHER) },
            0
        ).mapNotNull { it.activityInfo?.packageName }

        permissionsModule.getApplications().forEach { (appDesc: ApplicationDescription, pkgInfo: PackageInfo) ->
            val hasInternetPermission = hasInternetPermission(pkgInfo)
            val hasLocationPermission = hasLocationPermission(pkgInfo)
            val applicationInfo = pkgInfo.applicationInfo

            when {
                pkgInfo.packageName in compatibilityPNames -> {
                    if (pkgInfo.packageName in compatibilityInternetPNames) {
                        compatibilyInternetApps.add(appDesc)
                    }

                    if (pkgInfo.packageName in compatibilityLocationPNames) {
                        compatibilyLocationApps.add(appDesc)
                    }
                    appDescs.add(appDesc)
                }

                applicationInfo == null -> {} // Skip apps with missing applicationInfo.

                !isNotHiddenSystemApp(applicationInfo, launcherPackageNames) -> {
                    if (hasInternetPermission) {
                        systemInternetApps.add(appDesc)
                    }

                    if (hasLocationPermission) {
                        systemLocationApps.add(appDesc)
                    }
                    appDescs.add(appDesc)
                }

                hasInternetPermission || hasLocationPermission -> {
                    appDescs.add(appDesc)
                    val onResourcesReady: (CharSequence, Drawable) -> Unit = { label, icon ->
                        tempDisplayableApp.add(
                            buildDisplayableApp(
                                appDesc,
                                label,
                                icon,
                                hasInternetPermission,
                                hasLocationPermission
                            )
                        )
                    }

                    val resources = appResources[appDesc.apId]
                    if (resources != null) {
                        val (label, icon) = resources
                        onResourcesReady(label, icon)
                    } else {
                        fetchResourcesJobs.add(fetchResources(appDesc, applicationInfo, onResourcesReady))
                    }
                }

                else -> {} // Skip other apps.
            }
        }

        updateMaps(appDescs.toList())

        val displayableAppsJob = backgroundScope.launch {
            fetchResourcesJobs.joinAll()
            updateDisplayableApps(
                tempDisplayableApp,
                compatibilyInternetApps,
                compatibilyLocationApps,
                systemInternetApps,
                systemLocationApps
            )
        }

        if (fetchMissingIcons) {
            displayableAppsJob.join()
        }
    }

    private fun fetchResources(
        appDesc: ApplicationDescription,
        applicationInfo: ApplicationInfo,
        onResourcesReady: (CharSequence, Drawable) -> Unit
    ): Job {
        val id = appDesc.apId
        return backgroundScope.launch {
            val label = permissionsModule.getApplicationLabel(applicationInfo)
            val icon = permissionsModule.getApplicationIcon(appDesc)

            if (icon != null) {
                appResources[id] = label to icon
                onResourcesReady(label, icon)
            }
        }
    }

    private fun buildDisplayableApp(
        appDesc: ApplicationDescription,
        label: CharSequence,
        icon: Drawable,
        hasInternetPermission: Boolean,
        hasLocationPermission: Boolean
    ): DisplayableApp {
        return DisplayableApp(
            id = appDesc.apId,
            label = label,
            icon = icon,
            apps = setOf(appDesc),
            hasLocationPermission = hasLocationPermission,
            hasInternetPermission = hasInternetPermission,
            profileType = appDesc.profileType
        )
    }

    private fun updateMaps(apps: List<ApplicationDescription>) {
        val byUid = mutableMapOf<Int, ApplicationDescription>()
        val byApId = mutableMapOf<String, ApplicationDescription>()
        apps.forEach { app ->
            byUid[app.uid]?.run { packageName > app.packageName } == true
            if (byUid[app.uid].let { it == null || it.packageName > app.packageName }) {
                byUid[app.uid] = app
            }

            byApId[app.apId] = app
        }
        appsByUid = byUid
        appsByAPId = byApId
    }

    private fun updateDisplayableApps(
        tempDisplayableApp: MutableSet<DisplayableApp>,
        compatibilyInternetApps: MutableSet<ApplicationDescription>,
        compatibilyLocationApps: MutableSet<ApplicationDescription>,
        systemInternetApps: MutableSet<ApplicationDescription>,
        systemLocationApps: MutableSet<ApplicationDescription>
    ) {
        tempDisplayableApp.add(
            DisplayableApp(
                id = "compatibility-internet",
                label = compatibilityAppLabel,
                icon = compatibilityAppIcon,
                apps = compatibilyInternetApps,
                hasLocationPermission = false,
                hasInternetPermission = true,
                profileType = ProfileType.MAIN
            )
        )

        tempDisplayableApp.add(
            DisplayableApp(
                id = "compatibility-location",
                label = compatibilityAppLabel,
                icon = compatibilityAppIcon,
                apps = compatibilyLocationApps,
                hasLocationPermission = true,
                hasInternetPermission = false,
                profileType = ProfileType.MAIN
            )
        )

        tempDisplayableApp.add(
            DisplayableApp(
                id = "system-internet",
                label = systemAppLabel,
                icon = systemAppIcon,
                apps = systemInternetApps,
                hasLocationPermission = false,
                hasInternetPermission = true,
                profileType = ProfileType.MAIN
            )
        )

        tempDisplayableApp.add(
            DisplayableApp(
                id = "system-location",
                label = systemAppLabel,
                icon = systemAppIcon,
                apps = systemLocationApps,
                hasLocationPermission = true,
                hasInternetPermission = false,
                profileType = ProfileType.MAIN
            )
        )

        _displayableApps.update {
            tempDisplayableApp
        }
    }

    private var lastFetchApps = 0
    private var refreshAppJob: Job? = null

    fun refreshAppDescriptions(fetchMissingIcons: Boolean = true, force: Boolean = false): Job? {
        if (refreshAppJob == null || refreshAppJob?.isCompleted == true) {
            refreshAppJob = backgroundScope.launch {
                if (appsByUid.isEmpty() || appsByAPId.isEmpty() ||
                    force || context.packageManager.getChangedPackages(lastFetchApps) != null
                ) {
                    fetchAppDescriptions(fetchMissingIcons = fetchMissingIcons)
                    if (fetchMissingIcons) {
                        lastFetchApps = context.packageManager.getChangedPackages(lastFetchApps)
                            ?.sequenceNumber ?: lastFetchApps
                    }
                }
            }
        }

        return refreshAppJob
    }

    private fun hasInternetPermission(packageInfo: PackageInfo): Boolean {
        return packageInfo.requestedPermissions?.contains(Manifest.permission.INTERNET) == true
    }

    private fun hasLocationPermission(packageInfo: PackageInfo): Boolean {
        return packageInfo.requestedPermissions?.contains(Manifest.permission.ACCESS_FINE_LOCATION) == true ||
            packageInfo.requestedPermissions?.contains(Manifest.permission.ACCESS_COARSE_LOCATION) == true
    }

    @Suppress("ReturnCount")
    private fun isNotHiddenSystemApp(app: ApplicationInfo, launcherApps: List<String>): Boolean {
        if (app.packageName == PNAME_SETTINGS) {
            return false
        } else if (app.packageName == PNAME_PWAPLAYER) {
            return true
        } else if (app.hasFlag(ApplicationInfo.FLAG_UPDATED_SYSTEM_APP)) {
            return true
        } else if (!app.hasFlag(ApplicationInfo.FLAG_SYSTEM)) {
            return true
        } else if (launcherApps.contains(app.packageName)) {
            return true
        }
        return false
    }

    private fun ApplicationInfo.hasFlag(flag: Int) = (flags and flag) == 1
}
