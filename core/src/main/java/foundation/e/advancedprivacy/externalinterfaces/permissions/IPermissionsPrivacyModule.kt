/*
 * Copyright (C) 2022 - 2023 MURENA SAS
 * Copyright (C) 2021 - 2024 E FOUNDATION
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package foundation.e.advancedprivacy.externalinterfaces.permissions

import android.app.NotificationChannel
import android.content.pm.ApplicationInfo
import android.content.pm.PackageInfo
import android.graphics.drawable.Drawable
import foundation.e.advancedprivacy.domain.entities.AppOpModes
import foundation.e.advancedprivacy.domain.entities.ApplicationDescription
import foundation.e.advancedprivacy.domain.entities.PermissionDescription
import foundation.e.advancedprivacy.domain.entities.ProfileType

/**
 * List applications and manage theirs permissions.
 */
interface IPermissionsPrivacyModule {

    fun buildApplicationDescription(
        appInfo: ApplicationInfo,
        profileId: Int = -1,
        profileType: ProfileType = ProfileType.MAIN
    ): ApplicationDescription

    fun getApplications(): List<Pair<ApplicationDescription, PackageInfo>>

    /**
     * List of permissions names used by an app, specified by its [packageName].
     * @param packageName the appId of the app
     * @return the list off permission, in the "android.permission.PERMISSION" format.
     */
    fun getPermissions(packageName: String): List<String>

    fun getPermissionDescription(permissionName: String): PermissionDescription

    /**
     * Get the filled up [ApplicationDescription] for the app specified by its [packageName]
     * @param packageName the appId of the app
     * @return the informations about the app.
     */
    fun getApplicationDescription(packageName: String, withIcon: Boolean = true): ApplicationDescription

    /**
     * Check if the current runtime permission is granted for the specified app.
     *
     * @param packageName the packageName of the app
     * @param permissionName the name of the permission in "android.permission.PERMISSION" format.
     * @return the current status for this permission.
     */
    fun isDangerousPermissionGranted(packageName: String, permissionName: String): Boolean

    /**
     * Get the appOps mode for the specified [appOpPermissionName] of the specified application.
     *
     * @param appDesc the application
     * @param appOpPermissionName the AppOps permission name.
     * @return mode, as a [AppOpModes]
     */
    fun getAppOpMode(appDesc: ApplicationDescription, appOpPermissionName: String): AppOpModes

    /**
     * Grant or revoke the specified permission for the specigfied app.
     * If their is not enough privileges to get the permission, return the false
     *
     * @param appDesc the application
     * @param permissionName the name of the permission in "android.permission.PERMISSION" format.
     * @param grant true grant the permission, false revoke it.
     * @return true if the permission is or has just been granted, false if
     * user has to do it himself.
     */
    fun toggleDangerousPermission(appDesc: ApplicationDescription, permissionName: String, grant: Boolean): Boolean

    /**
     * Change the appOp Mode for the specified appOpPermission and application.
     * @param appDesc the application
     * @param appOpPermissionName the AppOps permission name.
     * @return true if the mode has been changed, false if
     * user has to do it himself.
     */
    fun setAppOpMode(appDesc: ApplicationDescription, appOpPermissionName: String, status: AppOpModes): Boolean

    /**
     * Return true if the application is flagged Dangerous.
     */
    fun isPermissionsDangerous(permissionName: String): Boolean

    /**
     * Get the application icon.
     */
    fun getApplicationIcon(packageName: String): Drawable?

    /**
     * Get the application icon.
     */
    fun getApplicationIcon(app: ApplicationDescription): Drawable?

    fun getApplicationLabel(appInfo: ApplicationInfo): CharSequence

    /**
     *  Authorize the specified package to be used as Vpn.
     *  @return true if authorization has been set, false if an error has occurred.
     */
    fun setVpnPackageAuthorization(packageName: String): Boolean

    /**
     * Returns the package name of the currently set always-on VPN application, or null.
     */
    fun getAlwaysOnVpnPackage(): String?

    /** Returns the dummy package name used by Android to highlight legacy VPN */
    fun getLegacyVpnDummyPackage(): String

    /**
     * Allows users to block notifications sent through this channel, if this channel belongs to
     * a package that is signed with the system signature.
     *
     * If the channel does not belong to a package that is signed with the system signature, this
     * method does nothing, since such channels are blockable by default and cannot be set to be
     * unblockable.
     */
    fun setBlockable(notificationChannel: NotificationChannel)
}
