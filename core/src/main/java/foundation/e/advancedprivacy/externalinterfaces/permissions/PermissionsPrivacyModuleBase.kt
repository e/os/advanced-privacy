/*
 * Copyright (C) 2022 - 2023 MURENA SAS
 * Copyright (C) 2021 - 2024 E FOUNDATION
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.advancedprivacy.externalinterfaces.permissions

import android.app.AppOpsManager
import android.content.Context
import android.content.pm.ApplicationInfo
import android.content.pm.PackageManager
import android.content.pm.PermissionInfo
import android.content.pm.PermissionInfo.PROTECTION_DANGEROUS
import android.graphics.drawable.Drawable
import android.os.Build
import android.util.Log
import foundation.e.advancedprivacy.domain.entities.AppOpModes
import foundation.e.advancedprivacy.domain.entities.ApplicationDescription
import foundation.e.advancedprivacy.domain.entities.PermissionDescription
import foundation.e.advancedprivacy.domain.entities.ProfileType

/**
 * Implementation of the commons functionality between privileged and standard
 * versions of the module.
 * @param context an Android context, to retrieve packageManager for example.
 */
abstract class PermissionsPrivacyModuleBase(protected val context: Context) : IPermissionsPrivacyModule {

    companion object {
        private const val TAG = "PermissionsModule"
    }

    /**
     * @see IPermissionsPrivacyModule.getInstalledApplications
     */
    override fun getApplicationDescription(packageName: String, withIcon: Boolean): ApplicationDescription {
        val appDesc = buildApplicationDescription(context.packageManager.getApplicationInfo(packageName, 0))
        if (withIcon) {
            appDesc.icon = getApplicationIcon(appDesc.packageName)
        }
        return appDesc
    }

    /**
     * * @see IPermissionsPrivacyModule.getPermissions
     */
    override fun getPermissions(packageName: String): List<String> {
        val packageInfo = context.packageManager.getPackageInfo(packageName, PackageManager.GET_PERMISSIONS)
        return packageInfo.requestedPermissions?.asList() ?: emptyList()
    }

    override fun getPermissionDescription(permissionName: String): PermissionDescription {
        val info = context.packageManager.getPermissionInfo(permissionName, 0)
        return PermissionDescription(
            name = permissionName,
            isDangerous = isPermissionsDangerous(info),
            group = null,
            label = info.loadLabel(context.packageManager),
            description = info.loadDescription(context.packageManager)
        )
    }

    /**
     * @see IPermissionsPrivacyModule.isDangerousPermissionGranted
     */
    override fun isDangerousPermissionGranted(packageName: String, permissionName: String): Boolean {
        return context.packageManager
            .checkPermission(permissionName, packageName) == PackageManager.PERMISSION_GRANTED
    }

    // on google version, work only for the current package.
    @Suppress("DEPRECATION")
    override fun getAppOpMode(appDesc: ApplicationDescription, appOpPermissionName: String): AppOpModes {
        val appOps = context.getSystemService(Context.APP_OPS_SERVICE) as AppOpsManager

        val mode = if (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) {
            appOps.checkOpNoThrow(
                appOpPermissionName,

                appDesc.uid,
                appDesc.packageName
            )
        } else {
            appOps.unsafeCheckOpNoThrow(
                appOpPermissionName,
                appDesc.uid,
                appDesc.packageName
            )
        }

        return AppOpModes.getByModeValue(mode)
    }

    override fun isPermissionsDangerous(permissionName: String): Boolean {
        try {
            val permissionInfo = context.packageManager.getPermissionInfo(permissionName, 0)
            return isPermissionsDangerous(permissionInfo)
        } catch (e: Exception) {
            Log.w(TAG, "exception in isPermissionsDangerous(String)", e)
            return false
        }
    }

    @Suppress("DEPRECATION")
    private fun isPermissionsDangerous(permissionInfo: PermissionInfo): Boolean {
        try {
            return if (Build.VERSION.SDK_INT < Build.VERSION_CODES.P) {
                permissionInfo.protectionLevel and PROTECTION_DANGEROUS == 1
            } else {
                permissionInfo.protection == PROTECTION_DANGEROUS
            }
        } catch (e: Exception) {
            Log.w(TAG, "exception in isPermissionsDangerous(PermissionInfo)", e)
            return false
        }
    }

    override fun buildApplicationDescription(appInfo: ApplicationInfo, profileId: Int, profileType: ProfileType): ApplicationDescription {
        return ApplicationDescription(
            packageName = appInfo.packageName,
            uid = appInfo.uid,
            label = null,
            icon = null,
            profileId = profileId,
            profileType = profileType
        )
    }

    override fun getApplicationLabel(appInfo: ApplicationInfo): CharSequence {
        return context.packageManager.getApplicationLabel(appInfo)
    }

    fun getApplicationIcon(appInfo: ApplicationInfo): Drawable? {
        return runCatching { context.packageManager.getApplicationIcon(appInfo) }.getOrNull()
    }

    override fun getApplicationIcon(packageName: String): Drawable? {
        return runCatching { context.packageManager.getApplicationIcon(packageName) }.getOrNull()
    }
}
