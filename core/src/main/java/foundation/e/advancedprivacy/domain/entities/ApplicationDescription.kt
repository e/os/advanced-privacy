/*
 * Copyright (C) 2023 MURENA SAS
 * Copyright (C) 2022 E FOUNDATION
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.advancedprivacy.domain.entities

import android.graphics.drawable.Drawable

/**
 * Useful informations to identify and describe an application.
 */
data class ApplicationDescription(
    val packageName: String,
    val uid: Int,
    val profileId: Int,
    val profileType: ProfileType,
    var label: CharSequence?,
    var icon: Drawable?
) {
    val profileFlag = when (profileType) {
        ProfileType.MAIN -> PROFILE_FLAG_MAIN
        ProfileType.WORK -> PROFILE_FLAG_WORK
        else -> profileId
    }

    val apId: String get() = "${profileFlag}_$packageName"

    companion object {
        const val PROFILE_FLAG_MAIN = -1
        const val PROFILE_FLAG_WORK = -2
    }
}

enum class ProfileType {
    MAIN,
    WORK,
    OTHER
}
