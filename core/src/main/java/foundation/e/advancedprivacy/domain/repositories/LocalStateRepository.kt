/*
 * Copyright (C) 2023 - 2024 MURENA SAS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package foundation.e.advancedprivacy.domain.repositories

import foundation.e.advancedprivacy.domain.entities.ApplicationDescription
import foundation.e.advancedprivacy.domain.entities.DisplayableApp
import foundation.e.advancedprivacy.domain.entities.FeatureState
import foundation.e.advancedprivacy.domain.entities.MainFeatures
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharedFlow

interface LocalStateRepository {
    val blockTrackers: Flow<Boolean>
    suspend fun toggleBlockTrackers(enabled: Boolean?)

    val areAllTrackersBlocked: MutableStateFlow<Boolean>

    val fakeLocationEnabled: Flow<Boolean>
    suspend fun toggleFakeLocation(enabled: Boolean?)

    val fakeLocation: Flow<Pair<Float, Float>>
    suspend fun getFakeLocation(): Pair<Float, Float>
    suspend fun setFakeLocation(latLon: Pair<Float, Float>)

    val fakeLocationWhitelistedApps: Flow<Set<String>>

    suspend fun toggleAppFakeLocationWhitelisted(app: DisplayableApp)
    suspend fun resetFakeLocationWhitelistedApp()

    val ipScramblingEnabled: Flow<Boolean>
    suspend fun toggleIpScrambling(enabled: Boolean?)

    val internetPrivacyMode: MutableStateFlow<FeatureState>

    suspend fun emitStartVpnDisclaimer(feature: MainFeatures)

    val startVpnDisclaimer: SharedFlow<MainFeatures>

    suspend fun emitOtherVpnRunning(appDesc: ApplicationDescription)
    val otherVpnRunning: SharedFlow<ApplicationDescription>

    suspend fun isFirstBoot(): Boolean
    suspend fun setFirstBoot(isStillFirstBoot: Boolean)

    suspend fun isHideWarningTrackers(): Boolean
    suspend fun hideWarningTrackers(hide: Boolean)

    suspend fun isHideWarningLocation(): Boolean
    suspend fun hideWarningLocation(hide: Boolean)

    suspend fun isHideWarningIpScrambling(): Boolean
    suspend fun hideWarningIpScrambling(hide: Boolean)

    suspend fun getTrackersScreenLastPosition(): Int
    suspend fun setTrackersScreenLastPosition(position: Int)

    var trackersScreenTabStartPosition: Int
}
