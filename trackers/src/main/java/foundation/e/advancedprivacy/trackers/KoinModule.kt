/*
 * Copyright (C) 2023 MURENA SAS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.advancedprivacy.trackers

import foundation.e.advancedprivacy.data.repositories.RemoteTrackersListRepository
import foundation.e.advancedprivacy.trackers.data.StatsDatabase
import foundation.e.advancedprivacy.trackers.data.TrackersRepository
import foundation.e.advancedprivacy.trackers.data.WhitelistRepository
import foundation.e.advancedprivacy.trackers.domain.usecases.FilterHostnameUseCase
import foundation.e.advancedprivacy.trackers.domain.usecases.UpdateTrackerListUseCase
import org.koin.android.ext.koin.androidContext
import org.koin.core.module.dsl.factoryOf
import org.koin.core.module.dsl.singleOf
import org.koin.core.qualifier.named
import org.koin.dsl.module

val trackersModule = module {

    factoryOf(::RemoteTrackersListRepository)
    factoryOf(::UpdateTrackerListUseCase)

    singleOf(::TrackersRepository)
    single {
        StatsDatabase(context = androidContext())
    }

    single {
        WhitelistRepository(
            context = androidContext(),
            appListRepository = get()
        )
    }

    factory {
        FilterHostnameUseCase(
            trackersRepository = get(),
            whitelistRepository = get(),
            appDesc = get(named("AdvancedPrivacy")),
            context = androidContext(),
            database = get(),
            appListRepository = get()
        )
    }
}
