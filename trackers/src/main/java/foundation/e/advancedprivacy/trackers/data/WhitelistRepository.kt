/*
 * Copyright (C) 2023 MURENA SAS
 * Copyright (C) 2022 E FOUNDATION
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.advancedprivacy.trackers.data

import android.content.Context
import android.content.SharedPreferences
import foundation.e.advancedprivacy.data.repositories.AppListRepository
import foundation.e.advancedprivacy.domain.entities.ApplicationDescription
import foundation.e.advancedprivacy.trackers.domain.entities.Tracker
import java.io.File
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext

class WhitelistRepository(
    context: Context,
    private val appListRepository: AppListRepository
) {
    private var appsWhitelist: Set<String> = HashSet()
    private var appUidsWhitelist: Set<Int> = HashSet()

    private var trackersWhitelist: Set<String> = HashSet()

    private var apIdTrackersWhitelist: Map<String, Boolean> = emptyMap()
    private var appUidTrackersWhitelist: Map<String, Boolean> = emptyMap()

    private val prefs: SharedPreferences

    companion object {
        private const val SHARED_PREFS_FILE = "trackers_whitelist_v3"
        private const val KEY_BLOCKING_ENABLED = "blocking_enabled"
        private const val KEY_APPS_WHITELIST = "apps_whitelist"
        private const val KEY_TRACKERS_WHITELIST = "trackers_whitelist"
        private const val KEY_APP_TRACKER_WHITELIST = "app_tracker_whitelist"
        private const val KEY_APP_TRACKER_BLACKLIST = "app_tracker_blacklist"

        // Deprecated keys.
        private const val SHARED_PREFS_FILE_V1 = "trackers_whitelist.prefs"
        private const val SHARED_PREFS_FILE_V2 = "trackers_whitelist_v2"
        private const val KEY_APP_TRACKERS_WHITELIST_PREFIX = "app_trackers_whitelist_"
    }

    init {
        prefs = context.getSharedPreferences(SHARED_PREFS_FILE, Context.MODE_PRIVATE)
        reloadCache()
        migrate(context)
    }

    private fun migrate(context: Context) {
        if (context.sharedPreferencesExists(SHARED_PREFS_FILE_V1)) {
            migrate1To2(context)
        }
        if (context.sharedPreferencesExists(SHARED_PREFS_FILE_V2)) {
            migrate2To3(context)
        }
    }

    private fun Context.sharedPreferencesExists(fileName: String): Boolean {
        return File(
            "${applicationInfo.dataDir}/shared_prefs/$fileName.xml"
        ).exists()
    }

    private fun migrate1To2(context: Context) {
        val prefsV1 = context.getSharedPreferences(SHARED_PREFS_FILE_V1, Context.MODE_PRIVATE)
        val editorV2 = prefs.edit()

        editorV2.putBoolean(KEY_BLOCKING_ENABLED, prefsV1.getBoolean(KEY_BLOCKING_ENABLED, false))

        val apIds = prefsV1.getStringSet(KEY_APPS_WHITELIST, HashSet())?.mapNotNull {
            try {
                val uid = it.toInt()
                appListRepository.getApp(uid)?.apId
            } catch (e: Exception) {
                null
            }
        }?.toSet() ?: HashSet()

        editorV2.putStringSet(KEY_APPS_WHITELIST, apIds)

        prefsV1.all.keys.forEach { key ->
            if (key.startsWith(KEY_APP_TRACKERS_WHITELIST_PREFIX)) {
                try {
                    val uid = key.substring(KEY_APP_TRACKERS_WHITELIST_PREFIX.length).toInt()
                    val apId = appListRepository.getApp(uid)?.apId
                    apId?.let {
                        val trackers = prefsV1.getStringSet(key, emptySet())
                        editorV2.putStringSet(KEY_APP_TRACKERS_WHITELIST_PREFIX + apId, trackers)
                    }
                } catch (e: Exception) { }
            }
        }
        editorV2.commit()

        context.deleteSharedPreferences(SHARED_PREFS_FILE_V1)

        reloadCache()
    }

    private fun migrate2To3(context: Context) {
        val prefsV2 = context.getSharedPreferences(SHARED_PREFS_FILE_V2, Context.MODE_PRIVATE)
        val editorV3 = prefs.edit()

        editorV3.putBoolean(KEY_BLOCKING_ENABLED, prefsV2.getBoolean(KEY_BLOCKING_ENABLED, false))

        prefsV2.getStringSet(KEY_APPS_WHITELIST, null)?.let {
            editorV3.putStringSet(KEY_APPS_WHITELIST, it)
        }
        editorV3.commit()

        runBlocking {
            prefsV2.all.keys.forEach { key ->
                if (key.startsWith(KEY_APP_TRACKERS_WHITELIST_PREFIX)) {
                    runCatching {
                        val apId = key.substring(KEY_APP_TRACKERS_WHITELIST_PREFIX.length)
                        prefsV2.getStringSet(key, null)
                            ?.map { trackerId -> buildApIdTrackerKey(apId, trackerId) }
                            ?.let { setWhitelisted(it, true) }
                    }
                }
            }
        }

        context.deleteSharedPreferences(SHARED_PREFS_FILE_V2)

        reloadCache()
    }

    private fun reloadCache() {
        isBlockingEnabled = prefs.getBoolean(KEY_BLOCKING_ENABLED, false)
        reloadAppsWhiteList()
        reloadAppTrackersWhitelist()
    }

    private fun reloadAppsWhiteList() {
        appsWhitelist = prefs.getStringSet(KEY_APPS_WHITELIST, HashSet()) ?: HashSet()
        appUidsWhitelist = appsWhitelist
            .mapNotNull { apId -> appListRepository.getApp(apId)?.uid }
            .toSet()
    }

    private fun reloadTrackersWhiteList() {
        trackersWhitelist = prefs.getStringSet(KEY_TRACKERS_WHITELIST, HashSet()) ?: HashSet()
    }

    private fun reloadAppTrackersWhitelist() {
        val whitelist = mutableMapOf<String, Boolean>()
        prefs.getStringSet(KEY_APP_TRACKER_WHITELIST, HashSet())?.forEach { key ->
            whitelist[key] = true
        }

        prefs.getStringSet(KEY_APP_TRACKER_BLACKLIST, HashSet())?.forEach { key ->
            whitelist[key] = false
        }

        apIdTrackersWhitelist = whitelist
        appUidTrackersWhitelist = whitelist.mapNotNull { (apIdTrackerId, isWhitelisted) ->
            val (apId, tracker) = parseApIdTrackerKey(apIdTrackerId)
            appListRepository.getApp(apId)?.uid?.let { uid ->
                buildAppUidTrackerKey(uid, tracker) to isWhitelisted
            }
        }.toMap()
    }

    var isBlockingEnabled: Boolean = false
        get() = field
        set(enabled) {
            prefs.edit().putBoolean(KEY_BLOCKING_ENABLED, enabled).apply()
            field = enabled
        }

    fun setWhiteListed(apIds: List<String>, isWhiteListed: Boolean) {
        val current = prefs.getStringSet(KEY_APPS_WHITELIST, HashSet())?.toHashSet() ?: HashSet()

        if (isWhiteListed) {
            current.addAll(apIds)
        } else {
            current.removeAll(apIds)
        }
        prefs.edit().putStringSet(KEY_APPS_WHITELIST, current).commit()
        reloadAppsWhiteList()
    }

    private suspend fun setWhitelisted(keys: List<String>, isWhitelisted: Boolean) = withContext(Dispatchers.IO) {
        val whitelist = HashSet<String>().apply {
            prefs.getStringSet(KEY_APP_TRACKER_WHITELIST, HashSet())?.let { addAll(it) }
        }

        val blacklist = HashSet<String>().apply {
            prefs.getStringSet(KEY_APP_TRACKER_BLACKLIST, HashSet())?.let { addAll(it) }
        }

        if (isWhitelisted) {
            blacklist.removeAll(keys)
            whitelist.addAll(keys)
        } else {
            whitelist.removeAll(keys)
            blacklist.addAll(keys)
        }

        prefs.edit().apply {
            putStringSet(KEY_APP_TRACKER_BLACKLIST, blacklist)
            putStringSet(KEY_APP_TRACKER_WHITELIST, whitelist)
            commit()
        }
        reloadAppTrackersWhitelist()
    }

    suspend fun setWhiteListed(tracker: Tracker, apId: String, isWhitelisted: Boolean) {
        setWhitelisted(listOf(buildApIdTrackerKey(apId, tracker.id)), isWhitelisted)
    }

    suspend fun setWhitelistedTrackersForApps(apIds: List<String>, trackerIds: List<String>, isWhitelisted: Boolean) = withContext(
        Dispatchers.IO
    ) {
        setWhitelisted(
            apIds.flatMap { apId ->
                trackerIds.map { trackerId -> buildApIdTrackerKey(apId, trackerId) }
            },
            isWhitelisted
        )
    }

    suspend fun setWhitelistedAppsForTracker(apIds: List<String>, trackerId: String, isWhitelisted: Boolean) = withContext(
        Dispatchers.IO
    ) {
        setWhitelisted(
            apIds.map { apId -> buildApIdTrackerKey(apId, trackerId) },
            isWhitelisted
        )
    }

    fun isAppWhiteListed(app: ApplicationDescription): Boolean {
        return appsWhitelist.contains(app.apId)
    }

    fun isAppWhiteListed(appUid: Int): Boolean {
        return appUidsWhitelist.contains(appUid)
    }

    fun isWhiteListed(appUid: Int, trackerId: String?): Boolean? {
        trackerId ?: return null

        val key = buildAppUidTrackerKey(appUid, trackerId)
        return appUidTrackersWhitelist.get(key)
    }

    private fun buildApIdTrackerKey(apId: String, trackerId: String): String {
        return "$apId|$trackerId"
    }

    private fun parseApIdTrackerKey(key: String): Pair<String, String> {
        return key.split("|").let { it[0] to it[1] }
    }

    private fun buildAppUidTrackerKey(appUid: Int, trackerId: String): String {
        return "$appUid-$trackerId"
    }

    fun areWhiteListEmpty(): Boolean {
        return appsWhitelist.isEmpty() &&
            trackersWhitelist.isEmpty() &&
            apIdTrackersWhitelist.values.none { it }
    }

    fun getWhiteListedApp(): List<ApplicationDescription> {
        return appsWhitelist.mapNotNull(appListRepository::getApp)
    }

    fun getWhiteListForApp(app: ApplicationDescription): List<String> {
        return apIdTrackersWhitelist.entries.mapNotNull { (key, isWhitelisted) ->
            if (!isWhitelisted) {
                null
            } else {
                val (apId, tracker) = parseApIdTrackerKey(key)
                if (apId == app.apId) {
                    tracker
                } else {
                    null
                }
            }
        }
    }

    fun clearWhiteList(apId: String) {
        val (whitelistToRemove, blacklistToRemove) = apIdTrackersWhitelist.entries
            .filter { (key, _) -> key.startsWith(apId) }
            .partition { (_, whitelisted) -> whitelisted }.let { (whitelistEntries, blacklistEntries) ->
                whitelistEntries.map { it.key }.toSet() to
                    blacklistEntries.map { it.key }.toSet()
            }

        val whitelist = HashSet<String>().apply {
            prefs.getStringSet(KEY_APP_TRACKER_WHITELIST, HashSet())?.let { addAll(it) }
        }

        val blacklist = HashSet<String>().apply {
            prefs.getStringSet(KEY_APP_TRACKER_BLACKLIST, HashSet())?.let { addAll(it) }
        }

        whitelist.removeAll(whitelistToRemove)
        blacklist.removeAll(blacklistToRemove)

        prefs.edit().apply {
            putStringSet(KEY_APP_TRACKER_WHITELIST, whitelist)
            putStringSet(KEY_APP_TRACKER_BLACKLIST, blacklist)
            commit()
        }
        reloadAppTrackersWhitelist()
    }

    fun setWhiteListed(tracker: Tracker, isWhiteListed: Boolean) {
        val current = prefs.getStringSet(KEY_TRACKERS_WHITELIST, HashSet())?.toHashSet() ?: HashSet()

        if (isWhiteListed) {
            current.add(tracker.id)
        } else {
            current.remove(tracker.id)
        }
        prefs.edit().putStringSet(KEY_TRACKERS_WHITELIST, current).commit()
        reloadTrackersWhiteList()
    }

    fun isWhiteListed(tracker: Tracker): Boolean {
        return trackersWhitelist.contains(tracker.id)
    }

    fun isTrackerWhiteListed(trackerId: String): Boolean {
        return trackersWhitelist.contains(trackerId)
    }
}
