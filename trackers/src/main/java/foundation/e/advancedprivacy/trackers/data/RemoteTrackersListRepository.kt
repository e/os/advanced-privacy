/*
 * Copyright (C) 2022 E FOUNDATION
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.advancedprivacy.data.repositories

import java.io.File
import java.io.FileWriter
import java.io.IOException
import java.io.PrintWriter
import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.GET
import timber.log.Timber

class RemoteTrackersListRepository {

    fun saveData(file: File, data: String): Boolean {
        try {
            val fos = FileWriter(file, false)
            val ps = PrintWriter(fos)
            ps.apply {
                print(data)
                flush()
                close()
            }
            return true
        } catch (e: IOException) {
            Timber.e(e, "While saving tracker file.")
        }
        return false
    }
}

interface ETrackersApi {
    companion object {
        fun build(): ETrackersApi {
            val retrofit = Retrofit.Builder()
                .baseUrl("https://gitlab.e.foundation/e/os/tracker-list/-/raw/main/")
                .addConverterFactory(ScalarsConverterFactory.create())
                .build()
            return retrofit.create(ETrackersApi::class.java)
        }
    }

    @GET("list/e_trackers.json")
    suspend fun trackers(): String
}
