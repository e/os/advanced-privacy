/*
 * Copyright (C) 2022 E FOUNDATION
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.advancedprivacy.trackers.data

import android.content.Context
import com.google.gson.Gson
import foundation.e.advancedprivacy.trackers.domain.entities.Tracker
import java.io.File
import java.io.FileInputStream
import java.io.InputStreamReader
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import timber.log.Timber

class TrackersRepository(
    private val context: Context,
    coroutineScope: CoroutineScope
) {

    private var trackersById: Map<String, Tracker> = HashMap()
    private var hostnameToId: Map<String, String> = HashMap()

    private val eTrackerFileName = "e_trackers.json"
    val eTrackerFile = File(context.filesDir.absolutePath, eTrackerFileName)

    init {
        coroutineScope.launch(Dispatchers.IO) {
            initTrackersFile()
        }
    }
    fun initTrackersFile() {
        try {
            var inputStream = context.assets.open(eTrackerFileName)
            if (eTrackerFile.exists()) {
                inputStream = FileInputStream(eTrackerFile)
            }
            val reader = InputStreamReader(inputStream, "UTF-8")
            val trackerResponse =
                Gson().fromJson(reader, ETrackersResponse::class.java)

            setTrackersList(mapper(trackerResponse))

            reader.close()
            inputStream.close()
        } catch (e: Exception) {
            Timber.e(e, "While parsing trackers in assets")
        }
    }

    private fun mapper(response: ETrackersResponse): List<Tracker> {
        return response.trackers.mapNotNull {
            try {
                it.toTracker()
            } catch (e: Exception) {
                null
            }
        }
    }

    private fun ETrackersResponse.ETracker.toTracker(): Tracker {
        return Tracker(
            id = id!!,
            hostnames = hostnames!!.toSet(),
            label = name!!,
            link = link
        )
    }

    private fun setTrackersList(list: List<Tracker>) {
        val trackersById: MutableMap<String, Tracker> = HashMap()
        val hostnameToId: MutableMap<String, String> = HashMap()
        list.forEach { tracker ->
            trackersById[tracker.id] = tracker
            for (hostname in tracker.hostnames) {
                hostnameToId[hostname] = tracker.id
            }
        }
        this.trackersById = trackersById
        this.hostnameToId = hostnameToId
    }

    fun isTracker(hostname: String?): Boolean {
        return hostnameToId.containsKey(hostname)
    }

    fun getTrackerId(hostname: String?): String? {
        return hostnameToId[hostname]
    }

    fun getTracker(id: String?): Tracker? {
        return trackersById[id]
    }
}
