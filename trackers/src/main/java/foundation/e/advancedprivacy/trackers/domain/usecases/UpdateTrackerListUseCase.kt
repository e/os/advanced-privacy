/*
 * Copyright (C) 2023 MURENA SAS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.advancedprivacy.trackers.domain.usecases

import foundation.e.advancedprivacy.data.repositories.ETrackersApi
import foundation.e.advancedprivacy.data.repositories.RemoteTrackersListRepository
import foundation.e.advancedprivacy.trackers.data.TrackersRepository
import java.io.IOException
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import timber.log.Timber

class UpdateTrackerListUseCase(
    private val remoteTrackersListRepository: RemoteTrackersListRepository,
    private val trackersRepository: TrackersRepository,
    private val coroutineScope: CoroutineScope

) {
    fun updateTrackers() = coroutineScope.launch {
        update()
    }

    suspend fun update() {
        val api = ETrackersApi.build()
        try {
            remoteTrackersListRepository.saveData(trackersRepository.eTrackerFile, api.trackers())
            trackersRepository.initTrackersFile()
        } catch (e: IOException) {
            Timber.d(e, "While updating trackers, is internet reachable ?")
        } catch (e: Exception) {
            Timber.e(e, "While updating trackers")
        }
    }
}
