/*
 * Copyright (C) 2023 MURENA SAS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package foundation.e.advancedprivacy.trackers.domain.usecases

import android.content.Context
import android.content.pm.PackageManager
import foundation.e.advancedprivacy.core.utils.runSuspendCatching
import foundation.e.advancedprivacy.data.repositories.AppListRepository
import foundation.e.advancedprivacy.domain.entities.ApplicationDescription
import foundation.e.advancedprivacy.trackers.data.StatsDatabase
import foundation.e.advancedprivacy.trackers.data.TrackersRepository
import foundation.e.advancedprivacy.trackers.data.WhitelistRepository
import java.util.concurrent.LinkedBlockingQueue
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import timber.log.Timber

class FilterHostnameUseCase(
    private val trackersRepository: TrackersRepository,
    private val whitelistRepository: WhitelistRepository,
    context: Context,
    private val appDesc: ApplicationDescription,
    private val database: StatsDatabase,
    private val appListRepository: AppListRepository
) {
    private var eBrowserAppUid = -1

    companion object {
        private const val E_BROWSER_DOT_SERVER = "chrome.cloudflare-dns.com"
    }

    init {
        initEBrowserDoTFix(context)
    }

    fun shouldBlock(hostname: String, appUid: Int = appDesc.uid): Boolean {
        var isBlocked = false

        if (isEBrowserDoTBlockFix(appUid, hostname)) {
            isBlocked = true
        } else if (trackersRepository.isTracker(hostname)) {
            val trackerId = trackersRepository.getTrackerId(hostname)
            if (shouldBlock(appUid, trackerId)) {
                isBlocked = true
            }
            queue.offer(DetectedTracker(trackerId, appUid, isBlocked))
        }
        return isBlocked
    }

    private fun initEBrowserDoTFix(context: Context) {
        try {
            eBrowserAppUid =
                context.packageManager.getApplicationInfo("foundation.e.browser", 0).uid
        } catch (e: PackageManager.NameNotFoundException) {
            Timber.i("no E Browser package found.")
        }
    }

    private fun isEBrowserDoTBlockFix(appUid: Int, hostname: String): Boolean {
        return appUid == eBrowserAppUid &&
            E_BROWSER_DOT_SERVER == hostname
    }

    private fun shouldBlock(appUid: Int, trackerId: String?): Boolean {
        return whitelistRepository.isBlockingEnabled &&
            trackerId != null &&
            !isWhitelisted(appUid, trackerId)
    }

    fun isWhitelisted(appUid: Int, trackerId: String): Boolean {
        return whitelistRepository.isWhiteListed(appUid, trackerId) ?: (
            whitelistRepository.isTrackerWhiteListed(trackerId) ||
                whitelistRepository.isAppWhiteListed(appUid)
            )
    }

    private val queue = LinkedBlockingQueue<DetectedTracker>()

    private suspend fun logAccess(detectedTracker: DetectedTracker) {
        appListRepository.getApp(detectedTracker.appUid)?.let { app ->
            database.logAccess(detectedTracker.trackerId, app.apId, detectedTracker.wasBlocked)
        }
    }

    fun writeLogJob(scope: CoroutineScope): Job {
        return scope.launch(Dispatchers.IO) {
            while (isActive) {
                runSuspendCatching {
                    logAccess(queue.take())
                }.onFailure {
                    Timber.e(it, "writeLogLoop detectedTrackersQueue.take() interrupted: ")
                }
            }
        }
    }

    inner class DetectedTracker(var trackerId: String?, var appUid: Int, var wasBlocked: Boolean)
}
