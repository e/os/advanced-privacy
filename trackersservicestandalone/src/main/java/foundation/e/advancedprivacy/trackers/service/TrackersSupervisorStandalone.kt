/*
 * Copyright (C) 2023 MURENA SAS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package foundation.e.advancedprivacy.trackers.service

import android.content.Context
import android.content.Intent
import foundation.e.advancedprivacy.domain.entities.FeatureState
import foundation.e.advancedprivacy.domain.usecases.VpnSupervisorUseCase
import foundation.e.advancedprivacy.trackers.domain.externalinterfaces.TrackersSupervisor
import foundation.e.advancedprivacy.trackers.service.data.NetworkDNSAddressRepository
import foundation.e.advancedprivacy.trackers.service.data.RequestDNSRepository
import foundation.e.advancedprivacy.trackers.service.usecases.ResolveDNSUseCase
import foundation.e.advancedprivacy.trackers.service.usecases.VpnSupervisorUseCaseStandalone
import java.util.function.Function
import kotlinx.coroutines.cancel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import org.koin.core.module.dsl.bind
import org.koin.core.module.dsl.singleOf
import org.koin.dsl.module
import org.pcap4j.packet.DnsPacket

class TrackersSupervisorStandalone(
    private val context: Context,
    private val resolveDNSUseCase: ResolveDNSUseCase
) : TrackersSupervisor {
    internal val mutableState: MutableStateFlow<FeatureState> = MutableStateFlow(FeatureState.OFF)
    override val state: StateFlow<FeatureState> = mutableState

    override fun start(): Boolean {
        return if (!isRunning()) {
            mutableState.value = FeatureState.STARTING
            val intent = Intent(context, TrackersService::class.java)
            context.startService(intent)
            true
        } else {
            false
        }
    }

    override fun stop(): Boolean {
        return when (mutableState.value) {
            FeatureState.ON -> {
                mutableState.value = FeatureState.STOPPING
                kotlin.runCatching { TrackersService.coroutineScope.cancel() }
                context.stopService(Intent(context, TrackersService::class.java))
                true
            }
            else -> false
        }
    }

    override fun isRunning(): Boolean {
        return state.value != FeatureState.OFF
    }

    override val dnsFilterForIpScrambling = Function<DnsPacket?, DnsPacket?> { dnsRequest -> resolveDNSUseCase.shouldBlock(dnsRequest) }
}

val trackerServiceModule = module {
    singleOf(::NetworkDNSAddressRepository)
    singleOf(::RequestDNSRepository)
    singleOf(::ResolveDNSUseCase)
    singleOf(::TunLooper)
    singleOf(::TrackersSupervisorStandalone) { bind<TrackersSupervisor>() }
    singleOf(::VpnSupervisorUseCaseStandalone) { bind<VpnSupervisorUseCase>() }
}
