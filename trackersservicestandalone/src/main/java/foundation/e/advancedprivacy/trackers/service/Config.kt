/*
 * Copyright (C) 2023 MURENA SAS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package foundation.e.advancedprivacy.trackers.service

internal object Config {
    const val SESSION_NAME = "TrackersService"

    const val FALLBACK_DNS = "1.1.1.1"
    const val VERBOSE = true

    const val VIRTUALDNS_IPV4 = "10.10.10.10"
    const val VIRTUALDNS_IPV6 = "fdc8:1095:91e1:aaaa:aaaa:aaaa:aaaa:aaa1"
    const val ADDRESS_IPV4 = "10.0.2.15"
    const val ADDRESS_IPV6 = "fdc8:1095:91e1:aaaa:aaaa:aaaa:aaaa:aaa2"

    const val BLOCKED_IPV4 = "127.0.0.1"
    const val BLOCKED_IPV6 = "::1"

    const val MTU = 3000
    const val LOCAL_RESOLVER_TTL = 60

    const val MAX_RESOLVER_COUNT = 100

    val DNS_SERVER_TO_CATCH_IPV4 = listOf(
        "8.8.8.8",
        "8.8.4.4",
        "1.1.1.1"
    )
    val DNS_SERVER_TO_CATCH_IPV6 = listOf(
        "2001:4860:4860::8888",
        "2001:4860:4860::8844"
    )
}
