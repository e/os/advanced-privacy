/*
 * Copyright (C) 2023 MURENA SAS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package foundation.e.advancedprivacy.trackers.service.data

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkRequest
import foundation.e.advancedprivacy.trackers.service.Config
import java.net.InetAddress

class NetworkDNSAddressRepository(private val context: Context) {
    private val connectivityManager: ConnectivityManager =
        context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

    fun start() {
        connectivityManager.registerNetworkCallback(
            NetworkRequest.Builder().build(),
            networkCallback
        )
    }

    fun stop() {
        kotlin.runCatching {
            connectivityManager.unregisterNetworkCallback(networkCallback)
        }
    }

    var dnsAddress: InetAddress = InetAddress.getByName(Config.FALLBACK_DNS)
        private set

    private val networkCallback = object : ConnectivityManager.NetworkCallback() {
        override fun onAvailable(network: Network) {
            super.onAvailable(network)
            connectivityManager.getLinkProperties(network)
                ?.dnsServers?.firstOrNull {
                    it.hostAddress.let {
                        it != Config.VIRTUALDNS_IPV4 && it != Config.VIRTUALDNS_IPV6
                    }
                }?.let {
                    dnsAddress = InetAddress.getByName(it.hostAddress)
                }
        }
    }
}
