/*
 * Copyright (C) 2022 MURENA SAS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.advancedprivacy.permissions.externalinterfaces

import android.app.NotificationChannel
import android.content.Context
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.graphics.drawable.Drawable
import foundation.e.advancedprivacy.domain.entities.AppOpModes
import foundation.e.advancedprivacy.domain.entities.ApplicationDescription
import foundation.e.advancedprivacy.externalinterfaces.permissions.PermissionsPrivacyModuleBase

/**
 * Implements [IPermissionsPrivacyModule] using only API authorized on the PlayStore.
 */
class PermissionsPrivacyModuleImpl(context: Context) : PermissionsPrivacyModuleBase(context) {
    override fun getApplications(filter: ((PackageInfo) -> Boolean)?): List<ApplicationDescription> {
        return context.packageManager.getInstalledPackages(PackageManager.GET_PERMISSIONS)
            .filter { filter?.invoke(it) == true }
            .map { buildApplicationDescription(it.applicationInfo) }
    }

    override fun getApplicationIcon(app: ApplicationDescription): Drawable? {
        return getApplicationIcon(app.packageName)
    }

    /**
     * @see IPermissionsPrivacyModule.toggleDangerousPermission
     * Return an ManualAction to go toggle manually the permission in the ap page of the settings.
     */
    override fun toggleDangerousPermission(appDesc: ApplicationDescription, permissionName: String, grant: Boolean): Boolean = false

    override fun setAppOpMode(appDesc: ApplicationDescription, appOpPermissionName: String, status: AppOpModes): Boolean = false

    override fun setVpnPackageAuthorization(packageName: String): Boolean {
        return false
    }

    override fun getAlwaysOnVpnPackage(): String? {
        return null
    }

    override fun getLegacyVpnDummyPackage(): String {
        return "[Legacy VPN]"
    }

    override fun setBlockable(notificationChannel: NotificationChannel) {}
}
